package rad.master.rk.zwonne.smartyha.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import rad.master.rk.zwonne.smartyha.R
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.TransitionManager
import io.reactivex.android.schedulers.AndroidSchedulers
import rad.master.rk.zwonne.smartyha.viewmodels.BackendUrlViewModel
import timber.log.Timber
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import io.reactivex.Completable
import io.reactivex.Single
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.util.hideSoftKeyboard
import rad.master.rk.zwonne.smartyha.databinding.FragmentBackendUrlBinding
import java.util.concurrent.TimeUnit
import rad.master.rk.zwonne.smartyha.ui.activities.MainActivity
import rad.master.rk.zwonne.smartyha.viewmodels.BackendUrlViewModel.BackendUrlEvents.LoadingEvent
import rad.master.rk.zwonne.smartyha.viewmodels.BackendUrlViewModel.BackendUrlEvents.ErrorEvent
import rad.master.rk.zwonne.smartyha.viewmodels.BackendUrlViewModel.BackendUrlEvents.OpenLoginScreenEvent
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel

class BackendUrlFragment : BaseFragment() {

    private val viewModel: BackendUrlViewModel by viewModels { viewModelFactory }

    lateinit var binding: FragmentBackendUrlBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return initViewBinding(inflater, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).hideActionBar()
    }

    override fun initViewModel(viewModel: BaseViewModel) {
        super.initViewModel(viewModel as BackendUrlViewModel)

        viewModel.showPortUi.observe(viewLifecycleOwner, Observer { handlePortChkBoxClick(it) })
        viewModel.openLoginScreenOrErrorEvent.observe(viewLifecycleOwner, Observer { handleEvent(it) })
    }

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
                                          R.layout.fragment_backend_url,
                                          container,
                                          false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        return binding.root
    }

    private fun handlePortChkBoxClick(showPortUi: Boolean) {
        Timber.d("handlePortChkBoxClick($showPortUi)")
        hideSoftKeyboard()
        val constraintSet = ConstraintSet()
        constraintSet.clone(binding.rootLayout)
        constraintSet.setVisibility(R.id.portGroup, if (showPortUi) View.VISIBLE else View.GONE)

        viewModel.addDisposable(Single.fromCallable{ true }
                                      .delay(400, TimeUnit.MILLISECONDS)
                                      .observeOn(AndroidSchedulers.mainThread())
                                      .flatMapCompletable {
                                          Completable.fromAction {
                                              TransitionManager.beginDelayedTransition(binding.rootLayout)
                                              constraintSet.applyTo(binding.rootLayout)
                                          }
                                      }
                                      .delay(400, TimeUnit.MILLISECONDS)
                                      .observeOn(AndroidSchedulers.mainThread())
                                      .subscribe( {
                                          if (showPortUi) {
                                              binding.portEt.requestFocus()
                                          } else {
                                              binding.portEt.clearFocus()
                                              binding.portEt.text!!.clear()
                                          }
                                      }, {}))
    }

    private fun handleEvent(event: BackendUrlViewModel.BackendUrlEvents) {
        Timber.d("handleEvent($event)")
        when(event) {
            is LoadingEvent -> showProgressUi()
            is OpenLoginScreenEvent -> goToLoginScreen(event.url)
            is ErrorEvent -> handleError(event)
        }
    }

    private fun showProgressUi() {
        Timber.d("showProgressUi()")
        hideSoftKeyboard()
        binding.contentGroup.visibility = View.GONE
        binding.portGroup.visibility = View.GONE
        binding.loadingGroup.visibility = View.VISIBLE
    }

    private fun goToLoginScreen(url: UrlHolder? = null) {
        Timber.d("goToLoginScreen()")
        val navDirections = BackendUrlFragmentDirections.actionBackendUrlToLogin().apply {
                                                                                      urlHolder = url
                                                                                  }
        navigate(navDirections)
    }

    private fun handleError(event: ErrorEvent) {
        Timber.d("handleError()")
        if (activity != null) {
            showContentUi(event.portUiWasVisible)
            AlertDialog.Builder(requireActivity())
                       .setMessage(R.string.backend_url_fragment_connection_error)
                       .create()
                       .show()
        }
    }

    private fun showContentUi(showPortUi: Boolean = false) {
        Timber.d("showContentUi()")
        hideSoftKeyboard()
        binding.loadingGroup.visibility = View.GONE
        binding.contentGroup.visibility = View.VISIBLE
        binding.portGroup.visibility = if (showPortUi) View.VISIBLE else View.GONE
    }

    override fun getViewModel(): BaseViewModel = viewModel
}
