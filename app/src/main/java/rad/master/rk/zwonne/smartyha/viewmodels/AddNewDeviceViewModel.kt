package rad.master.rk.zwonne.smartyha.viewmodels

import rad.master.rk.zwonne.smartyha.ui.fragments.AddNewDeviceFragmentDirections
import timber.log.Timber
import javax.inject.Inject

class AddNewDeviceViewModel @Inject constructor() : BaseViewModel() {

    fun onTradfriGatewayClicked() {
        Timber.d("onTradfriGatewayClicked()")
        navigate(AddNewDeviceFragmentDirections.actionAddNewDeviceToAddNewTradfriGateway())
    }

    fun onEstimoteBeaconClicked() {
        Timber.d("onEstimoteBeaconClicked()")
    }

    fun onAmazonEchoClicked() {
        Timber.d("onAmazonEchoClicked()")
    }

    fun onRasbPySensorClicked() {
        Timber.d("onRasbPySensorClicked()")
    }
}