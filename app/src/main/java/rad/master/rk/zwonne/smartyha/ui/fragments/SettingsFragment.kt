package rad.master.rk.zwonne.smartyha.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.transition.TransitionManager
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.databinding.FragmentSettingsBinding
import rad.master.rk.zwonne.smartyha.util.hideSoftKeyboard
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.SettingsViewModel
import timber.log.Timber
import java.util.concurrent.TimeUnit

class SettingsFragment : BaseFragment() {

    val viewModel: SettingsViewModel by viewModels { viewModelFactory }

    lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return initViewBinding(inflater, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val syncWithBackendFrag = SyncWithBackendPreferenceFragment()

        childFragmentManager.beginTransaction()
                            .replace(R.id.childFragmentContainer, syncWithBackendFrag)
                            .commit()
    }

    override fun initViewModel(viewModel: BaseViewModel) {
        super.initViewModel(viewModel as SettingsViewModel)

        viewModel.backendServiceUrlFromLocalDb.observe(viewLifecycleOwner, Observer { })
        viewModel.showSyncWithBackendUi.observe(viewLifecycleOwner, Observer {
            handleSyncWithBackendChkBoxClicked(it)
        })
    }

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
                                          R.layout.fragment_settings,
                                          container,
                                          false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        return binding.root
    }

    private fun handleSyncWithBackendChkBoxClicked(showSyncWithBackendUi: Boolean) {
        Timber.d("handleSyncWithBackendChkBoxClicked($showSyncWithBackendUi)")
        hideSoftKeyboard()
        val constraintSet = ConstraintSet()
        constraintSet.clone(binding.rootLayout)
        constraintSet.setVisibility(R.id.childFragmentContainer, if (showSyncWithBackendUi) View.VISIBLE else View.GONE)

        viewModel.addDisposable(Single.fromCallable{ true }
                                      .delay(400, TimeUnit.MILLISECONDS)
                                      .observeOn(AndroidSchedulers.mainThread())
                                      .flatMapCompletable {
                                          Completable.fromAction {
                                              TransitionManager.beginDelayedTransition(binding.rootLayout)
                                              constraintSet.applyTo(binding.rootLayout)
                                          }
                                      }
                                      .delay(400, TimeUnit.MILLISECONDS)
                                      .observeOn(AndroidSchedulers.mainThread())
                                      .subscribe( {}, {}))
    }

    override fun getViewModel(): BaseViewModel = viewModel
}