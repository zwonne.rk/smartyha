package rad.master.rk.zwonne.smartyha.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.MergeAdapter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.data.room.entities.Device
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway
import rad.master.rk.zwonne.smartyha.databinding.FragmentDevicesBinding
import rad.master.rk.zwonne.smartyha.ui.adapters.TradfriGatewayAdapter
import rad.master.rk.zwonne.smartyha.ui.adapters.TradfriLightBulbAdapter
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.DevicesViewModel
import javax.inject.Inject

class DevicesFragment : BaseFragment() {

    lateinit var adapter: MergeAdapter

    @Inject
    lateinit var tradfriGatewayAdapter: TradfriGatewayAdapter

    @Inject
    lateinit var tradfriLightBulbAdapter: TradfriLightBulbAdapter

    private val viewModel: DevicesViewModel by viewModels { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // init the adapter
        adapter = MergeAdapter(tradfriGatewayAdapter, tradfriLightBulbAdapter)

        return initViewBinding(inflater, container)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun initViewModel(viewModel: BaseViewModel) {
        super.initViewModel(viewModel as DevicesViewModel)

        tradfriGatewayAdapter.setDeviceClickedTrigger(viewModel.deviceClickedTrigger)
        tradfriLightBulbAdapter.setDeviceClickedTrigger(viewModel.deviceClickedTrigger)

        viewModel.tradfriGateways
                 .observe(viewLifecycleOwner, Observer {
                     tradfriGatewayAdapter.setData(it)
                 })

        viewModel.tradfriLightbulbs
                 .observe(viewLifecycleOwner, Observer {
                     it?.let {
                         tradfriLightBulbAdapter.setData(it)
                     }
                 })

        viewModel.user
                 .observe(viewLifecycleOwner, Observer { })

        viewModel.deviceClickedTrigger
                 .observe(viewLifecycleOwner, Observer { device ->
                     when (device) {
                         is TradfriGateway -> {
                             DevicesFragmentDirections.actionDevicesToTradfriGateway()
                                                      .apply {
                                                          tradfriGateway = device.copy()
                                                          isEdit = true
                                                      }
                         }
                         is Device.TradfriLightBulb -> {
                             DevicesFragmentDirections.actionDevicesToTradfriLightBulb()
                                                      .apply {
                                                          tradfriLightBulb = device.copy()
                                                      }
                         }
                         else -> null
                     }?.let { navDirections ->
                         navigate(navDirections)
                     }
                 })
    }

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        // Inflate the layout for this fragment
        val binding: FragmentDevicesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_devices, container, false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.adapter = adapter

        return binding.root
    }

    override fun getViewModel(): BaseViewModel = viewModel
}