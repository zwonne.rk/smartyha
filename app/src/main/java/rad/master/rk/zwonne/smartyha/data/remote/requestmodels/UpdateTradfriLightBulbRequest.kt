package rad.master.rk.zwonne.smartyha.data.remote.requestmodels

import com.google.gson.annotations.SerializedName

data class UpdateTradfriLightBulbRequest constructor(
        val gatewayPsk: String,
        @SerializedName("owner_uid") var uid: String,
        val bulbId: Int,
        val isOn: Boolean,
        val dimmerValue: Int
)