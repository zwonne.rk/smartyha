package rad.master.rk.zwonne.smartyha.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.data.livedata.SingleLiveEvent
import rad.master.rk.zwonne.smartyha.data.room.entities.Device
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway
import rad.master.rk.zwonne.smartyha.databinding.TradfriGatewayListItemBinding
import timber.log.Timber
import javax.inject.Inject

class TradfriGatewayAdapter @Inject constructor(): RecyclerView.Adapter<TradfriGatewayAdapter.TradfriGatewayHolder>() {

    var tradfriGateways: Array<TradfriGateway> = emptyArray()

    private lateinit var deviceClickedTrigger: MutableLiveData<Device>

    fun setData(tradfriGateways: Array<TradfriGateway>) {
        this.tradfriGateways = tradfriGateways
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TradfriGatewayHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TradfriGatewayHolder(TradfriGatewayListItemBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: TradfriGatewayHolder, position: Int) {
        val tradfriGateway = tradfriGateways[position]
        holder.bind(tradfriGateway)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.tradfri_gateway_list_item
    }

    override fun getItemCount(): Int {
        return tradfriGateways.size
    }

    fun setDeviceClickedTrigger(deviceClickedTrigger: SingleLiveEvent<Device>) {
        this.deviceClickedTrigger = deviceClickedTrigger
    }

    inner class TradfriGatewayHolder(binding: TradfriGatewayListItemBinding) :
            RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private var binding: TradfriGatewayListItemBinding = binding

        init {
            binding.root.setOnClickListener(this)
        }

        fun bind(tradfriGateway: TradfriGateway) {
            Timber.d("bindTradfriGateway()")
            binding.tradfriGateway = tradfriGateway
            binding.executePendingBindings()
        }

        override fun onClick(v: View) {
            Timber.d("onTradfriGatewayClicked(${binding.tradfriGateway?.psk})")
            deviceClickedTrigger.value = binding.tradfriGateway as Device
        }
    }
}