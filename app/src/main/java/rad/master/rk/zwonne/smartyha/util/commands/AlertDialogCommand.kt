package rad.master.rk.zwonne.smartyha.util.commands

data class AlertDialogCommand(
        val messageResId: Int,
        val titleResId: Int? = null,
        val positiveBtnTextId: Int? = null,
        val positiveBtnFunc: () -> Unit = {},
        val negativeBtnTextId: Int? = null,
        val negativeBtnFunc: () -> Unit = {}
)