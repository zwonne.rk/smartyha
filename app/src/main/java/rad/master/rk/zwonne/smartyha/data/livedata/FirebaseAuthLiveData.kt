package rad.master.rk.zwonne.smartyha.data.livedata

import androidx.lifecycle.LiveData
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.FirebaseAuth
import timber.log.Timber

class FirebaseAuthLiveData(private val firebaseAuth: FirebaseAuth): LiveData<FirebaseUser?>() {

    /**
     * This listener gets invoked:
     * a) Right after the listener has been registered,
     * b) When a user is signed in,
     * c) When the current user is signed out,
     * d) When the current user changes.
     */
    private val authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
        val currentUser = firebaseAuth.currentUser
        Timber.d("authStateListener: a new FirebaseUser: ${currentUser?.email}, current value: ${value?.email}")
        value = currentUser
    }

    override fun onActive() {
        super.onActive()
        firebaseAuth.addAuthStateListener(authStateListener)
    }

    override fun onInactive() {
        super.onInactive()
        firebaseAuth.removeAuthStateListener(authStateListener)
    }
}
