package rad.master.rk.zwonne.smartyha.dagger

import android.app.Application
import android.content.res.Resources
import com.google.firebase.auth.FirebaseAuth
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.data.remote.HostSelectionInterceptor
import rad.master.rk.zwonne.smartyha.data.remote.MyHostNameVerifier
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.util.RetrofitSelfSignedCertUtil
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module()
internal class ApiHelpersModule {
    @Provides
    fun provideFirebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(
            retrofitSelfSignedCertUtil: RetrofitSelfSignedCertUtil,
            hostSelectionInterceptor: HostSelectionInterceptor,
            app: Application
    ): OkHttpClient {
        val resultBuilder = OkHttpClient.Builder()

        try {
            // trigger exception if non existent
            app.applicationContext.resources.openRawResource(R.raw.smartyha)

            val socketFactoryAndTrustManager = retrofitSelfSignedCertUtil.getSslSocketFactoryAndTrustManager()

            resultBuilder.sslSocketFactory(socketFactoryAndTrustManager.first,
                                           socketFactoryAndTrustManager.second)
                         .hostnameVerifier(MyHostNameVerifier())
                         .addInterceptor(hostSelectionInterceptor)
        } catch (e: Resources.NotFoundException) {
            // no self-signed cert in project
            resultBuilder.readTimeout(10, TimeUnit.SECONDS)
                         .connectTimeout(10, TimeUnit.SECONDS)
        }

        return resultBuilder.build()
    }

    @Singleton
    @Provides
    fun provideSmartyHAServiceHttps(okHttpClient: OkHttpClient): SmartyHAService {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpBuilder = okHttpClient.newBuilder()
        okHttpBuilder.addInterceptor(httpLoggingInterceptor)

        val retrofit = Retrofit.Builder()
                               .baseUrl("http://localhost/")
                               .addConverterFactory(GsonConverterFactory.create())
                               .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                               .client(okHttpBuilder.build())
                               .build()

        return retrofit.create(SmartyHAService::class.java)
    }
}