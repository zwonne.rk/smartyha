package rad.master.rk.zwonne.smartyha.ui.fragments

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import dagger.android.support.DaggerFragment
import rad.master.rk.zwonne.smartyha.util.commands.NavigationCommand
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.ViewModelFactory
import javax.inject.Inject

abstract class BaseFragment : DaggerFragment() {

    @Inject
    protected lateinit var viewModelFactory: ViewModelFactory

    abstract fun getViewModel() : BaseViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel(getViewModel())
    }

    /**
     * Needs to be called after onCreateView(), since a crash will occur otherwise
     * when accessing viewLifecycleOwner
     */
    protected open fun initViewModel(viewModel: BaseViewModel) {
        lifecycle.addObserver(viewModel)
        subscribeToNavigationEvent(viewModel)
        subscribeToShowDialogEvent(viewModel)
    }

    /**
     * Subscribe to ViewModel's navigationCommands filed here
     * if needed.
     */
    private fun subscribeToNavigationEvent(viewModel: BaseViewModel) {
        viewModel.navigationCommands
                 .observe(viewLifecycleOwner, Observer { command ->
                     when (command) {
                         is NavigationCommand.To -> navigate(command.directions)
                     }
                 })
    }

    private fun subscribeToShowDialogEvent(viewModel: BaseViewModel) {
        viewModel.showDialogCommand
                 .observe(viewLifecycleOwner, Observer { command ->
                     if (context != null) {
                         val builder = AlertDialog.Builder(requireContext())
                                                  .setMessage(command.messageResId)

                         command.titleResId?.let {
                             builder.setTitle(it)
                         }

                         command.positiveBtnTextId?.let {
                             builder.setPositiveButton(it) { _, _ -> command.positiveBtnFunc.invoke() }
                         }

                         command.negativeBtnTextId?.let {
                             builder.setNegativeButton(it) { _, _ -> command.negativeBtnFunc.invoke() }
                         }

                         builder.create()
                                .show()
                     }
                 })
    }

    protected open fun navigate(navDirections: NavDirections) {
        findNavController().navigate(navDirections)
    }
}
