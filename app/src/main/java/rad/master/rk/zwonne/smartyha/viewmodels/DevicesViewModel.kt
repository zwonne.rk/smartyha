package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.launch
import rad.master.rk.zwonne.smartyha.data.livedata.SingleLiveEvent
import rad.master.rk.zwonne.smartyha.data.repositories.*
import rad.master.rk.zwonne.smartyha.data.room.entities.Device
import rad.master.rk.zwonne.smartyha.ui.fragments.DevicesFragmentDirections
import timber.log.Timber
import javax.inject.Inject

class DevicesViewModel @Inject constructor(
        userRepository: UserRepository,
        val tradfriGatewayRepository: TradfriGatewayRepository,
        val tradfriLightbulbRepository: TradfriLightbulbRepository,
        estimoteBeaconRepository: EstimoteBeaconRepository,
        amazonEchoRepository: AmazonEchoRepository,
        raspberryPiSensorRepository: RaspberryPiSensorRepository
) : BaseViewModel() {

    val user = userRepository.user

    @ExperimentalCoroutinesApi
    val tradfriGateways = tradfriGatewayRepository.tradfriGateways
                                                  .asLiveData()
    @FlowPreview
    @ExperimentalCoroutinesApi
    val tradfriLightbulbs = tradfriLightbulbRepository.tradfriLightbulbs
                                                      .asLiveData()

    val estimoteBeacons = estimoteBeaconRepository.estimoteBeacons

    val amazonEchoDots = amazonEchoRepository.amazonEchoDots

    val raspberryPiSensors = raspberryPiSensorRepository.sensors

    val deviceClickedTrigger = SingleLiveEvent<Device>()

    // Need this for unit testing override... smh
    fun getViewModelScope(): CoroutineScope {
        return viewModelScope
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun triggerDevices() {
        getViewModelScope().launch {
            tradfriGatewayRepository.triggerGateways()
            tradfriLightbulbRepository.triggerLightBulbs()
        }
    }

    fun onAddNewDeviceClicked() {
        Timber.d("onAddNewDeviceClicked()")
        navigate(DevicesFragmentDirections.actionDevicesToAddNewDevice())
    }
}