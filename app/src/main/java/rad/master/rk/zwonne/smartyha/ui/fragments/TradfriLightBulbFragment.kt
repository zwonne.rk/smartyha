package rad.master.rk.zwonne.smartyha.ui.fragments

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout.GONE
import androidx.constraintlayout.widget.ConstraintLayout.VISIBLE
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_tradfri_gateway.*

import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.databinding.FragmentTradfriLightBulbBinding
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.TradfriLightBulbViewModel
import timber.log.Timber

class TradfriLightBulbFragment : BaseFragment() {

    private val viewModel: TradfriLightBulbViewModel by viewModels { viewModelFactory }

    private lateinit var binding: FragmentTradfriLightBulbBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return initViewBinding(inflater, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subscribeToTradfriIcon()
    }

    override fun initViewModel(viewModel: BaseViewModel) {
        super.initViewModel(viewModel as TradfriLightBulbViewModel)

        arguments?.let { bundle ->
            TradfriLightBulbFragmentArgs.fromBundle(bundle)
                                        .tradfriLightBulb?.let { tradfriLightBulb ->
                                                                    viewModel._tradfriLightBulb.value = tradfriLightBulb
                                                               }
        }

        viewModel.enableAllUi.observe(viewLifecycleOwner, Observer { renderMainUi(it) })
        viewModel.showError.observe(viewLifecycleOwner, Observer { renderError() })
        viewModel.enableSaveButton.observe(viewLifecycleOwner, Observer { saveButton.isEnabled = it })
    }

    private fun subscribeToTradfriIcon() {
        Timber.d("subscribeToTradfriIcon()")
        val navController = findNavController()

        navController.currentBackStackEntry
                    ?.savedStateHandle
                    ?.getLiveData<String>(CameraFragment.ICON_KEY)
                    ?.observe(viewLifecycleOwner) { result: String ->
                        viewModel.onIconChanged(result)
                    }
    }

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
                                          R.layout.fragment_tradfri_light_bulb,
                                          container,
                                          false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        binding.seekBar.addOnChangeListener { slider, value, fromUser -> viewModel.dimmerChanged(value) }

        return binding.root
    }

    private fun renderMainUi(enableInput: Boolean) {
        Timber.d("renderMainUi($enableInput)")

        if (enableInput) renderContent() else renderLoading()

        enableUiElements(enableInput)
    }

    private fun enableUiElements(value: Boolean) {
        val inputType = when (value) {
            true -> InputType.TYPE_CLASS_TEXT
            false -> InputType.TYPE_NULL // block input while the screen is displaying Loading
        }

        binding.saveButton.isEnabled = value
    }

    private fun renderContent() {
        Timber.d("renderContent()")
        binding.loadingGroup.visibility = GONE
        binding.contentGroup.visibility = VISIBLE
    }

    private fun renderLoading() {
        Timber.d("renderLoading()")
        binding.contentGroup.visibility = GONE
        binding.loadingGroup.visibility = VISIBLE
    }

    private fun renderError() {
        Timber.d("renderError()")
        Toast.makeText(activity, R.string.tradfri_light_bulb_fragment_error_text, Toast.LENGTH_LONG)
             .show()
    }

    override fun getViewModel(): BaseViewModel = viewModel
}
