package rad.master.rk.zwonne.smartyha.ui.fragments

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import rad.master.rk.zwonne.smartyha.R

class SyncWithBackendPreferenceFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.fragment_sync_with_backend_preference, rootKey)
    }
}