package rad.master.rk.zwonne.smartyha.dagger

import dagger.Module
import dagger.android.ContributesAndroidInjector
import rad.master.rk.zwonne.smartyha.ui.activities.MainActivity

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    internal abstract fun contributeMainActivity(): MainActivity
}