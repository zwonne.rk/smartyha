package rad.master.rk.zwonne.smartyha.viewmodels

import com.snakydesign.livedataextensions.distinctUntilChanged
import com.snakydesign.livedataextensions.doAfterNext
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.ui.fragments.DashboardFragmentDirections
import rad.master.rk.zwonne.smartyha.util.commands.AlertDialogCommand
import timber.log.Timber
import javax.inject.Inject

class DashboardViewModel @Inject constructor(
        private val userRepository: UserRepository,
        private val remoteDataSource: SmartyHAService
) : BaseViewModel() {

    fun setBackendUrlFromLoginToRepo(url: UrlHolder?) {
        Timber.d("setBackendUrlFromLoginToRepo($url)")
        userRepository.backendUrlHolderFromLogin = url
    }

    val firebaseUser = userRepository.firebaseUser
                                     .doAfterNext {
                                         if (it == null) {
                                             Timber.d("firebaseUser: user is null, navigate to login")
                                             navigate(DashboardFragmentDirections.actionDashboardToBackendUrl())
                                         } else {
                                             handleServerAvailable(remoteDataSource)
                                         }
                                     }
                                     .distinctUntilChanged()

    val user = userRepository.user
                             .doAfterNext {
                                 Timber.d("user: ${it?.email}")
                             }

    private fun handleServerAvailable(remoteDataSource: SmartyHAService = this.remoteDataSource) {
        addDisposable(remoteDataSource.isServerAvailable()
                                      .subscribeOn(Schedulers.io())
                                      .observeOn(AndroidSchedulers.mainThread())
                                      .subscribe({ serverIsAvailable ->
                                          if (!serverIsAvailable) {
                                              Timber.e("firebaseUser: server not available issue")
                                              showDialogCommand.value = AlertDialogCommand(messageResId = R.string.dialog_message_backend_not_responding,
                                                                                           titleResId = R.string.dialog_title_backend_not_responding,
                                                                                           positiveBtnTextId = R.string.dialog_ok)
                                          }
                                      }, { error ->
                                          Timber.e(error, "firebaseUser: error ${error.message}")
                                          showDialogCommand.value = AlertDialogCommand(messageResId = R.string.dialog_message_backend_not_responding,
                                                                                       titleResId = R.string.dialog_title_backend_not_responding,
                                                                                       positiveBtnTextId = R.string.dialog_ok)
                                      }))
    }
}