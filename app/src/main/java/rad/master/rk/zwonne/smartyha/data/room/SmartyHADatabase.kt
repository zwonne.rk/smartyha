package rad.master.rk.zwonne.smartyha.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import rad.master.rk.zwonne.smartyha.data.room.daos.TradfriGatewayDao
import rad.master.rk.zwonne.smartyha.data.room.daos.TradfriLightBulbDao
import rad.master.rk.zwonne.smartyha.data.room.daos.UserDao
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriLightBulb
import rad.master.rk.zwonne.smartyha.data.room.entities.User

@Database(entities = [User::class, TradfriGateway::class, TradfriLightBulb::class], version = 4)
abstract class SmartyHADatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun tradfriGatewayDao(): TradfriGatewayDao
    abstract fun tradfriLightBulbDao(): TradfriLightBulbDao
}