package rad.master.rk.zwonne.smartyha.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.DashboardViewModel

class DashboardFragment : BaseFragment() {

    val viewModel: DashboardViewModel by viewModels { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun initViewModel(viewModel: BaseViewModel) {
        super.initViewModel(viewModel as DashboardViewModel)

        arguments?.let {
            viewModel.setBackendUrlFromLoginToRepo(LoginFragmentArgs.fromBundle(it).urlHolder)
        }

        // must be observed to trigger business logic in ViewModel
        // IMPORTANT: business logic in this observer lambda will not be executed because
        // of doAfterNext() call in ViewModel.
        viewModel.firebaseUser.observe(viewLifecycleOwner, Observer {})
        viewModel.user.observe(viewLifecycleOwner, Observer {})
    }

    override fun getViewModel(): BaseViewModel = viewModel
}