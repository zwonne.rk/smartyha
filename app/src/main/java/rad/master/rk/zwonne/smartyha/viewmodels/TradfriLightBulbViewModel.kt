package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.lifecycle.*
import com.snakydesign.livedataextensions.distinctUntilChanged
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import rad.master.rk.zwonne.smartyha.data.livedata.SingleLiveEvent
import rad.master.rk.zwonne.smartyha.data.repositories.TradfriLightbulbRepository
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriLightBulb
import rad.master.rk.zwonne.smartyha.ui.fragments.TradfriLightBulbFragmentDirections
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TradfriLightBulbViewModel @Inject constructor(
        val userRepository: UserRepository,
        val tradfriLightBulbRepository: TradfriLightbulbRepository
) : BaseViewModel() {

    var _tradfriLightBulb = MutableLiveData<TradfriLightBulb?>()

    var tradfriLightBulb: LiveData<TradfriLightBulb> = Transformations.switchMap(_tradfriLightBulb.distinctUntilChanged()) {
        Timber.d("TradfriLightBulb fetch triggered")
        it?.let {
            tradfriLightBulbRepository.getLightBulbById(it.gatewayPsk, it.bulbId)
        }
    }

    val enableAllUi = SingleLiveEvent<Boolean>()

    val showError = SingleLiveEvent<Boolean>()

    private val _iconChangedTrigger: PublishSubject<TradfriLightBulb> = PublishSubject.create()

    private val _dimmerChangedTrigger: PublishSubject<Float> = PublishSubject.create()

    private val _isOnChangedTrigger: PublishSubject<TradfriLightBulb> = PublishSubject.create()

    private val _dimmerChangedObservable: Observable<TradfriLightBulb>

    val enableSaveButton: LiveData<Boolean>

    init {
        _tradfriLightBulb.value = null

        _dimmerChangedObservable = _dimmerChangedTrigger.debounce(500, TimeUnit.MILLISECONDS)
                                                        .observeOn(AndroidSchedulers.mainThread())
                                                        .map { value ->
                                                            _tradfriLightBulb.value?.let {
                                                                Timber.d("dimmerChanged()")
                                                                it.copy(dimmerValue = value)
                                                            }
                                                        }

        enableSaveButton = LiveDataReactiveStreams.fromPublisher(Observable.mergeArray(_iconChangedTrigger,
                                                                                       _dimmerChangedObservable,
                                                                                       _isOnChangedTrigger.debounce(300, TimeUnit.MILLISECONDS))
                                                                           .subscribeOn(Schedulers.io())
                                                                           .switchMap { bulbFromScreen ->
                                                                                return@switchMap Observable.fromCallable {
                                                                                    _tradfriLightBulb.postValue(bulbFromScreen)

                                                                                    var enableSaveBtn = false
                                                                                    this.tradfriLightBulb.value?.let {
                                                                                        enableSaveBtn = it.reachable && ((it.isOn != bulbFromScreen.isOn) || (it.dimmerValue != bulbFromScreen.dimmerValue) || (it.icon != bulbFromScreen.icon))
                                                                                    }

                                                                                    Timber.d("enableSaveButton: $enableSaveBtn")
                                                                                    return@fromCallable enableSaveBtn
                                                                                }
                                                                           }
                                                                           .startWith(false)
                                                                           .toFlowable(BackpressureStrategy.LATEST))
    }

    fun isOnChanged(checked: Boolean) {
        _tradfriLightBulb.value?.let {
            Timber.d("isOnChanged()")
            _isOnChangedTrigger.onNext(it.copy(isOn = checked))
        }
    }

    fun dimmerChanged(value: Float) {
        _tradfriLightBulb.value?.let {
            Timber.d("dimmerChanged()")
            _dimmerChangedTrigger.onNext(value)
        }
    }

    fun onIconChanged(icon: String) {
        _tradfriLightBulb.value?.let {
            Timber.d("onIconChanged()")
            _iconChangedTrigger.onNext(it.copy(icon = icon))
        }
    }

    fun onIconClicked() {
        Timber.d("onIconClicked()")
        navigate(TradfriLightBulbFragmentDirections.actionTradfriLightBulbToCamera())
    }

    // Need this for unit testing override... smh
    fun getViewModelScope(): CoroutineScope {
        return viewModelScope
    }

    @ExperimentalCoroutinesApi
    @InternalCoroutinesApi
    fun onTryConnectionClicked() {
        userRepository.user.value?.email?.let { email ->
            getViewModelScope().launch {
                val lightBulb = tradfriLightBulb.value
                tradfriLightBulbRepository.checkConnection(email, lightBulb!!.gatewayPsk, lightBulb.bulbId)
            }
        }
    }

    fun saveTradfriLightBulb() {
        // trigger enable/disable button
        isOnChanged(_tradfriLightBulb.value!!.isOn)

        // call backend side
        getViewModelScope().launch {
            tradfriLightBulbRepository.updateSingleLightBulbOnBackend(_tradfriLightBulb.value!!.copy())
        }
    }
}
