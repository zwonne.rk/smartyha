package rad.master.rk.zwonne.smartyha.data.repositories

import android.content.SharedPreferences
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import io.reactivex.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import rad.master.rk.zwonne.smartyha.data.ApiResponseToEntityConverter
import rad.master.rk.zwonne.smartyha.data.livedata.SharedPrefKeys.SYNC_TRADFRI_GATEWAYS
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.data.remote.requestmodels.RegisterTradfriGatewayRequest
import rad.master.rk.zwonne.smartyha.data.remote.requestmodels.UpdateTradfriGatewayRequest
import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriGatewayFromResponse
import rad.master.rk.zwonne.smartyha.data.room.daos.TradfriGatewayDao
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.CoroutineDispatcherProvider
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TradfriGatewayRepository@Inject constructor(
        private val userRepository: UserRepository,
        private val localDataSource: TradfriGatewayDao,
        private val sharedPreferences: SharedPreferences,
        private val remoteDataSource: SmartyHAService,
        private val dispatcherProvider: CoroutineDispatcherProvider
) {
    @ExperimentalCoroutinesApi
    val tradfriGateways: Flow<Array<TradfriGateway>> = Transformations.switchMap(userRepository.user) { user ->
        return@switchMap MutableLiveData<User?>().apply {
                                                     value = user
                                                 }
        }.asFlow()
        .flatMapLatest { user ->
            return@flatMapLatest if (user != null) {
                 localDataSource.getGatewaysByUserIdFlow(user.uid)
            } else {
                flow { emit(emptyArray<TradfriGateway>()) }
            }
        }.flowOn(dispatcherProvider.getIO())

    @ExperimentalCoroutinesApi
    suspend fun triggerGateways() {
        val userFlow = userRepository.user.asFlow()
        val gatewaysFlow = tradfriGateways

        userFlow.combine(gatewaysFlow) { user: User?, gateways: Array<TradfriGateway>? ->
            Timber.d("triggerGateways()")
            val syncTradfriGatewaysWithBackend = sharedPreferences.getBoolean(SYNC_TRADFRI_GATEWAYS, false)

            val callBackend = user != null && gateways != null && syncTradfriGatewaysWithBackend
            return@combine Triple(callBackend, user, gateways)
        }.flatMapLatest { triple: Triple<Boolean, User?, Array<TradfriGateway>?> ->
            return@flatMapLatest flow {
                emit(if (triple.first) {
                    Pair(triple.third, getTradfriGatewaysForUserFromBackend(triple.second!!.email))
                } else {
                    Pair(triple.third, emptyArray<TradfriGatewayFromResponse>())
                })
            }.catch { err ->
                Timber.e("triggerGateways() onError(): %s", err.message)
                emit(Pair(triple.third, emptyArray()))
            }
        }
        .map { pair: Pair<Array<TradfriGateway>?, Array<TradfriGatewayFromResponse>> ->
            val gatewaysFromDb = pair.first
            val gatewaysFromResponse = pair.second
            // call to backend was successful, no need for sync flag anymore
            sharedPreferences.edit()
                             .putBoolean(SYNC_TRADFRI_GATEWAYS, false)
                             .apply()
            return@map gatewaysFromResponse.map {
                                               ApiResponseToEntityConverter.tradfriGatewayFromResponseToEntity(it)
                                           }.filter { tradfriGateway: TradfriGateway ->
                                               tradfriGateway !in gatewaysFromDb!!
                                           }.toTypedArray()
        }.map { gtWays: Array<TradfriGateway> ->
                    if (gtWays.isNotEmpty()) {
                        localDataSource.insert(*gtWays)
                    }
        }.flowOn(dispatcherProvider.getIO())
        .collect()
    }

    @VisibleForTesting
    suspend fun getTradfriGatewaysForUserFromBackend(
            email: String,
            remoteDataSource: SmartyHAService = this.remoteDataSource
    ): Array<TradfriGatewayFromResponse> {
        return remoteDataSource.getTradfriGatewaysForUser(email)
    }

    /**
     * Registers the gateway on the backend side, and returns the psk
     */
    fun registerNewGatewayBackendSide(
            tradfriGateway: TradfriGateway,
            remoteDataSource: SmartyHAService = this.remoteDataSource
    ): Single<String> {
        val newTradfriGatewayRequest = RegisterTradfriGatewayRequest(tradfriGateway.userId,
                                                                     tradfriGateway.host,
                                                                     tradfriGateway.identity,
                                                                     tradfriGateway.securityCode,
                                                                     tradfriGateway.label,
                                                                     tradfriGateway.icon)
        return remoteDataSource.registerNewTradfriGateway(newTradfriGatewayRequest)
    }

    /**
     * Updates the gateway on the backend side, and returns the psk
     */
    fun updateGatewayBackendSide(
            tradfriGateway: TradfriGateway,
            remoteDataSource: SmartyHAService = this.remoteDataSource
    ): Single<String> {
        val updateTradfriGatewayRequest = UpdateTradfriGatewayRequest(psk = tradfriGateway.psk,
                                                                      uid = tradfriGateway.userId,
                                                                      host = tradfriGateway.host,
                                                                      label = tradfriGateway.label,
                                                                      icon = tradfriGateway.icon)
        return remoteDataSource.updateTradfriGateway(updateTradfriGatewayRequest)
    }

    /**
     * Persists the gateway entity to the local db and returns the rowId
     */
    fun persistNewGatewayLocally(
            newGateway: TradfriGateway,
            localDataSource: TradfriGatewayDao = this.localDataSource
    ): Single<Long> {
        return localDataSource.insert(newGateway)
    }

    fun updateGatewayLocally(psk: String,
                             host: String,
                             label: String,
                             localDataSource: TradfriGatewayDao = this.localDataSource
    ): Single<Int> {
        return localDataSource.updateTradfriGateway(psk, host, label)
    }
}