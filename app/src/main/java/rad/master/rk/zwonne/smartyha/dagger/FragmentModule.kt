package rad.master.rk.zwonne.smartyha.dagger

import dagger.Module
import dagger.android.ContributesAndroidInjector
import rad.master.rk.zwonne.smartyha.ui.fragments.*

@Suppress("unused")
@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeBackendUrlFragment(): BackendUrlFragment

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeDashboardFragment(): DashboardFragment

    @ContributesAndroidInjector
    abstract fun contributeDevicesFragment(): DevicesFragment

    @ContributesAndroidInjector
    abstract fun contributeAddNewDeviceFragment(): AddNewDeviceFragment

    @ContributesAndroidInjector
    abstract fun contributeTradfriGatewayFragment(): TradfriGatewayFragment

    @ContributesAndroidInjector
    abstract fun contributeTradfriLightBulbFragment(): TradfriLightBulbFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector
    abstract fun contributeLogoutFragment(): LogoutFragment
}
