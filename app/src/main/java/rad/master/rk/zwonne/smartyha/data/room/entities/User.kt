package rad.master.rk.zwonne.smartyha.data.room.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import rad.master.rk.zwonne.smartyha.data.UrlHolder

@Entity(tableName = "users")
data class User(
        @PrimaryKey(autoGenerate = false) val uid: String,
        val email: String,
        @ColumnInfo(name = "registered_on_backend") var registeredOnBackend: Boolean = false,
        @Embedded var urlHolder: UrlHolder? = null
)