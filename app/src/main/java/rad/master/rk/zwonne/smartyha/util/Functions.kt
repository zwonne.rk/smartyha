package rad.master.rk.zwonne.smartyha.util

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

/**
 * Combines the latest values from multiple LiveData objects.
 * First emits after all LiveData objects have emitted a value, and will emit afterwards after any
 * of them emits a new value.
 *
 * The difference between combineLatest and zip is that the zip only emits after all LiveData
 * objects have a new value, but combineLatest will emit after any of them has a new value.
 *
 * This function is made based on snakydesign.livedataextensions functions
 */
fun <T, R> combineLatest(
        liveDataList: List<LiveData<T>>,
        combineFunction: (List<T?>) -> R
): LiveData<R> {
    val finalLiveData: MediatorLiveData<R> = MediatorLiveData()

    val emitArray = arrayOfNulls<Emit<T>>(liveDataList.size)
    for (i in liveDataList.indices) {
        emitArray[i] = Emit()
    }

    val combine: () -> Unit = {
        val didEmitArray = BooleanArray(liveDataList.size)
        emitArray.forEachIndexed { i, emit ->
            didEmitArray[i] = emit!!.emitted
        }

        if (!didEmitArray.contains(false)) {
            val combined = combineFunction(emitArray.map {
                                               it!!.value
                                           })
            finalLiveData.value = combined
        }
    }

    for (i in liveDataList.indices) {
        finalLiveData.addSource(liveDataList[i]) { value ->
            emitArray[i]!!.value = value
            combine()
        }
    }

    return finalLiveData
}

/**
 * Zips the values from multiple LiveData objects -> emits a value after all of them have emitted their values,
 * after that, emits values whenever all of them emit another value.
 *
 * The difference between combineLatest and zip is that the zip only emits after all LiveData
 * objects have a new value, but combineLatest will emit after any of them has a new value.
 */
fun <T, R> zip (
        liveDataArray: List<LiveData<T>?>,
        zipFunction: (List<T?>) -> R
): LiveData<R> {
    val finalLiveData: MediatorLiveData<R> = MediatorLiveData()

    val emitArray = arrayOfNulls<Emit<T>>(liveDataArray.size)
    for (i in liveDataArray.indices) {
        emitArray[i] = Emit()
    }

    val combine: () -> Unit = {
        val didEmitArray = BooleanArray(liveDataArray.size)
        emitArray.forEachIndexed { i, emit ->
            didEmitArray[i] = emit!!.emitted
        }

        if (!didEmitArray.contains(false)) {
            val combined = zipFunction(emitArray.map {
                                            it!!.value
                                       })
            emitArray.forEach { emit ->
                emit!!.reset()
            }
            finalLiveData.value = combined
        }
    }

    val nonNullElements = liveDataArray.filterNotNull()
    for (i in nonNullElements.indices) {
        finalLiveData.addSource(nonNullElements[i]) { value ->
            emitArray[i]!!.value = value
            combine()
        }
    }

    return finalLiveData
}

/**
 * Wrapper that wraps an emitted value.
 */
private class Emit<T> {

    internal var emitted: Boolean = false

    internal var value: T? = null
        set(value) {
            field = value
            emitted = true
        }

    fun reset() {
        value = null
        emitted = false
    }
}

/**
 * Returns a `LiveData` mapped from the input `source` `LiveData` by applying
 * `switchMapFunction` to each value set on `source`.
 *
 *
 * The returned `LiveData` delegates to the most recent `LiveData` created by
 * calling `switchMapFunction` with the most recent value set to `source`, without
 * changing the reference. In this way, `switchMapFunction` can change the 'backing'
 * `LiveData` transparently to any observer registered to the `LiveData` returned
 * by `switchMap()`.
 *
 *
 * Note that when the backing `LiveData` is switched, no further values from the older
 * `LiveData` will be set to the output `LiveData`. In this way, the method is
 * analogous to [io.reactivex.Observable.switchMap].
 *
 *
 * `switchMapFunction` will be executed on the main thread.
 *
 *
 * Here is an example class that holds a typed-in name of a user
 * `String` (such as from an `EditText`) in a [MutableLiveData] and
 * returns a `LiveData` containing a List of `User` objects for users that have
 * that name. It populates that `LiveData` by requerying a repository-pattern object
 * each time the typed name changes.
 *
 *
 * This `ViewModel` would permit the observing UI to update "live" as the user ID text
 * changes.
 *
 * <pre>
 * class UserViewModel extends AndroidViewModel {
 * MutableLiveData<String> nameQueryLiveData = ...
 *
 * LiveData<List></List><String>> getUsersWithNameLiveData() {
 * return Transformations.switchMap(
 * nameQueryLiveData,
 * name -> myDataSource.getUsersWithNameLiveData(name));
 * }
 *
 * void setNameQuery(String name) {
 * this.nameQueryLiveData.setValue(name);
 * }
 * }
</String></String></pre> *
 *
 * @param source            the `LiveData` to map from
 * @param switchMapFunction a function to apply to each value set on `source` to create a
 * new delegate `LiveData` for the returned one
 * @param <X>               the generic type parameter of `source`
 * @param <Y>               the generic type parameter of the returned `LiveData`
 * @return a LiveData mapped from `source` to type `<Y>` by delegating
 * to the LiveData returned by applying `switchMapFunction` to each
 * value set
</Y></X> */
@MainThread
fun <X, Y> switchMap(
        source: LiveData<X>,
        switchMapFunction: (X) -> LiveData<Y?>): LiveData<Y?> {
    val result = MediatorLiveData<Y?>()
    result.addSource(source, object : Observer<X> {
        var mSource: LiveData<Y?>? = null

        override fun onChanged(x: X) {
            val newLiveData: LiveData<Y?>? = switchMapFunction(x)
            if (mSource === newLiveData) {
                return
            }
            if (mSource != null) {
                result.removeSource(mSource!!)
            }
            mSource = newLiveData
            if (mSource != null) {
                result.addSource(mSource!!) { y -> result.value = y }
            }
        }
    })
    return result
}