package rad.master.rk.zwonne.smartyha.data.remote.requestmodels

import com.google.gson.annotations.SerializedName

data class UpdateTradfriGatewayRequest constructor(
        var psk: String,
        @SerializedName("owner_uid") var uid: String,
        var host: String,
        var label: String,
        var icon: String? = null
)