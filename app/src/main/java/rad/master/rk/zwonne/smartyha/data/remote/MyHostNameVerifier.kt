package rad.master.rk.zwonne.smartyha.data.remote

import timber.log.Timber
import java.security.cert.Certificate
import java.security.cert.CertificateParsingException
import java.security.cert.X509Certificate
import java.util.*
import java.util.regex.Pattern
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLException
import javax.net.ssl.SSLSession

class MyHostNameVerifier : HostnameVerifier {

    override fun verify(hostname: String, session: SSLSession): Boolean {
        Timber.d("verify(hostname: $hostname, session)")
        val certs: Array<Certificate>
        try {
            certs = session.peerCertificates
        } catch (e: SSLException) {
            return false
        }

        val x509 = certs[0] as X509Certificate
        // We can be case-insensitive when comparing the host we used to
        // establish the socket to the hostname in the certificate.
        val hostName = hostname.trim().toLowerCase(Locale.ENGLISH)
        Timber.d("verify() trimmed hostname: $hostname")
        // Verify the first CN provided. Other CNs are ignored. Firefox, wget,
        // curl, and Sun Java work this way.
        val firstCn = getFirstCn(x509)
        Timber.d("verify() firstCN: $firstCn")

        if (Pattern.matches(hostName, firstCn)) {
            return true
        }

        getDNSSubjectAlts(x509)?.let {
            for (cn in it) {
                if (Pattern.matches(hostName, cn)) {
                    return true
                }
            }
        }

        return false
    }

    private fun getFirstCn(cert: X509Certificate): String? {
        Timber.d("getFirstCn()")
        val subjectPrincipal = cert.subjectX500Principal.toString()
        for (token in subjectPrincipal.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
            val x = token.indexOf("CN=")
            Timber.d("getFirstCn() token: $token")
            if (x >= 0) {
                return token.substring(x + 3)
            }
        }
        return null
    }

    /**
     * This method was directly copied from the deprecated [org.apache.http.conn.ssl.AbstractVerifier] class.
     *
     * Extracts the array of SubjectAlt DNS names from an X509Certificate.
     * Returns null if there aren't any.
     *
     * Note:  Java doesn't appear able to extract international characters
     * from the SubjectAlts.  It can only extract international characters
     * from the CN field.
     *
     * (Or maybe the version of OpenSSL I'm using to test isn't storing the
     * international characters correctly in the SubjectAlts?).
     *
     * @param cert X509Certificate
     * @return Array of SubjectALT DNS names stored in the certificate.
     */
    private fun getDNSSubjectAlts(cert: X509Certificate): Array<String>? {
        val subjectAltList = LinkedList<String>()
        var subjectAlternativeNamesFromCert: Collection<List<*>>? = null
        try {
            // Each entry is a List whose first entry is an Int (the name type, 0-8)
            // and whose second entry is a String or a byte array (the name, in string
            // or ASN.1 DER encoded form, respectively).
            subjectAlternativeNamesFromCert = cert.subjectAlternativeNames
        } catch (cpe: CertificateParsingException) {
            Timber.d("Error parsing certificate ${cpe.message}")
        }

        subjectAlternativeNamesFromCert?.let {
            for (entry in it) {
                val type = entry[0] as Int
                // If type is 2, then we've got a dNSName
                if (type == 2) {
                    val dnsName = entry[1] as String
                    subjectAltList.add(dnsName)
                }
            }
        }

        return if (!subjectAltList.isEmpty()) {
            subjectAltList.toTypedArray()
        } else {
            null
        }
    }
}
