package rad.master.rk.zwonne.smartyha.data.room.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Single
import rad.master.rk.zwonne.smartyha.data.room.entities.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertUser(user: User)

    @Query("UPDATE users SET scheme = :scheme, schemeColonBackslash = :schemeColonBackslash, host = :host, portColon = :portColon, port = :port WHERE uid == :uid")
    fun updateUrlHolder(
            uid: String, scheme: String, schemeColonBackslash: String, host: String,
            portColon: String, port: String = ""
    ): Int

    @Query("UPDATE users SET registered_on_backend = :registeredOnBackend WHERE uid == :uid")
    fun updateRegisteredOnBackend(uid: String, registeredOnBackend: Boolean): Int

    @Delete
    fun deleteUser(user: User)

    @Query("SELECT * FROM users WHERE uid == :uid")
    fun getUserByIdSingle(uid: String): Single<List<User>>

    @Query("SELECT * FROM users WHERE email == :email")
    fun getUserByEmail(email: String): User?

    @Query("SELECT * FROM users WHERE uid == :uid")
    fun getUserByIdLiveData(uid: String): LiveData<User?>
}