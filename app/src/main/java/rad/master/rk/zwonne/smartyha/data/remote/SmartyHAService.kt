package rad.master.rk.zwonne.smartyha.data.remote

import io.reactivex.Single
import rad.master.rk.zwonne.smartyha.data.remote.requestmodels.RegisterTradfriGatewayRequest
import rad.master.rk.zwonne.smartyha.data.remote.requestmodels.RegisterUserRequest
import rad.master.rk.zwonne.smartyha.data.remote.requestmodels.UpdateTradfriGatewayRequest
import rad.master.rk.zwonne.smartyha.data.remote.requestmodels.UpdateTradfriLightBulbRequest
import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriGatewayFromResponse
import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriLightBulbFromResponse
import retrofit2.http.*

interface SmartyHAService {

    @GET("api/v1/ping/")
    fun isServerAvailable(): Single<Boolean>

    // User related APIs
    @GET("api/v1/accounts/{email}")
    fun isUserRegistered(@Path("email") email: String): Single<Boolean>

    @POST("/api/v1/accounts/")
    fun registerUser(@Body requestModel: RegisterUserRequest): Single<Boolean>

    // Tradfri Gateway related APIs
    @GET("api/v1/devices/tradfri/gateways/{email}")
    suspend fun getTradfriGatewaysForUser(@Path("email") email: String): Array<TradfriGatewayFromResponse>

    @POST("api/v1/devices/tradfri/gateways/")
    fun registerNewTradfriGateway(@Body requestModel: RegisterTradfriGatewayRequest): Single<String>

    @PUT("api/v1/devices/tradfri/gateways/")
    fun updateTradfriGateway(@Body requestModel: UpdateTradfriGatewayRequest): Single<String>

    // Tradfri LightBulb related APIs
    @GET("api/v1/devices/tradfri/bulbs/{email}")
    suspend fun getTradfriLightBulbsForUser(@Path("email") email: String): Array<TradfriLightBulbFromResponse>

    @GET("api/v1/devices/tradfri/bulb/{email}/{psk}/{bulb_id}")
    suspend fun getTradfriLightBulbForPskAndBulbId(@Path("email") email: String, @Path("psk") psk: String, @Path("bulb_id") bulbId: Int): Array<TradfriLightBulbFromResponse>

    @PUT("api/v1/devices/tradfri/bulb/")
    suspend fun updateTradfriLightBulb(@Body requestModel: UpdateTradfriLightBulbRequest): Pair<Int, Boolean>
}