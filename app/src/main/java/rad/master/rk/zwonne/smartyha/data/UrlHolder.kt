package rad.master.rk.zwonne.smartyha.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UrlHolder constructor(
        var scheme: String = "",
        var schemeColonBackslash: String = "", // needs this for bindingAdapter
        var host: String = "",
        var port: String = "",
        var portColon: String = "" // needs this for bindingAdapter
) : Parcelable {
    override fun toString(): String {
        return StringBuilder(scheme).append(schemeColonBackslash)
                                    .append(host)
                                    .append(portColon)
                                    .append(port)
                                    .toString()
    }
}
