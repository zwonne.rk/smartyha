package rad.master.rk.zwonne.smartyha.data.remote.requestmodels

import com.google.gson.annotations.SerializedName

data class RegisterTradfriGatewayRequest constructor(
        @SerializedName("owner_uid") var uid: String = "",
        var host: String = "",
        @SerializedName("identity") var connectionIdentity: String = "",
        @SerializedName("security_code") var securityCode: String = "",
        var label: String = "",
        var icon: String? = null
)