package rad.master.rk.zwonne.smartyha.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.util.Base64
import java.io.*
import java.lang.Exception

interface ImageUtils {
    companion object {
        @JvmStatic fun encodeBitmapToBase64String(bitmap: Bitmap): String {
            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)

            val bitmapData = bos.toByteArray()
            return Base64.encodeToString(bitmapData, Base64.NO_WRAP)
        }

        @JvmStatic fun decodeBase64StringToBitmap(input: String): Bitmap {
            val decodedByteArray = Base64.decode(input, Base64.NO_WRAP)
            return BitmapFactory.decodeByteArray(decodedByteArray, 0, decodedByteArray.size)
        }

        @JvmStatic fun resizeBitmap(source: Bitmap, maxLengthPx: Int): Bitmap {
            try {
                val aspectRatio = source.width.toDouble() / source.height.toDouble()
                val targetLengthPx = (maxLengthPx * aspectRatio).toInt()

                if (source.height >= source.width) {
                    if (source.height <= maxLengthPx) { // if image height already smaller than the required height
                        return source
                    }

                    return Bitmap.createScaledBitmap(source, targetLengthPx, maxLengthPx, false)
                } else {
                    if (source.width <= maxLengthPx) { // if image width already smaller than the required width
                        return source
                    }

                    return Bitmap.createScaledBitmap(source, maxLengthPx, targetLengthPx, false)
                }
            } catch (e: Exception) {
                return source
            }
        }

        @JvmStatic fun rotateBitmap(bitmap: Bitmap, rotationDegrees: Float): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(rotationDegrees)
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        }
    }
}