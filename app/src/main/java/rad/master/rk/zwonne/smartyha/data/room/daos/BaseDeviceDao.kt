package rad.master.rk.zwonne.smartyha.data.room.daos

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update
import io.reactivex.Single
import rad.master.rk.zwonne.smartyha.data.room.entities.Device

interface BaseDeviceDao<T : Device> {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(obj: T): Single<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg obj: T): List<Long>

    @Update
    fun updateSingle(obj: T): Single<Int>

    @Update
    suspend fun update(obj: T): Int

    @Update
    fun update(vararg obj: T): Int

    @Delete
    fun delete(gateway: T)
}