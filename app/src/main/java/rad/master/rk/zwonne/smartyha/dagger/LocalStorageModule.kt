package rad.master.rk.zwonne.smartyha.dagger

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import dagger.Module
import dagger.Provides
import rad.master.rk.zwonne.smartyha.data.room.SmartyHADatabase
import rad.master.rk.zwonne.smartyha.data.room.daos.TradfriGatewayDao
import rad.master.rk.zwonne.smartyha.data.room.daos.TradfriLightBulbDao
import rad.master.rk.zwonne.smartyha.data.room.daos.UserDao
import javax.inject.Singleton

@Module()
internal class LocalStorageModule {
    @Singleton
    @Provides
    fun provideDatabase(app: Application): SmartyHADatabase {
        return Room.databaseBuilder(app, SmartyHADatabase::class.java, "SmartyHA.db")
                   .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(db: SmartyHADatabase): UserDao {
        return db.userDao()
    }

    @Singleton
    @Provides
    fun provideTradfriGatewayDao(db: SmartyHADatabase): TradfriGatewayDao {
        return db.tradfriGatewayDao()
    }

    @Singleton
    @Provides
    fun provideTradfriLightBulbDao(db: SmartyHADatabase): TradfriLightBulbDao {
        return db.tradfriLightBulbDao()
    }

    @Singleton
    @Provides
    fun provideSharedPreferences(app: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
    }
}