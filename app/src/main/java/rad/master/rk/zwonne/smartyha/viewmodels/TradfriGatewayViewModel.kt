package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import io.reactivex.*
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import rad.master.rk.zwonne.smartyha.data.livedata.SingleLiveEvent
import rad.master.rk.zwonne.smartyha.data.repositories.TradfriGatewayRepository
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway
import rad.master.rk.zwonne.smartyha.ui.fragments.TradfriGatewayFragmentDirections
import rad.master.rk.zwonne.smartyha.util.UrlUtils
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TradfriGatewayViewModel @Inject constructor(
        val userRepository: UserRepository,
        private val tradfriGatewayRepository: TradfriGatewayRepository
) : BaseViewModel() {

    val _is_edit = MutableLiveData<Boolean>()

    var tradfriGateway = MutableLiveData<TradfriGateway>()

    val enableAllUi = SingleLiveEvent<Boolean>()

    val showError = SingleLiveEvent<Boolean>()

    private val _securityCodeTxtChangedTrigger: PublishSubject<Unit> = PublishSubject.create()

    private val _identityTxtChangedTrigger: PublishSubject<Unit> = PublishSubject.create()

    private val _hostTxtChangedTrigger: PublishSubject<Unit> = PublishSubject.create()

    private val _labelTxtChangedTrigger: PublishSubject<Unit> = PublishSubject.create()

    private val _iconChangedTrigger: PublishSubject<Unit> = PublishSubject.create()

    val inputDataIsValid: LiveData<Boolean>

    init {
        tradfriGateway.value = TradfriGateway()
        _is_edit.value = false

        inputDataIsValid = LiveDataReactiveStreams.fromPublisher(Observable.mergeArray(
                                                                                _iconChangedTrigger,
                                                                                _securityCodeTxtChangedTrigger.debounce(500, TimeUnit.MILLISECONDS),
                                                                                _identityTxtChangedTrigger.debounce(500, TimeUnit.MILLISECONDS),
                                                                                _hostTxtChangedTrigger.debounce(500, TimeUnit.MILLISECONDS),
                                                                                _labelTxtChangedTrigger.debounce(500, TimeUnit.MILLISECONDS))
                                                                           .subscribeOn(Schedulers.io())
                                                                           .switchMap {
                                                                                return@switchMap Observable.fromCallable {
                                                                                    val immutableTradfriGateway = tradfriGateway.value!!.copy()
                                                                                    val result = (immutableTradfriGateway.securityCode.trim().isNotBlank() || _is_edit.value!!) &&
                                                                                                 UrlUtils.isHostValid(immutableTradfriGateway.host.trim()) &&
                                                                                                 (immutableTradfriGateway.identity.trim().isNotBlank() || _is_edit.value!!)
                                                                                    Timber.d("isDataInputValid: $result")
                                                                                    return@fromCallable result
                                                                                }
                                                                           }
                                                                           .startWith(false)
                                                                           .toFlowable(BackpressureStrategy.LATEST))
    }

    fun saveTradfriGateway() {
        Timber.d("saveTradfriGateway()")
        enableAllUi.value = false
            val immutableTradfriGateway = tradfriGateway.value!!
                                                        .copy(userId = userRepository.user.value?.uid.orEmpty())
                                                        .apply {
                                                            host.trim()
                                                            identity.trim()
                                                            securityCode.trim()
                                                            label.trim()
                                                        }

            addDisposable(saveGatewayOnBackendAndInLocalDb(immutableTradfriGateway).subscribeOn(Schedulers.io())
                                                                                   .delay(1, TimeUnit.SECONDS)
                                                                                   .subscribe({ rowId ->
                                                                                       handleSuccess(rowId)
                                                                                   }, { error ->
                                                                                       handleError(error)
                                                                                   }))
    }

    @VisibleForTesting
    fun handleSuccess(rowId: Number?) {
        Timber.d("saveTradfriGateway() Gateway saved successfully! Row id: $rowId")
        enableAllUi.postValue(true)
    }

    @VisibleForTesting
    fun handleError(error: Throwable) {
        Timber.e("saveTradfriGateway() Gateway wasn't saved successfully! Error: ${error.message}")
        enableAllUi.postValue(true)
        showError.postValue(true)
    }

    private fun saveGatewayOnBackendAndInLocalDb(
            tradfriGateway: TradfriGateway,
            tradfriGatewayRepository: TradfriGatewayRepository = this.tradfriGatewayRepository
    ): Single<out Number?> {
        if (_is_edit.value!!) {
            return tradfriGatewayRepository.updateGatewayBackendSide(tradfriGateway)
                                           .flatMap { psk ->
                                               return@flatMap if (psk.isNotBlank()) {
                                                   tradfriGatewayRepository.updateGatewayLocally(psk,
                                                                                                 tradfriGateway.host,
                                                                                                 tradfriGateway.label)
                                               } else {
                                                   // kill the stream if updating the gateway on the backend
                                                   // was unsuccessful
                                                   Single.fromCallable { null }
                                               }
                                           }
        } else {
            return tradfriGatewayRepository.registerNewGatewayBackendSide(tradfriGateway)
                                           .flatMap { psk ->
                                               return@flatMap if (psk.isNotBlank()) {
                                                   tradfriGatewayRepository.persistNewGatewayLocally(tradfriGateway)
                                               } else {
                                                   // kill the stream if registering the gateway on the backend
                                                   // was unsuccessful
                                                   Single.fromCallable { null }
                                               }
                                           }
        }
    }

    fun onSecurityCodeTextChanged() {
        Timber.d("onSecurityCodeTextChanged()")
        _securityCodeTxtChangedTrigger.onNext(Unit)
    }

    fun onIdentityTextChanged() {
        Timber.d("onIdentityTextChanged()")
        _identityTxtChangedTrigger.onNext(Unit)
    }

    fun onHostTextChanged() {
        Timber.d("onHostTextChanged()")
        _hostTxtChangedTrigger.onNext(Unit)
    }

    fun onLabelTextChanged() {
        Timber.d("onLabelTextChanged()")
        _labelTxtChangedTrigger.onNext(Unit)
    }

    fun onIconChanged() {
        Timber.d("onIconChanged()")
        _iconChangedTrigger.onNext(Unit)
    }

    fun onIconClicked() {
        Timber.d("onIconClicked()")
        navigate(TradfriGatewayFragmentDirections.actionTradfriGatewayToCamera())
    }
}
