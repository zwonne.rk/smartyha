package rad.master.rk.zwonne.smartyha.util.commands

import androidx.navigation.NavDirections

/**
 * This class is used to communicate between ViewModel and Fragment via
 * the command pattern for use in navigation.
 * (to trigger navigation from ViewModel)
 */
sealed class NavigationCommand {
    data class To(val directions: NavDirections): NavigationCommand()
}