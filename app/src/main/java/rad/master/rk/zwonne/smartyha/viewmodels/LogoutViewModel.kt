package rad.master.rk.zwonne.smartyha.viewmodels

import com.snakydesign.livedataextensions.zip
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.ui.fragments.LogoutFragmentDirections
import timber.log.Timber
import javax.inject.Inject

class LogoutViewModel @Inject constructor(private val userRepository: UserRepository) : BaseViewModel() {

    val isLoggedOut = zip(userRepository.firebaseUser, userRepository.user) { fbUser, user ->
        Timber.d("isLoggedOut: fbUser: ${fbUser?.email}, user: ${user?.email}")
        val isLoggedOut = fbUser == null && user == null

        if (isLoggedOut) {
            Timber.d("isLoggedOut: go to login fragment")
            navigate(LogoutFragmentDirections.actionLogoutToDashboard())
        } else {
            Timber.d("isLoggedOut: stay in logout fragment")
        }

        isLoggedOut
    }

    init {
        signOut()
    }

    fun signOut() {
        userRepository.signOut()
    }
}
