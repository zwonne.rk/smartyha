package rad.master.rk.zwonne.smartyha.dagger

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import rad.master.rk.zwonne.smartyha.viewmodels.*

@Suppress("unused")
@Module
internal abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(BackendUrlViewModel::class)
    abstract fun bindBackendUrlViewModel(viewModel: BackendUrlViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    abstract fun bindDashboardViewModel(viewModel : DashboardViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DevicesViewModel::class)
    abstract fun bindDevicesViewModel(viewModel : DevicesViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddNewDeviceViewModel::class)
    abstract fun bindAddNewDeviceViewModel(viewModel : AddNewDeviceViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TradfriGatewayViewModel::class)
    abstract fun bindTradfriGatewayViewModel(viewModel : TradfriGatewayViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TradfriLightBulbViewModel::class)
    abstract fun bindTradfriLightBulbViewModel(viewModel : TradfriLightBulbViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    abstract fun bindSettingsViewModel(viewModel : SettingsViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LogoutViewModel::class)
    abstract fun bindLogoutViewModel(viewModel : LogoutViewModel) : ViewModel
}
