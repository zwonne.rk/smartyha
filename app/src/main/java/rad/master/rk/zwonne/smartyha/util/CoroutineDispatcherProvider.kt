package rad.master.rk.zwonne.smartyha.util

import kotlinx.coroutines.Dispatchers

/**
 * a dispatcher helper to ease unit testing.
 */
open class CoroutineDispatcherProvider() {
    open fun getIO() = Dispatchers.IO
    open fun getDefault() = Dispatchers.IO
}