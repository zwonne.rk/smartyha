package rad.master.rk.zwonne.smartyha.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.data.livedata.SingleLiveEvent
import rad.master.rk.zwonne.smartyha.data.room.entities.Device
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriLightBulb
import rad.master.rk.zwonne.smartyha.databinding.TradfriLightBulbListItemBinding
import timber.log.Timber
import javax.inject.Inject

class TradfriLightBulbAdapter @Inject constructor(): RecyclerView.Adapter<TradfriLightBulbAdapter.TradfriLightBulbHolder>() {

    var tradfriLightBulbs: Array<TradfriLightBulb> = emptyArray()

    private lateinit var deviceClickedTrigger: MutableLiveData<Device>

    fun setData(tradfriLightBulbs: Array<TradfriLightBulb>) {
        this.tradfriLightBulbs = tradfriLightBulbs
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TradfriLightBulbHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TradfriLightBulbHolder(TradfriLightBulbListItemBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: TradfriLightBulbHolder, position: Int) {
        val tradfriLightBulb = tradfriLightBulbs[position]
        holder.bind(tradfriLightBulb)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.tradfri_gateway_list_item
    }

    override fun getItemCount(): Int {
        return tradfriLightBulbs.size
    }

    fun setDeviceClickedTrigger(deviceClickedTrigger: SingleLiveEvent<Device>) {
        this.deviceClickedTrigger = deviceClickedTrigger
    }

    inner class TradfriLightBulbHolder(binding: TradfriLightBulbListItemBinding) :
            RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private var binding: TradfriLightBulbListItemBinding = binding

        init {
            binding.root.setOnClickListener(this)
        }

        fun bind(tradfriLightBulb: TradfriLightBulb) {
            Timber.d("bindTradfriLightBulb()")
            binding.tradfriLightBulb = tradfriLightBulb
            binding.executePendingBindings()
        }

        override fun onClick(v: View) {
            Timber.d("onTradfriLightBulbClicked(${binding.tradfriLightBulb?.gatewayPsk})")
            deviceClickedTrigger.value = binding.tradfriLightBulb as Device
        }
    }
}