package rad.master.rk.zwonne.smartyha.data.room.entities

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize

sealed class Device() : Parcelable {

    abstract fun getId(): String

    abstract fun getImage(): String?

    @Parcelize
    @Entity(tableName = "tradfri_gateways",
            indices = [Index(value = ["user_id"], unique = true)],
            foreignKeys = [ForeignKey(entity = User::class,
                                      parentColumns = arrayOf("uid"),
                                      childColumns = arrayOf("user_id"),
                                      onDelete = ForeignKey.CASCADE)])
    data class TradfriGateway(
            @PrimaryKey(autoGenerate = false) var psk: String = "",
            @Ignore var identity: String = "",
            @Ignore var securityCode: String = "",
            @ColumnInfo(name = "user_id") var userId: String = "",
            var host: String = "",
            var label: String = "",
            // icon represented by a 64 bit encoded string
            var icon: String? = null
    ): Device() {
        override fun getId() = psk
        override fun getImage() = icon
    }

    @Parcelize
    @Entity(tableName = "tradfri_light_bulbs",
            foreignKeys = [
                ForeignKey(entity = TradfriGateway::class,
                           parentColumns = ["psk"],
                           childColumns = ["gateway_psk"],
                           onDelete = ForeignKey.CASCADE),
                ForeignKey(entity = TradfriGateway::class,
                           parentColumns = ["user_id"],
                           childColumns = ["uid"],
                           onDelete = ForeignKey.CASCADE)
            ],
            primaryKeys = ["gateway_psk", "bulb_id"])
    data class TradfriLightBulb (
            @ColumnInfo(name = "gateway_psk", index = true) val gatewayPsk: String,
            @ColumnInfo(name = "bulb_id") val bulbId: Int,
            @ColumnInfo(name = "uid", index = true) var userId: String = "",
            val name: String,
            @ColumnInfo(name = "last_seen") val lastSeen: Int,
            val reachable: Boolean,
            @ColumnInfo(name = "is_on") var isOn: Boolean,
            @ColumnInfo(name = "dimmer_value") val dimmerValue: Float,
            val manufacturer: String,
            @ColumnInfo(name = "model_number") val modelNumber: String,
            @ColumnInfo(name = "firmware_version") val firmwareVersion: String,
            // icon represented by a 64 bit encoded string
            var icon: String? = null) : Device() {
        override fun getId() = "$gatewayPsk--$bulbId"
        override fun getImage() = icon
    }
}