package rad.master.rk.zwonne.smartyha.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Size
import android.view.*
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.common.util.concurrent.ListenableFuture
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.databinding.FragmentCameraBinding
import rad.master.rk.zwonne.smartyha.util.ImageUtils
import java.util.concurrent.Executors

class CameraFragment: Fragment() {

    companion object {
        const val ICON_KEY: String = "iconEncodedString"

        private const val REQUEST_CODE_PERMISSIONS = 10

        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }

    lateinit var binding: FragmentCameraBinding

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>

    private lateinit var previewView: PreviewView

    private lateinit var imagePreview: Preview

    private lateinit var imageCapture: ImageCapture

    private val executor = Executors.newSingleThreadExecutor()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val result = initViewBinding(inflater, container)

        previewView = binding.previewView

        cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        if (allPermissionsGranted()) {
            previewView.post { startCamera() }
        } else {
            requestPermissions(
                    REQUIRED_PERMISSIONS,
                    REQUEST_CODE_PERMISSIONS
            )
        }

        binding.cameraCaptureButton.setOnClickListener {
            takePicture()
        }

        return result
    }

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
                                          R.layout.fragment_camera,
                                          container,
                                          false)
        binding.lifecycleOwner = this

        return binding.root
    }

    private fun startCamera() {
        previewView.preferredImplementationMode = PreviewView.ImplementationMode.TEXTURE_VIEW
        previewView.scaleType = PreviewView.ScaleType.FIT_START

        // Image preview
        imagePreview = Preview.Builder()
                              .apply {
                                  setTargetAspectRatio(AspectRatio.RATIO_4_3)
                              }.build()

        // Image capture
        imageCapture = ImageCapture.Builder()
                                   .apply {
                                       // quick, but low quality -> that's ok since this is used for small icons
                                       setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                                       setTargetResolution(Size(300, 300))
                                   }.build()

        val orientationEventListener = object : OrientationEventListener(requireContext()) {
            override fun onOrientationChanged(orientation : Int) {
                // Monitors orientation values to determine the target rotation value
                val rotation : Int = when (orientation) {
                    in 45..134 -> Surface.ROTATION_0
                    in 135..224 -> Surface.ROTATION_270
                    in 225..314 -> Surface.ROTATION_180
                    else -> Surface.ROTATION_90
                }

                imageCapture.targetRotation = rotation
            }
        }
        orientationEventListener.enable()

        val cameraSelector = CameraSelector.Builder()
                                           .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                                           .build()

        cameraProviderFuture.addListener(Runnable {
                                             val cameraProvider = cameraProviderFuture.get()
                                             cameraProvider.bindToLifecycle(this, cameraSelector, imagePreview, imageCapture)
                                             // if cameraInfo is null, then default is textureview, not surfaceview
                                             imagePreview.setSurfaceProvider(previewView.createSurfaceProvider(null))
                                         },
                                         ContextCompat.getMainExecutor(context))
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                previewView.post { startCamera() }
            } else {
                if (isAdded) {
                    findNavController().popBackStack()
                }
            }
        }
    }

    private fun takePicture() {
        binding.progressBar.visibility = View.VISIBLE

        imageCapture.takePicture(executor, object : ImageCapture.OnImageCapturedCallback() {
            @SuppressLint("UnsafeExperimentalUsageError")
            override fun onCaptureSuccess(imageProxy: ImageProxy) {
                imageProxy.image?.let {
                    val bitmap = (previewView.getChildAt(0) as TextureView).bitmap
                    val scaledBitmap = ImageUtils.resizeBitmap(bitmap, 300) // scale down to 300px
                    val rotatedBitmap = ImageUtils.rotateBitmap(scaledBitmap, imageProxy.imageInfo.rotationDegrees.toFloat())

                    val result = ImageUtils.encodeBitmapToBase64String(rotatedBitmap)

                    // use the new bitmap
                    activity?.runOnUiThread {
                        this@CameraFragment.findNavController()
                                           .previousBackStackEntry
                                           ?.savedStateHandle
                                           ?.set(ICON_KEY, result)
                    }

                    super.onCaptureSuccess(imageProxy)
                    // free memory
                    bitmap.recycle()
                    scaledBitmap.recycle()
                    imageProxy.close()

                    // close the camera screen
                    if (isAdded) {
                        activity?.runOnUiThread { this@CameraFragment.findNavController().popBackStack() }
                    }
                }
            }
            override fun onError(exception: ImageCaptureException) {
                val msg = "Photo capture failed: ${exception.message}"
                previewView.post {
                    Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
                }

                // close the camera screen
                if (isAdded) {
                    activity?.runOnUiThread { this@CameraFragment.findNavController().popBackStack() }
                }
            }
        })
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all { permission ->
        context?.let { context ->
            ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
        } ?: false
    }
}