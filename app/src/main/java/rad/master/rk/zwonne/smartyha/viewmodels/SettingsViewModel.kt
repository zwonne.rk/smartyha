package rad.master.rk.zwonne.smartyha.viewmodels

import android.content.SharedPreferences
import android.content.res.Resources
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import com.snakydesign.livedataextensions.*
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.livedata.SharedPrefKeys.SHOW_SYNC_BACKEND_UI
import rad.master.rk.zwonne.smartyha.data.livedata.SharedPrefKeys.SYNC_AMAZON_ECHO_DOTS
import rad.master.rk.zwonne.smartyha.data.livedata.SharedPrefKeys.SYNC_ESTIMOTE_BEACONS
import rad.master.rk.zwonne.smartyha.data.livedata.SharedPrefKeys.SYNC_RASPBERRY_PI_SENSORS
import rad.master.rk.zwonne.smartyha.data.livedata.SharedPrefKeys.SYNC_TRADFRI_GATEWAYS
import rad.master.rk.zwonne.smartyha.data.livedata.SharedPrefKeys.SYNC_TRADFRI_LIGHT_BULBS
import rad.master.rk.zwonne.smartyha.data.livedata.SingleLiveEvent
import rad.master.rk.zwonne.smartyha.data.livedata.booleanLiveData
import rad.master.rk.zwonne.smartyha.data.remote.HostSelectionInterceptor
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.util.UrlUtils
import rad.master.rk.zwonne.smartyha.util.getDistinct
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import com.snakydesign.livedataextensions.map

class SettingsViewModel @Inject constructor(
        private val resources: Resources,
        private val userRepository: UserRepository,
        private val hostSelectionInterceptor: HostSelectionInterceptor,
        private val sharedPreferences: SharedPreferences
) : BaseViewModel(), LifecycleObserver {

    // ================ Backend Url related fields

    var urlHolder: ObservableField<UrlHolder> = ObservableField()

    private val _backendServiceUrlTxtChangedTrigger: PublishSubject<Unit> = PublishSubject.create()

    val _backendServiceUrlErrorMsg: LiveData<String?>

    val isBackendServiceUrlValid: LiveData<Boolean>

    val backendServiceUrlFromLocalDb = Transformations.switchMap(userRepository.user) { user ->
        // the result doesn't matter, this is only used to change the UrlHolder
        val result = MutableLiveData<Unit>()

        if (user?.urlHolder != null) {
            urlHolder.set(user.urlHolder!!)
        } else {
            urlHolder.set(UrlHolder())
        }

        result
    }

    // ================ Other fields

    val showSyncWithBackendUi = sharedPreferences.booleanLiveData(SHOW_SYNC_BACKEND_UI, false)

    val enableSyncWithBackendUiChkBox: LiveData<Boolean> = rad.master.rk.zwonne.smartyha.util.combineLatest(listOf(
            sharedPreferences.booleanLiveData(SYNC_TRADFRI_GATEWAYS, false),
            sharedPreferences.booleanLiveData(SYNC_TRADFRI_LIGHT_BULBS, false),
            sharedPreferences.booleanLiveData(SYNC_ESTIMOTE_BEACONS, false),
            sharedPreferences.booleanLiveData(SYNC_AMAZON_ECHO_DOTS, false),
            sharedPreferences.booleanLiveData(SYNC_RASPBERRY_PI_SENSORS, false))
    ) { syncValues ->
        return@combineLatest !syncValues.contains(true)
    }

    private val _saveBtnEnabled = SingleLiveEvent<Boolean>()

    var saveBtnEnabled: LiveData<Boolean>

    init {
        _saveBtnEnabled.value = false

        _backendServiceUrlErrorMsg = LiveDataReactiveStreams.fromPublisher(_backendServiceUrlTxtChangedTrigger.debounce(500, TimeUnit.MILLISECONDS)
                                                                                                              .subscribeOn(Schedulers.io())
                                                                                                              .switchMap {
                                                                                                                  return@switchMap Observable.fromCallable {
                                                                                                                      Timber.d("ViewModel.urlHolder: $urlHolder")
                                                                                                                      val immutableUrlHolder = urlHolder.get()?.copy() ?: UrlHolder()

                                                                                                                      val result = if (UrlUtils.isUrlValid(immutableUrlHolder.toString()) || immutableUrlHolder == UrlHolder()) {
                                                                                                                          ""
                                                                                                                      } else {
                                                                                                                          resources.getString(R.string.server_url_error_message)
                                                                                                                      }

                                                                                                                      Timber.d("backendUrl errorMessage: $result")
                                                                                                                      return@fromCallable result
                                                                                                                  }
                                                                                                              }
                                                                                                              .startWith("")
                                                                                                              .toFlowable(BackpressureStrategy.LATEST))

        isBackendServiceUrlValid = merge(listOf(combineLatest(first = _backendServiceUrlErrorMsg.map { errorMsg -> errorMsg!!.isBlank() },
                                                              second = LiveDataReactiveStreams.fromPublisher(_backendServiceUrlTxtChangedTrigger.toFlowable(BackpressureStrategy.LATEST)),
                                                              combineFunction = { first, _ -> return@combineLatest first!! && (urlHolder.get() != UrlHolder()) }).getDistinct(),
                                                _saveBtnEnabled))
                                      .getDistinct()

        // when new fields are added, use combineLatest() for all fields to enable/disable the button
        saveBtnEnabled = isBackendServiceUrlValid
    }

    fun onSaveBtnClicked() {
        Timber.d("onSaveBtnClicked: $urlHolder")
        addDisposable(Single.fromCallable {
                                val immutableUrlHolder = urlHolder.get()?.copy() ?: UrlHolder()
                                userRepository.updateHostSelectionInterceptor(hostSelectionInterceptor, immutableUrlHolder)
                                return@fromCallable userRepository.updateUserUrlHolderLocally(userRepository.user.value!!, immutableUrlHolder)
                            }
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                _saveBtnEnabled.value = false
                            }) {
                                _saveBtnEnabled.value = false
                            })
    }

    fun onUrlTextChanged() {
        Timber.d("onUrlTextChanged")
        _backendServiceUrlTxtChangedTrigger.onNext(Unit)
    }

    fun showSyncWithBackendUiChecked(isChecked: Boolean) {
        with (showSyncWithBackendUi) {
            sharedPreferences.edit()
                             .putBoolean(this.key, isChecked)
                             .apply()
        }
    }
}