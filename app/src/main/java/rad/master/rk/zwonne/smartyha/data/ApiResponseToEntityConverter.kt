package rad.master.rk.zwonne.smartyha.data

import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriGatewayFromResponse
import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriLightBulbFromResponse
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriLightBulb

object ApiResponseToEntityConverter {

    fun tradfriGatewayFromResponseToEntity(
            tradfriGatewayFromResponse: TradfriGatewayFromResponse
    ): TradfriGateway = TradfriGateway(tradfriGatewayFromResponse.psk,
                                       userId = tradfriGatewayFromResponse.uid,
                                       host = tradfriGatewayFromResponse.host,
                                       label = tradfriGatewayFromResponse.label,
                                       icon = tradfriGatewayFromResponse.icon)

    fun tradfriLightBulbFromResponseToEntity(
            tradfriLightBulbFromResponse: TradfriLightBulbFromResponse
    ): TradfriLightBulb {
        return TradfriLightBulb(tradfriLightBulbFromResponse.gatewayPsk,
                                tradfriLightBulbFromResponse.bulbId,
                                tradfriLightBulbFromResponse.uid,
                                tradfriLightBulbFromResponse.name,
                                tradfriLightBulbFromResponse.lastSeen,
                                tradfriLightBulbFromResponse.reachable,
                                tradfriLightBulbFromResponse.isOn == 1,
                                tradfriLightBulbFromResponse.dimmerValue.toFloat(),
                                tradfriLightBulbFromResponse.manufacturer,
                                tradfriLightBulbFromResponse.modelNumber,
                                tradfriLightBulbFromResponse.firmwareVersion)
    }
}