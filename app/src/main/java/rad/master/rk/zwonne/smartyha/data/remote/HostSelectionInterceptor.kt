package rad.master.rk.zwonne.smartyha.data.remote

import androidx.lifecycle.LiveData
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * An OkHttp interceptor class for a dynamic base url.
 */
@Singleton
class HostSelectionInterceptor @Inject constructor() : Interceptor {
    var scheme: String = ""
    var host: String = ""
    var port: String = ""

    lateinit var idToken: LiveData<String>

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        if (scheme.isNotEmpty() && host.isNotEmpty()) {
            val urlBuilder = request.url
                                    .newBuilder()
                                    .scheme(scheme)
                                    .host(host)
            if (port.isNotEmpty()) {
                try {
                    urlBuilder.port(port.toInt())
                } catch (error: NumberFormatException) {
                  Timber.e("$port is not a valid port number!")
                }
            }

            val requestBuilder = request.newBuilder()


            if (request.header("X-UserToken").isNullOrEmpty()) {
                idToken.value?.let {
                    requestBuilder.addHeader("X-UserToken", it)
                }
            }
            val newUrl: HttpUrl = urlBuilder.build()

            request = requestBuilder.url(newUrl)
                                    .build()
        }

        return chain.proceed(request)
    }
}
