package rad.master.rk.zwonne.smartyha.data.repositories

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GetTokenResult
import com.snakydesign.livedataextensions.combineLatest
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.livedata.FirebaseAuthLiveData
import rad.master.rk.zwonne.smartyha.data.remote.HostSelectionInterceptor
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.data.remote.requestmodels.RegisterUserRequest
import rad.master.rk.zwonne.smartyha.data.room.daos.UserDao
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.getDistinct
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
        private val localDataSource: UserDao,
        private val remoteDataSource: SmartyHAService,
        private val hostSelectionInterceptor: HostSelectionInterceptor,
        private val auth: FirebaseAuth
) {
    private val compositeDisposable = CompositeDisposable()

    private val _idToken: MutableLiveData<String> = MutableLiveData()

    private val idTokenListener = OnCompleteListener<GetTokenResult> { task ->
        Timber.d("idTokenListener.onComplete() task was successful: ${task.isSuccessful}")
        val resultToken = task.result?.token ?: run { "" }

        _idToken.value = if (task.isSuccessful) resultToken else ""
    }

    val idToken: LiveData<String> = _idToken

    var backendUrlHolderFromLogin: UrlHolder? = null

    val firebaseUser = FirebaseAuthLiveData(auth).getDistinct()

    val user: LiveData<User?> = Transformations.switchMap(firebaseUser) { firebaseUser ->
        if (firebaseUser != null) {
            Timber.d("user.Transformations.switchMap() FirebaseUser isn't null, email: ${firebaseUser.email}")

            // init the idToken
            firebaseUser.getIdToken(false)
                        .addOnCompleteListener(idTokenListener)

            return@switchMap localDataSource.getUserByIdLiveData(firebaseUser.uid)
        } else {
            Timber.d("user.Transformations.switchMap() FirebaseUser is null!")
            updateHostSelectionInterceptor(hostSelectionInterceptor, UrlHolder())
            _idToken.value = ""

            val result = MutableLiveData<User?>()
            result.value = null
            return@switchMap result
        }
    }.getDistinct()

    private val _updateUserTrigger = combineLatest(user, firebaseUser) { user, fbUser ->
        Timber.d("updateUserTrigger()")
        compositeDisposable.clear()
        val result = MutableLiveData<Unit>()
        result.value = Unit

        if (fbUser == null && user == null) {
            Timber.d("updateUserTrigger() firebase user is null, and user is null")
            updateHostSelectionInterceptor(hostSelectionInterceptor, UrlHolder())
        } else if (fbUser != null && user == null) {
            Timber.d("updateUserTrigger() firebase user is not null, but user is null")
            compositeDisposable.add(Completable.fromAction {
                                                    Timber.d("updateUserTrigger() User not found in local db")
                                                }.subscribeOn(Schedulers.io())
                                                .subscribe({
                                                    // Add a try-catch here since even when logging in with an existing account,
                                                    // combineLatest will emit a firebaseUser != null && user == null case, before
                                                    // emitting the correct one, so an exception will occur with insert to local db
                                                    try {
                                                        val newUser = User(fbUser.uid, fbUser.email!!, urlHolder = backendUrlHolderFromLogin)
                                                        updateHostSelectionInterceptor(hostSelectionInterceptor, backendUrlHolderFromLogin)
                                                        localDataSource.insertUser(newUser)
                                                    } catch (e: Exception) {
                                                        Timber.e("updateUserTrigger() error ${e.message}")
                                                    }
                                                },
                                                { error -> Timber.e("updateUserTrigger() error ${error.message}")}))
        } else {
            Timber.d("updateUserTrigger() neither firebase user, nor user are null")
            updateHostSelectionInterceptor(hostSelectionInterceptor, user!!.urlHolder)

            compositeDisposable.add(checkIfUserIsRegisteredOnBackend(user, localDataSource, remoteDataSource)
                                        .subscribeOn(Schedulers.io())
                                        .onErrorResumeNext(Single.fromCallable { user })
                                        .flatMap { usr ->
                                            registerUserOnBackendIfNeedBe(usr).onErrorResumeNext(Single.fromCallable { usr })
                                        }
                                        .doOnError { error -> Timber.e("updateUserTrigger() onError() ${error.message}") }
                                        .subscribe())
        }

        return@combineLatest result
    }.observeForever { }

    init {
        _idToken.value = ""
        hostSelectionInterceptor.idToken = idToken
    }

    fun updateUserUrlHolderLocally(user: User, urlHolder: UrlHolder): Int {
        Timber.d("updateUserUrlHolderLocally()")
        return localDataSource.updateUrlHolder(user.uid,
                                               urlHolder.scheme,
                                               urlHolder.schemeColonBackslash,
                                               urlHolder.host,
                                               urlHolder.portColon,
                                               urlHolder.port)
    }

    fun updateHostSelectionInterceptor(
            hostSelectionInterceptor: HostSelectionInterceptor,
            urlHolder: UrlHolder?) {
        Timber.d("updateHostSelectionInterceptor()")
        if (urlHolder != null) {
            hostSelectionInterceptor.scheme = urlHolder.scheme
            hostSelectionInterceptor.host = urlHolder.host
            hostSelectionInterceptor.port = urlHolder.port
        }
    }

    @VisibleForTesting
    fun checkIfUserIsRegisteredOnBackend(
            user: User,
            localDataSource: UserDao,
            remoteDataSource: SmartyHAService
    ): Single<User> {
        Timber.d("checkIfUserIsRegisteredOnBackend()")
            return if (user.registeredOnBackend) {
                Single.fromCallable {
                    Timber.d("checkIfUserIsRegisteredOnBackend() firebaseUser is already registered on backend")
                    user
                }
            } else {
                Timber.d("checkIfUserIsRegisteredOnBackend() firebaseUser is not registered on backend")
                // Check if the data is perhaps inconsistent
                return checkIfUserIsRegisteredOnBackendAndUpdateLocalDb(user, localDataSource, remoteDataSource)
            }
    }

    @VisibleForTesting
    fun checkIfUserIsRegisteredOnBackendAndUpdateLocalDb(
            user: User,
            localDataSource: UserDao,
            remoteDataSource: SmartyHAService
    ): Single<User> {
        return remoteDataSource.isUserRegistered(user.email)
                               .retry(3)
                               .map { isRegistered ->
                                   Timber.d("checkIfUserIsRegisteredOnBackendAndUpdateLocalDb() result: $isRegistered")
                                   localDataSource.updateRegisteredOnBackend(user.uid, isRegistered)
                                   user.apply {
                                       registeredOnBackend = isRegistered
                                   }
                               }
    }

    @VisibleForTesting
    fun registerUserOnBackendIfNeedBe(
            user: User
    ): Single<User> {
        Timber.d("registerUserOnBackendIfNeedBe()")
        return if (user.registeredOnBackend) {
            Single.fromCallable{
                Timber.d("registerUserOnBackendIfNeedBe() User is already registered on backend")
                user
            }
        } else {
            Timber.d("registerUserOnBackendIfNeedBe() User isn't registered on backend")
            remoteDataSource.registerUser(RegisterUserRequest(user.email))
                            .retry(3)
                            .map { registrationSuccessful ->
                                Timber.d("registerUserOnBackendIfNeedBe() response from api call: $registrationSuccessful")
                                localDataSource.updateRegisteredOnBackend(user.uid, true)
                                user.apply { registeredOnBackend = true }
                            }
        }
    }

    fun signOut() {
        Timber.d("signOut()")
        auth.signOut()
    }
}
