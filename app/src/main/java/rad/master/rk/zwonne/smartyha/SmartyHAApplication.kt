package rad.master.rk.zwonne.smartyha

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import rad.master.rk.zwonne.smartyha.dagger.DaggerAppComponent
import timber.log.Timber
import javax.inject.Inject

class SmartyHAApplication : Application(), HasAndroidInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        initTimber()

        initDagger()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initDagger() {
        DaggerAppComponent.builder()
                          .application(this)
                          .build()
                          .inject(this)
    }

    // this is required to setup Dagger2 for Activity
    override fun androidInjector(): AndroidInjector<Any> = activityInjector
}
