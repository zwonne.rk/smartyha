package rad.master.rk.zwonne.smartyha.ui.fragments

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout.GONE
import androidx.constraintlayout.widget.ConstraintLayout.VISIBLE
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_tradfri_gateway.*

import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.databinding.FragmentTradfriGatewayBinding
import rad.master.rk.zwonne.smartyha.viewmodels.TradfriGatewayViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel
import timber.log.Timber

class TradfriGatewayFragment : BaseFragment() {

    private val viewModel: TradfriGatewayViewModel by viewModels { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return initViewBinding(inflater, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subscribeToTradfriIcon()
    }

    override fun initViewModel(viewModel: BaseViewModel) {
        super.initViewModel(viewModel as TradfriGatewayViewModel)

        arguments?.let { bundle ->
            TradfriGatewayFragmentArgs.fromBundle(bundle)
                                      .tradfriGateway?.let { tradfriGateway ->
                                                          viewModel.tradfriGateway.value = tradfriGateway
                                                      }

            viewModel._is_edit.value = (TradfriGatewayFragmentArgs.fromBundle(bundle)
                                                                  .isEdit)
        }

        viewModel.enableAllUi.observe(viewLifecycleOwner, Observer { renderMainUi(it) })
        viewModel.showError.observe(viewLifecycleOwner, Observer { renderError() })
        viewModel.inputDataIsValid.observe(viewLifecycleOwner, Observer { saveButton.isEnabled = it })
    }

    private fun subscribeToTradfriIcon() {
        Timber.d("subscribeToTradfriIcon()")
        val navController = findNavController()

        navController.currentBackStackEntry
                    ?.savedStateHandle
                    ?.getLiveData<String>(CameraFragment.ICON_KEY)
                    ?.observe(viewLifecycleOwner) { result: String ->
                        viewModel.tradfriGateway.value = viewModel.tradfriGateway.value?.copy(icon = result)
                        viewModel.onIconChanged()
                    }
    }

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        // Inflate the layout for this fragment
        val binding: FragmentTradfriGatewayBinding = DataBindingUtil.inflate(inflater,
                                                                             R.layout.fragment_tradfri_gateway,
                                                                             container,
                                                                             false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        return binding.root
    }

    private fun renderMainUi(enableInput: Boolean) {
        Timber.d("renderMainUi($enableInput)")

        if (enableInput) renderContent() else renderLoading()

        enableUiElements(enableInput)
    }

    private fun enableUiElements(value: Boolean) {
        val inputType = when (value) {
            true -> InputType.TYPE_CLASS_TEXT
            false -> InputType.TYPE_NULL // block input while the screen is displaying Loading
        }

        securityCodeEt.isEnabled = value
        securityCodeEt.inputType = inputType

        hostEt.isEnabled = value
        hostEt.inputType = inputType

        identityEt.isEnabled = value
        identityEt.inputType = inputType

        labelEt.isEnabled = value
        labelEt.inputType = inputType

        saveButton.isEnabled = value
    }

    private fun renderContent() {
        Timber.d("renderContent()")
        loadingGroup.visibility = GONE
        contentGroup.visibility = VISIBLE
    }

    private fun renderLoading() {
        Timber.d("renderLoading()")
        contentGroup.visibility = GONE
        loadingGroup.visibility = VISIBLE
    }

    private fun renderError() {
        Timber.d("renderError()")
        Toast.makeText(activity, R.string.tradfri_gateway_fragment_error_text, Toast.LENGTH_LONG)
             .show()
    }

    override fun getViewModel(): BaseViewModel = viewModel
}
