package rad.master.rk.zwonne.smartyha.util

import androidx.core.util.PatternsCompat

interface UrlUtils {
    companion object {
        @JvmStatic
        fun isUrlValid(url: String): Boolean {
            var result = false
            val schemeAndHost = url.split("://")

            if (schemeAndHost.size == 2) {
                if (isSchemeValid(schemeAndHost[0])) {
                    val hostAndPort = schemeAndHost[1].split(":")
                    result = if (hostAndPort.size == 2) {
                        isHostValid(hostAndPort[0]) && isPortValid(hostAndPort[1]) && hostAndPort[1].isNotBlank()
                    } else {
                        isHostValid(schemeAndHost[1])
                    }
                }
            }

            return result
        }

        @JvmStatic
        fun isSchemeValid(scheme: String): Boolean {
            return scheme == "http" || scheme == "https"
        }

        @JvmStatic
        fun isHostValid(host: String): Boolean {
            return PatternsCompat.DOMAIN_NAME
                    .matcher(host.toLowerCase())
                    .matches()
        }

        @JvmStatic
        fun isPortValid(port: String): Boolean {
            return port.matches("\\d{0,5}".toRegex())
        }
    }
}