package rad.master.rk.zwonne.smartyha.dagger

import android.app.Application
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import dagger.Reusable
import rad.master.rk.zwonne.smartyha.util.CoroutineDispatcherProvider
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class, LocalStorageModule::class, ApiHelpersModule::class])
internal open class AppModule {

    @Singleton
    @Provides
    fun provideResources(app: Application): Resources {
        return app.resources
    }

    @Reusable
    @Provides
    open fun provideCoroutineDispatcherProvider(): CoroutineDispatcherProvider {
        return CoroutineDispatcherProvider()
    }
}