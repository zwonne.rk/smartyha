package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.lifecycle.*
import com.snakydesign.livedataextensions.combineLatest
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.livedata.SingleLiveEvent
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import timber.log.Timber
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val userRepository: UserRepository): BaseViewModel(), LifecycleObserver {

    var backendUrlHolder: UrlHolder? = null

    private val _firebaseUiLoginEvent = SingleLiveEvent<FirebaseUiLoginEvent?>()

    val firebaseUiLoginEvent: LiveData<FirebaseUiLoginEvent?>
        get() = _firebaseUiLoginEvent

    val firebaseUiLoginSuccessEvent = combineLatest(userRepository.firebaseUser, firebaseUiLoginEvent) { fbUser, loginEvent ->
        if (fbUser == null && loginEvent is FirebaseUiLoginEvent.FirebaseLoginSuccess) {
            Timber.d("firebaseUiLoginSuccessEvent: login success has arrived, but firebase user is still null!")
            FirebaseUiLoginEvent.FirebaseLoginStarted
        } else {
            Timber.d("firebaseUiLoginSuccessEvent: login event $loginEvent")
            Timber.d("firebaseUiLoginSuccessEvent: user is null: ${fbUser == null}")
            loginEvent
        }
    }

    fun setFirebaseUiLoginEvent(firebaseUiLoginEvent: FirebaseUiLoginEvent) {
        _firebaseUiLoginEvent.value = firebaseUiLoginEvent
    }

    fun onRetryLoginBtnClicked() {
        _firebaseUiLoginEvent.value = FirebaseUiLoginEvent.FirebaseLoginStarted
    }

    sealed class FirebaseUiLoginEvent {
        object FirebaseLoginStarted : FirebaseUiLoginEvent()
        object FirebaseLoginSuccess : FirebaseUiLoginEvent()
        data class FirebaseLoginFailure(val errorCode: Int?) : FirebaseUiLoginEvent()
    }
}