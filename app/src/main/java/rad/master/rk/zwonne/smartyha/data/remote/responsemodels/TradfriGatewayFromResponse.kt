package rad.master.rk.zwonne.smartyha.data.remote.responsemodels

import com.google.gson.annotations.SerializedName

data class TradfriGatewayFromResponse constructor(
        val psk: String,
        @SerializedName("owner_uid") val uid: String,
        var host: String = "",
        val identity: String,
        var label: String = "",
        var icon: String? = null
)