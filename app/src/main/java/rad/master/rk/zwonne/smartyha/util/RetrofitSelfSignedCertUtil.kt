package rad.master.rk.zwonne.smartyha.util

import android.app.Application
import java.security.cert.Certificate
import javax.inject.Singleton
import android.content.Context
import rad.master.rk.zwonne.smartyha.R
import java.security.KeyStore
import java.security.cert.CertificateFactory
import javax.inject.Inject
import javax.net.ssl.*

@Singleton
class RetrofitSelfSignedCertUtil @Inject constructor(val app: Application) {

    /**
     * Loads CAs from an [InputStream], and returns them.
     */
    private fun generateCertificateInstance(context: Context): Certificate? {
        val cf = CertificateFactory.getInstance("X.509")
        val cert = context.resources.openRawResource(R.raw.smartyha)
        var ca: Certificate? = null

        try {
            ca = cf.generateCertificate(cert)
        } finally {
            cert.close()
            return ca
        }
    }

    /**
     * Generates a keystore using the trusted CAs in the method param.
     */
    private fun generateKeystore(ca: Certificate?): KeyStore {
        val keyStoreType = KeyStore.getDefaultType()
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null, null)
        keyStore.setCertificateEntry("ca", ca)

        return keyStore
    }

    /**
     * Generates a trust manager that trusts the CAs in the provided keystore.
     */
    private fun generateTrustManager(keyStore: KeyStore): X509TrustManager {
        val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
        val tmf = TrustManagerFactory.getInstance(tmfAlgorithm)
        tmf.init(keyStore)
        val trustManagers = tmf.trustManagers

        return trustManagers[0] as X509TrustManager
    }

    /**
     * Generates an SSLSocketFactory that uses the provided trust manager
     */
    private fun generateSslSocketFactory(trustManager: X509TrustManager): SSLSocketFactory {
        val sslContext = SSLContext.getInstance("TLS")
        var trustManagers = Array<TrustManager>(1){ trustManager }
        sslContext.init(null, trustManagers, null)

        return sslContext.socketFactory
    }

    fun getSslSocketFactoryAndTrustManager(): Pair<SSLSocketFactory, X509TrustManager> {
        val trustManager = generateTrustManager(generateKeystore(generateCertificateInstance(app.applicationContext)))
        val sslSocketFactory = generateSslSocketFactory(trustManager)

        return Pair(sslSocketFactory, trustManager)
    }

}
