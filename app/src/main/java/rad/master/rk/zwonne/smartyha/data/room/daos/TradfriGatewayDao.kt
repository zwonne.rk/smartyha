package rad.master.rk.zwonne.smartyha.data.room.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Single
import kotlinx.coroutines.flow.Flow
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway

@Dao
interface TradfriGatewayDao: BaseDeviceDao<TradfriGateway> {

    /**
     * Returns a list of all Tradfri Gateways linked to the specified user
     */
    @Query("SELECT * FROM tradfri_gateways WHERE user_id == :uid")
    fun getGatewaysByUserIdLiveData(uid: String): LiveData<Array<TradfriGateway>>

    /**
     * Returns a list of all Tradfri Gateways linked to the specified user
     */
    @Query("SELECT * FROM tradfri_gateways WHERE user_id == :uid")
    fun getGatewaysByUserIdFlow(uid: String): Flow<Array<TradfriGateway>>

    @Query("UPDATE tradfri_gateways SET host = :host, label = :label WHERE psk == :psk")
    fun updateTradfriGateway(psk: String, host: String, label:String): Single<Int>
}