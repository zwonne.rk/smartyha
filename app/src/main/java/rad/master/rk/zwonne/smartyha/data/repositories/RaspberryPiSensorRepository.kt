package rad.master.rk.zwonne.smartyha.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import rad.master.rk.zwonne.smartyha.data.room.entities.Device
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RaspberryPiSensorRepository @Inject constructor(private val userRepository: UserRepository) {
    val user = userRepository.user

    val idToken = userRepository.idToken

    val sensors: LiveData<Array<Device>> = MutableLiveData()
}