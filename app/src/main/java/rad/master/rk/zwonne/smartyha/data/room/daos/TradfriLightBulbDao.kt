package rad.master.rk.zwonne.smartyha.data.room.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import kotlinx.coroutines.flow.Flow
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriLightBulb

@Dao
interface TradfriLightBulbDao: BaseDeviceDao<TradfriLightBulb> {

    /**
     * Returns a list of all Tradfri LightBulbs linked to the specified gateway
     */
    @Query("SELECT * FROM tradfri_light_bulbs WHERE gateway_psk == :psk")
    fun getLightBulbsByGateWayPsk(psk: String): Flow<List<TradfriLightBulb>>

    @Query("SELECT * FROM tradfri_light_bulbs WHERE gateway_psk == :psk AND bulb_id == :id")
    fun getLightBulbById(psk: String, id: Int): LiveData<TradfriLightBulb>
}