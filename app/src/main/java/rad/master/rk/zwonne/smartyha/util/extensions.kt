package rad.master.rk.zwonne.smartyha.util

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import java.io.Serializable

// ***********************************************************************************************
// ************************** Simplifying starting new activities ********************************
// ***********************************************************************************************
inline fun <reified T> AppCompatActivity?.startActivityWithData(vararg pairs: Pair<String, Any?>) {
    this ?: return
    val intent = Intent(this, T::class.java)
    fillTheIntent(intent, pairs)
    this.startActivity(intent)
}

inline fun <reified T> Fragment?.startActivityWithData(vararg pairs: Pair<String, Any?>) {
    this ?: return
    this.activity.startActivityWithData<T>(*pairs)
}

inline fun <reified T> Context?.startActivityWithData(vararg pairs: Pair<String, Any?>) {
    this ?: return
    val intent = Intent(this, T::class.java)
    fillTheIntent(intent, pairs)
    this.startActivity(intent)
}

fun fillTheIntent(intent: Intent, pairs: Array<out Pair<String, Any?>>) {
    pairs.forEach {
        val value = it.second
        when (value) {
            null -> intent.putExtra(it.first, null as Serializable?)
            is Int -> intent.putExtra(it.first, value)
            is Long -> intent.putExtra(it.first, value)
            is CharSequence -> intent.putExtra(it.first, value)
            is String -> intent.putExtra(it.first, value)
            is Float -> intent.putExtra(it.first, value)
            is Double -> intent.putExtra(it.first, value)
            is Char -> intent.putExtra(it.first, value)
            is Short -> intent.putExtra(it.first, value)
            is Boolean -> intent.putExtra(it.first, value)
            is Serializable -> intent.putExtra(it.first, value)
            is Bundle -> intent.putExtra(it.first, value)
            is Parcelable -> intent.putExtra(it.first, value)

            is Array<*> -> when {
                value.isArrayOf<CharSequence>() -> intent.putExtra(it.first, value)
                value.isArrayOf<String>() -> intent.putExtra(it.first, value)
                value.isArrayOf<Parcelable>() -> intent.putExtra(it.first, value)
            }

            is IntArray -> intent.putExtra(it.first, value)
            is LongArray -> intent.putExtra(it.first, value)
            is FloatArray -> intent.putExtra(it.first, value)
            is DoubleArray -> intent.putExtra(it.first, value)
            is CharArray -> intent.putExtra(it.first, value)
            is ShortArray -> intent.putExtra(it.first, value)
            is BooleanArray -> intent.putExtra(it.first, value)
            else -> {   // ignore }
            }
        }
    }
}

// ***********************************************************************************************
// **************** Combining any number of live datas to mediator live data *********************
// ***********************************************************************************************

inline fun <T> dependantLiveData(vararg dependencies: LiveData<*>, crossinline mapper: () -> T?): MediatorLiveData<T> {
    val result = MediatorLiveData<T>()

    val observer = Observer<Any> { result.value = mapper() }
    dependencies.forEach { dependencyLiveData ->
        result.addSource(dependencyLiveData, observer)
    }

    return result
}

// ***********************************************************************************************
// ******************************* Getting distinct live data ************************************
// ***********************************************************************************************

fun <T> LiveData<T>.getDistinct(): LiveData<T> {
    val distinctLiveData = MediatorLiveData<T>()

    distinctLiveData.addSource(this, object : Observer<T> {
        private var initialized = false
        private var lastObj: T? = null
        override fun onChanged(obj: T?) {
            if (!initialized) {
                initialized = true
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            } else if ((obj == null && lastObj != null)
                    || obj != lastObj) {
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            }
        }
    })
    return distinctLiveData
}

// ***********************************************************************************************
// ********************************** Hiding soft keyboard ***************************************
// ***********************************************************************************************

fun AppCompatActivity.hideSoftKeyboard() {
    if (currentFocus != null) {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }
}

fun Fragment.hideSoftKeyboard() {
    activity?.let {
        (it as AppCompatActivity).hideSoftKeyboard()
    }
}

// ***********************************************************************************************
// ************************************* Flatten arrays ******************************************
// ***********************************************************************************************

/**
 * Returns a single list of all elements from all arrays in the given array.
 */
fun <T> List<List<T>?>.flattenNonNull(): List<T> {
    val result = mutableListOf<T>()
    for (childList in this) {
        childList?.let {
            result.addAll(it)
        }
    }
    return result
}