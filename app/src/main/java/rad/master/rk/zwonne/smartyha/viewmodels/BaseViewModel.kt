package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import rad.master.rk.zwonne.smartyha.data.livedata.SingleLiveEvent
import rad.master.rk.zwonne.smartyha.util.commands.NavigationCommand
import rad.master.rk.zwonne.smartyha.util.commands.AlertDialogCommand

/**
 * This class represents a BaseViewModel that holds the
 * navigation commands which Fragments listen to
 */
open class BaseViewModel : ViewModel(), LifecycleObserver {

    val navigationCommands = SingleLiveEvent<NavigationCommand>()

    val showDialogCommand = SingleLiveEvent<AlertDialogCommand>()

    private val compositeDisposable = CompositeDisposable()

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    fun navigate(directions: NavDirections) {
        navigationCommands.postValue(NavigationCommand.To(directions))
    }

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        compositeDisposable.clear()
    }
}