package rad.master.rk.zwonne.smartyha.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.LogoutViewModel

class LogoutFragment : BaseFragment() {

    private val viewModel: LogoutViewModel by viewModels { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_logout, container, false)
    }

    override fun initViewModel(viewModel: BaseViewModel) {
        super.initViewModel(viewModel as LogoutViewModel)

        // must be observed to trigger business logic in ViewModel
        viewModel.isLoggedOut.observe(viewLifecycleOwner, Observer { })
    }

    override fun navigate(navDirections: NavDirections) {
        NavHostFragment.findNavController(this)
                       .navigate(navDirections)
    }

    override fun getViewModel(): BaseViewModel {
        return viewModel
    }
}
