package rad.master.rk.zwonne.smartyha.viewmodels

import android.content.res.Resources
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.livedata.SingleLiveEvent
import rad.master.rk.zwonne.smartyha.data.remote.HostSelectionInterceptor
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.util.UrlUtils
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BackendUrlViewModel @Inject constructor(
        private val resources: Resources,
        private val hostSelectionInterceptor: HostSelectionInterceptor,
        private val remoteDataSource: SmartyHAService
) : BaseViewModel(), LifecycleObserver {

    var urlHolder = UrlHolder(scheme = resources.getString(R.string.backend_url_fragment_https_label))

    private val _schemeChangedTrigger: PublishSubject<Unit> = PublishSubject.create()

    private val _hostTxtChangedTrigger: PublishSubject<Unit> = PublishSubject.create()

    private val _portTxtChangedTrigger: PublishSubject<Unit> = PublishSubject.create()

    val showPortUi = MutableLiveData<Boolean>()

    val openLoginScreenOrErrorEvent = SingleLiveEvent<BackendUrlEvents>()

    val enableTryConnectionBtn: LiveData<Boolean> =
            LiveDataReactiveStreams.fromPublisher(Observable.merge(
                                                                _schemeChangedTrigger,
                                                                _hostTxtChangedTrigger.debounce(500, TimeUnit.MILLISECONDS),
                                                                _portTxtChangedTrigger.debounce(500, TimeUnit.MILLISECONDS))
                                                            .subscribeOn(Schedulers.io())
                                                            .switchMap { Observable.fromCallable { UrlUtils.isUrlValid(urlHolder.copy().toString()) } }
                                                            .toFlowable(BackpressureStrategy.LATEST))
    init {
        showPortUi.value = false
    }

    fun onShowPortUiChanged(isChecked: Boolean) {
        showPortUi.value = isChecked
    }

    @VisibleForTesting
    fun setupHostSelectionInterceptor(hostSelectionInterceptor: HostSelectionInterceptor) {
        Timber.d("setupHostSelectionInterceptor()")
        val immutableUrlHolder = urlHolder.copy()
        hostSelectionInterceptor.scheme = immutableUrlHolder.scheme
        hostSelectionInterceptor.host = immutableUrlHolder.host
        hostSelectionInterceptor.port = immutableUrlHolder.port
    }

    fun onTryConnectionBtnClicked() {
        Timber.d("onTryConnectionBtnClicked()")
        val immutableUrlHolder = urlHolder.copy()
        addDisposable(Single.fromCallable {
                                 openLoginScreenOrErrorEvent.postValue(BackendUrlEvents.LoadingEvent)
                                 setupHostSelectionInterceptor(hostSelectionInterceptor)
                             }
                             .subscribeOn(Schedulers.io())
                             .flatMap { remoteDataSource.isServerAvailable() }
                             .delay(1, TimeUnit.SECONDS)
                             .subscribe({
                                 Timber.d("onTryConnectionBtnClicked() onSuccess()")
                                 openLoginScreenOrErrorEvent.postValue(BackendUrlEvents.OpenLoginScreenEvent(immutableUrlHolder))
                             }, { error ->
                                 Timber.e("onTryConnectionBtnClicked() onError(): ${error.message}")
                                 openLoginScreenOrErrorEvent.postValue(BackendUrlEvents.ErrorEvent(error, showPortUi.value!!))
                             }))
    }

    fun onAlreadyHaveAccBtnClicked() {
        Timber.d("onAlreadyHaveAccBtnClicked()")
        openLoginScreenOrErrorEvent.value = BackendUrlEvents.OpenLoginScreenEvent(null)
    }

    fun onSchemeChanged(checkedId: Int) {
        val stringResId = if (checkedId == R.id.httpsRadioButton) R.string.backend_url_fragment_https_label else R.string.backend_url_fragment_http_label
        urlHolder.scheme = resources.getString(stringResId)
        _schemeChangedTrigger.onNext(Unit)
    }

    fun onHostTextChanged() {
        Timber.d("onHostTextChanged()")
        _hostTxtChangedTrigger.onNext(Unit)
    }

    fun onPortTextChanged() {
        Timber.d("onPortTextChanged()")
        _portTxtChangedTrigger.onNext(Unit)
    }

    sealed class BackendUrlEvents {
        object LoadingEvent : BackendUrlEvents()
        data class OpenLoginScreenEvent(val url: UrlHolder?) : BackendUrlEvents()
        data class ErrorEvent(val error: Throwable, val portUiWasVisible: Boolean) : BackendUrlEvents()
    }
}
