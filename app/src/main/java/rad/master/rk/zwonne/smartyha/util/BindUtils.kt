package rad.master.rk.zwonne.smartyha.util

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.google.android.material.slider.Slider
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.room.entities.Device
import rad.master.rk.zwonne.smartyha.util.ImageUtils.Companion.decodeBase64StringToBitmap

class BindUtils {

    companion object Converter: UrlUtils, ImageUtils {

        @BindingAdapter("android:text")
        @JvmStatic fun setUrlHolder(view: TextInputEditText, newValue: UrlHolder?) {
            val urlString = newValue?.toString() ?: ""
            // Important to break potential infinite loops.
            if (view.text.toString() != urlString) {
                view.setText(urlString)
                view.setSelection(view.text!!.length)
            }
        }

        @InverseBindingAdapter(attribute = "android:text")
        @JvmStatic fun getUrlHolder(view: TextInputEditText) : UrlHolder {
            val result = UrlHolder()

            val url = view.text.toString()

            val colonBackSlashStringBuilder = StringBuilder("")

            if (url.isNotBlank()) {
                // setup scheme
                val splitByFirstColon = url.split(":")
                if (url.indexOf(':') != -1) {
                    result.scheme = splitByFirstColon[0]
                    colonBackSlashStringBuilder.append(':')
                } else {
                    result.scheme = url
                    return result
                }

                if (splitByFirstColon[1].startsWith("/")) {
                    colonBackSlashStringBuilder.append('/')
                    if (splitByFirstColon[1].startsWith("//")) {
                        colonBackSlashStringBuilder.append('/')
                        result.schemeColonBackslash = colonBackSlashStringBuilder.toString()
                    } else {
                        result.schemeColonBackslash = colonBackSlashStringBuilder.toString()
                        result.host = splitByFirstColon[1].drop(1)
                        return result
                    }
                } else {
                    result.schemeColonBackslash = colonBackSlashStringBuilder.toString()
                    result.host = splitByFirstColon[1]
                    return result
                }

                val hostAndPort = url.split("://")[1]
                if (hostAndPort.indexOf(':') != -1) {
                    result.host = hostAndPort.split(':')[0]
                    result.portColon = ":"
                    result.port = hostAndPort.split(':')[1]
                } else {
                    result.host = hostAndPort
                }
            }

            return result
        }

        @BindingAdapter("app:errorMessage")
        @JvmStatic fun setErrorMessage(view: TextInputLayout, errorMessage: String?) {
            view.error = if (errorMessage.isNullOrBlank()) null else errorMessage
        }

        @BindingAdapter("android:visibility")
        @JvmStatic fun setVisibility(view: View, isVisible: Boolean) {
            view.visibility = if (isVisible) View.VISIBLE else View.GONE
        }

        @BindingAdapter("app:srcCompat")
        @JvmStatic fun setTradfriGatewayIcon(view: AppCompatImageView, tradfriGateway: Device.TradfriGateway?) {
            val defaultImage = R.drawable.ic_default_tradfri_gateway

            if (tradfriGateway != null) {
                tradfriGateway.getImage()?.let {
                    view.setImageBitmap(decodeBase64StringToBitmap(it))
                } ?: run {
                    view.setImageResource(defaultImage)
                }
            } else {
                view.setImageResource(defaultImage)
            }
        }

        @BindingAdapter("app:srcCompat")
        @JvmStatic fun setTradfriLightBulbIcon(view: AppCompatImageView, tradfriLightBubl: Device.TradfriLightBulb?) {
            val defaultImage = R.drawable.ic_default_tradfri_light_bulb

            if (tradfriLightBubl != null) {
                tradfriLightBubl.getImage()?.let {
                    view.setImageBitmap(decodeBase64StringToBitmap(it))
                } ?: run {
                    view.setImageResource(defaultImage)
                }
            } else {
                view.setImageResource(defaultImage)
            }
        }

        @InverseBindingAdapter(attribute = "android:value")
        @JvmStatic fun getSliderValue(slider: Slider) = slider.value

        @BindingAdapter( "android:valueAttrChanged")
        @JvmStatic fun setSliderListeners(slider: Slider, attrChange: InverseBindingListener) {
            slider.addOnChangeListener { _, _, _ ->
                attrChange.onChange()
            }
        }
    }
}