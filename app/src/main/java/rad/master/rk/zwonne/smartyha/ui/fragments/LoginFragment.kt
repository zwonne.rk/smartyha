package rad.master.rk.zwonne.smartyha.ui.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout.GONE
import androidx.constraintlayout.widget.ConstraintLayout.VISIBLE
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionManager
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import kotlinx.android.synthetic.main.fragment_login.*
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.databinding.FragmentLoginBinding
import rad.master.rk.zwonne.smartyha.ui.activities.MainActivity
import rad.master.rk.zwonne.smartyha.viewmodels.LoginViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginStarted
import rad.master.rk.zwonne.smartyha.viewmodels.LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginSuccess
import rad.master.rk.zwonne.smartyha.viewmodels.LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginFailure
import rad.master.rk.zwonne.smartyha.ui.fragments.LoginFragmentDirections.actionLoginToDashboard
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel
import timber.log.Timber

class LoginFragment : BaseFragment() {

    private val viewModel: LoginViewModel by viewModels { viewModelFactory }

    private val RC_SIGN_IN: Int = 123

    // Choose authentication loginProviders
    private val loginProviders = arrayListOf(
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.EmailBuilder().build()
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        processSavedInstance(savedInstanceState)
        return initViewBinding(inflater, container)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.d("onActivityResult()")
        if (requestCode == RC_SIGN_IN) {
            // The result from Firebase Auth Ui chooser activity
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                triggerLoginSuccessEvent()
            } else {
                triggerLoginFailureEvent(response?.error?.errorCode)
            }
        }
    }

    override fun initViewModel(viewModel: BaseViewModel) {
        super.initViewModel(viewModel as LoginViewModel)

        arguments?.let {
            viewModel.backendUrlHolder = LoginFragmentArgs.fromBundle(it)
                                                          .urlHolder
        }

        viewModel.firebaseUiLoginEvent.observe(viewLifecycleOwner, Observer { firebaseUiLoginEvent ->
            Timber.d("initViewModel() firebaseUiLoginEvent arrived: $firebaseUiLoginEvent")
            if (firebaseUiLoginEvent == null) {
                return@Observer
            }

            if (firebaseUiLoginEvent is FirebaseLoginStarted) {
                renderLoginStarted()
            } else if (firebaseUiLoginEvent is FirebaseLoginFailure) {
                handleLoginFailure(firebaseUiLoginEvent.errorCode)
            }
        })

        viewModel.firebaseUiLoginSuccessEvent.observe(viewLifecycleOwner, Observer { firebaseUiLoginEvent ->
            Timber.d("initViewModel() firebaseUiLoginEvent arrived: $firebaseUiLoginEvent")
            if (firebaseUiLoginEvent == null) {
                return@Observer
            }

            if (firebaseUiLoginEvent is FirebaseLoginSuccess) {
                handleLoginSuccess()
            }
        })
    }

    private fun processSavedInstance(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            // Activity started first time
            triggerLoginStartedEvent()
        } else {
            // Configuration change
            showHideLoginRetryButtonAndProgressUi(true)
        }
    }

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        val binding: FragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        return binding.root
    }

    private fun startChooserActivity() {
        Timber.d("startChooserActivity()")
        startActivityForResult(AuthUI.getInstance()
                                     .createSignInIntentBuilder()
                                     .setAvailableProviders(loginProviders)
                                     .setTheme(R.style.LoginTheme)
                                     .setLogo(R.drawable.ic_app_logo)
                                     .build(),
                               RC_SIGN_IN)
    }

    private fun triggerLoginStartedEvent() {
        viewModel.setFirebaseUiLoginEvent(FirebaseLoginStarted)
    }

    private fun triggerLoginSuccessEvent() {
        viewModel.setFirebaseUiLoginEvent(FirebaseLoginSuccess)
    }

    private fun triggerLoginFailureEvent(errorCode: Int?) {
        viewModel.setFirebaseUiLoginEvent(FirebaseLoginFailure(errorCode))
    }

    private fun renderLoginStarted() {
        Timber.d("renderLoginStarted()")
        showHideLoginRetryButtonAndProgressUi(false)
        startChooserActivity()
    }

    private fun handleLoginSuccess() {
        Timber.d("handleLoginSuccess()")
        showHideLoginRetryButtonAndProgressUi(false)

        val navDirections = actionLoginToDashboard()
        findNavController().navigate(navDirections.apply {
                                        urlHolder = viewModel.backendUrlHolder
                                     })
        (activity as MainActivity).showActionBar()
    }

    private fun handleLoginFailure(error: Int?) {
        Timber.e("handleLoginFailure($error)")
        showHideLoginRetryButtonAndProgressUi(true)

        if (error == null) {
            activity?.finish()
            return
        }

        var toastErrorMessage =  resources.getString(R.string.login_fragment_unknown_error)

        // Sign in failed. If response is null the user canceled the
        // sign-in flow using the back button.
        val logMessage = when (error) {
            ErrorCodes.NO_NETWORK -> {
                toastErrorMessage = resources.getString(R.string.login_fragment_connection_error)
                "handleLoginFailure(NO_NETWORK)"
            }
            ErrorCodes.PROVIDER_ERROR -> {
                toastErrorMessage = resources.getString(R.string.login_fragment_provider_error)
                "handleLoginFailure(PROVIDER_ERROR)"
            }
            ErrorCodes.UNKNOWN_ERROR -> "handleLoginFailure(UNKNOWN_ERROR)"
            ErrorCodes.PLAY_SERVICES_UPDATE_CANCELLED -> "handleLoginFailure(PLAY_SERVICES_UPDATE_CANCELLED)"
            ErrorCodes.DEVELOPER_ERROR -> "handleLoginFailure(DEVELOPER_ERROR)"
            ErrorCodes.ANONYMOUS_UPGRADE_MERGE_CONFLICT -> "handleLoginFailure(ANONYMOUS_UPGRADE_MERGE_CONFLICT)"
            ErrorCodes.EMAIL_MISMATCH_ERROR -> "handleLoginFailure(EMAIL_MISMATCH_ERROR)"
            else ->  "handleLoginFailure()"
        }

        Timber.e(logMessage)
        Toast.makeText(activity, toastErrorMessage, Toast.LENGTH_SHORT)
             .show()
    }

    private fun showHideLoginRetryButtonAndProgressUi(hideProgressView: Boolean) {
        val constraintSet = ConstraintSet()
        constraintSet.clone(rootLayout)

        if (hideProgressView) {
            constraintSet.setVisibility(R.id.loadingGroup, GONE)
            constraintSet.setVisibility(R.id.retryLoginBtn, VISIBLE)
        } else {
            constraintSet.setVisibility(R.id.retryLoginBtn, GONE)
            constraintSet.setVisibility(R.id.loadingGroup, VISIBLE)
        }

        TransitionManager.beginDelayedTransition(rootLayout)
        constraintSet.applyTo(rootLayout)
    }

    override fun getViewModel(): BaseViewModel = viewModel
}
