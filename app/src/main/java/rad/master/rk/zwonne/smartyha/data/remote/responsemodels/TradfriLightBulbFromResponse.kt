package rad.master.rk.zwonne.smartyha.data.remote.responsemodels

import com.google.gson.annotations.SerializedName

data class TradfriLightBulbFromResponse constructor(
        @SerializedName("gateway_psk") val gatewayPsk: String,
        @SerializedName("bulb_id") val bulbId: Int,
        @SerializedName("user_id") val uid: String,
        val name: String,
        @SerializedName("last_seen") val lastSeen: Int,
        val reachable: Boolean,
        @SerializedName("is_on") val isOn: Int,
        @SerializedName("dimmer_value") val dimmerValue: Int,
        val manufacturer: String,
        @SerializedName("model_number") val modelNumber: String,
        @SerializedName("firmware_version") val firmwareVersion: String
)