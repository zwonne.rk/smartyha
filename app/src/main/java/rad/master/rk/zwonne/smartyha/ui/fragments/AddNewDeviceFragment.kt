package rad.master.rk.zwonne.smartyha.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import dagger.android.support.AndroidSupportInjection

import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.databinding.FragmentAddNewDeviceBinding
import rad.master.rk.zwonne.smartyha.util.commands.NavigationCommand
import rad.master.rk.zwonne.smartyha.viewmodels.AddNewDeviceViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.BaseViewModel
import rad.master.rk.zwonne.smartyha.viewmodels.ViewModelFactory
import timber.log.Timber
import javax.inject.Inject

class AddNewDeviceFragment : DialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel: AddNewDeviceViewModel by viewModels { viewModelFactory }

    companion object {
        fun newInstance(): AddNewDeviceFragment {
            return AddNewDeviceFragment()
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return initViewBinding(inflater, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel(viewModel)
    }

    /**
     * Needs to be called after onCreateView(), since a crash will occur otherwise
     * when accessing viewLifecycleOwner
     */
    private fun initViewModel(viewModel: AddNewDeviceViewModel) {
        Timber.d("initViewModel()")
        lifecycle.addObserver(viewModel)

        subscribeToNavigationEvent(viewModel)
    }

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        Timber.d("initViewBinding()")
        // Inflate the layout for this fragment
        val binding: FragmentAddNewDeviceBinding = DataBindingUtil.inflate(inflater,
                                                                           R.layout.fragment_add_new_device,
                                                                           container,
                                                                           false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        return binding.root
    }

    /**
     * Subscribe to ViewModel's navigationCommands filed here
     * if needed.
     */
    private fun subscribeToNavigationEvent(viewModel: BaseViewModel) {
        viewModel.navigationCommands
                 .observe(viewLifecycleOwner, Observer { command ->
                     when (command) {
                         is NavigationCommand.To -> navigate(command.directions)
                     }
                 })
    }

    private fun navigate(navDirections: NavDirections) {
        findNavController().navigate(navDirections)
    }
}
