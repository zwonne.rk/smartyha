package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.Exception
import java.lang.RuntimeException
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

/**
 * Dagger will create a key-value map in compile time, and will provide it to this factory.
 * The key is a class type, i.e. MainViewModel::class.
 * The value is provider of a ViewModel instance, i.e. MainViewModel(datasource)
 */
@Singleton
class ViewModelFactory @Inject constructor(private val creators: Map<Class<out ViewModel>,
                                                                     @JvmSuppressWildcards Provider<ViewModel>>):
        ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val creator = creators[modelClass] ?:
        // if not null get map entries
        // if the class is assignable from the entry.key,
        // assign the entry.value to the 'creator' local var
        // else throw an exception
        creators.asIterable()
                .firstOrNull {
                    modelClass.isAssignableFrom(it.key)
                }?.value ?: throw IllegalArgumentException("Unknown model class $modelClass")

        return try {
            creator.get() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}
