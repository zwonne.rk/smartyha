package rad.master.rk.zwonne.smartyha.data.repositories

import android.content.SharedPreferences
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import com.snakydesign.livedataextensions.zip
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.*
import rad.master.rk.zwonne.smartyha.data.ApiResponseToEntityConverter
import rad.master.rk.zwonne.smartyha.data.livedata.SharedPrefKeys.SYNC_TRADFRI_LIGHT_BULBS
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.data.remote.requestmodels.UpdateTradfriLightBulbRequest
import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriLightBulbFromResponse
import rad.master.rk.zwonne.smartyha.data.room.daos.TradfriLightBulbDao
import rad.master.rk.zwonne.smartyha.data.room.entities.Device
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriLightBulb
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.CoroutineDispatcherProvider
import rad.master.rk.zwonne.smartyha.util.zip
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * This repository works with Flow and Coroutines on purpose.
 */
@Singleton
class TradfriLightbulbRepository @Inject constructor(
        private val userRepository: UserRepository,
        private val tradfriGatewayRepository: TradfriGatewayRepository,
        private val localDataSource: TradfriLightBulbDao,
        private val sharedPreferences: SharedPreferences,
        private val remoteDataSource: SmartyHAService,
        private val coroutineDispatcherProvider: CoroutineDispatcherProvider
) {
    // return null for default value if no gateways
    @FlowPreview
    @ExperimentalCoroutinesApi
    val tradfriLightbulbs: Flow<Array<TradfriLightBulb>?> = tradfriGatewayRepository.tradfriGateways.flatMapLatest { tradfriGateways: Array<TradfriGateway> ->
        if (tradfriGateways.isNotEmpty()) {
            val result = mutableListOf<TradfriLightBulb>()
            Timber.d("tradfriLightbulbs flatMapLatest() Gateways aren't empty, size: ${tradfriGateways.size}")
            // each live data has an array of lightBulbs per gateway psk
            // so an array of livedatas is an array of all lightBulbs for a user
            val userAllLightBulbs = mutableListOf<Flow<List<TradfriLightBulb>>>()

            tradfriGateways.forEach { tradfriGateway ->
                Timber.d("tradfriLightbulbs flatMapLatest() Gateway psk: ${tradfriGateway.psk}")
                userAllLightBulbs.add(localDataSource.getLightBulbsByGateWayPsk(tradfriGateway.psk)
                                                     .distinctUntilChanged())
            }

            userAllLightBulbs.asFlow()
                             .flattenConcat()
                             .map { bulbs: List<TradfriLightBulb> ->
                                 bulbs
                             }
                             .flatMapConcat { bulbs: List<TradfriLightBulb> -> if (bulbs.isNotEmpty()) bulbs.asFlow() else flow { emit(null) } }
                             .filter { it != null }
                             .flowOn(coroutineDispatcherProvider.getIO())
                    .collect {
                        Timber.d("bulb")
                    }
//                             .toList(result)
            Timber.d("tradfriLightbulbs flatMapLatest() flatMapLatest() Number of lightBulbs found: ${result.size}")

            return@flatMapLatest flow {
                emit(result.toTypedArray())
            }
        } else {
            flow {
                Timber.d("tradfriLightbulbs.Transformations.switchMap() Gateways are empty!")
                emit(null)
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    suspend fun triggerLightBulbs() {
        val userFlow = userRepository.user.asFlow()
        val lightBulbsFlow = tradfriLightbulbs

        userFlow.combine(lightBulbsFlow) { user: User?, lightbulbs: Array<TradfriLightBulb>? ->
            Timber.d("triggerLightBulbs()")
            val syncTradfriLightBulbsWithBackend = sharedPreferences.getBoolean(SYNC_TRADFRI_LIGHT_BULBS, false)

            val callBackend = (user != null && syncTradfriLightBulbsWithBackend)
            return@combine Triple(callBackend, user, lightbulbs)
        }.flatMapConcat { triple: Triple<Boolean, User?, Array<TradfriLightBulb>?> ->
            return@flatMapConcat if (triple.first) {
                getAllTradfriLightBulbsForUserFromBackend(triple.second!!.email,
                                                          remoteDataSource)
                        .catch { emit(emptyArray()) }
                        .flowOn(coroutineDispatcherProvider.getIO())
                        .map { lightBulbsFromResponse: Array<TradfriLightBulbFromResponse> ->
                            // call to backend was successful, no need for sync flag anymore
                            sharedPreferences.edit()
                                             .putBoolean(SYNC_TRADFRI_LIGHT_BULBS, false)
                                             .apply()

                            return@map lightBulbsFromResponse.map { ApiResponseToEntityConverter.tradfriLightBulbFromResponseToEntity(it) }
                                                             .partition { return@partition it in triple.third!! }
                        }
                        .flowOn(coroutineDispatcherProvider.getDefault())
                        .map { lgtBulbs: Pair<List<TradfriLightBulb>, List<TradfriLightBulb>> ->
                            val oldLightBulbs = lgtBulbs.first.toTypedArray()
                            if (oldLightBulbs.isNotEmpty()) {
                                val numberOfUpdated = localDataSource.update(*oldLightBulbs)
                                Timber.d("LightBulbs updated: %d", numberOfUpdated)
                            }

                            val newLightBulbs = lgtBulbs.second.toTypedArray()
                            if (newLightBulbs.isNotEmpty()) {
                                localDataSource.insert(*newLightBulbs)
                            }
                        }
                        .flowOn(coroutineDispatcherProvider.getIO())
            } else {
                flow { emit(Unit) }
            }
        }
        .flowOn(coroutineDispatcherProvider.getIO())
        .collect()
    }

    @VisibleForTesting
    fun getAllTradfriLightBulbsForUserFromBackend(
            email: String,
            remoteDataSource: SmartyHAService
    ): Flow<Array<TradfriLightBulbFromResponse>> {
        return flow {
            emit(remoteDataSource.getTradfriLightBulbsForUser(email))
        }
    }

    @ExperimentalCoroutinesApi
    @InternalCoroutinesApi
    suspend fun checkConnection(
            email: String,
            psk: String,
            bulbId: Int,
            remoteDataSource: SmartyHAService = this.remoteDataSource,
            localDataSource: TradfriLightBulbDao = this.localDataSource
    ) {
        getSingleTradfriLightBulbFromBackend(email,
                                             psk,
                                             bulbId,
                                             remoteDataSource)
                .map { updateSingleLightBulbLocally(it, localDataSource)  }
                .flowOn(coroutineDispatcherProvider.getIO())
                .catch { e -> Timber.e("checkConnection() onError :%s", e.message)}
                .collect { numberOfUpdated: Int ->
                    Timber.d("checkConnection() Updated LightBulbs: %d", numberOfUpdated)
                }
    }

    @VisibleForTesting
    fun getSingleTradfriLightBulbFromBackend(
            email: String,
            psk: String,
            bulbId: Int,
            remoteDataSource: SmartyHAService
    ): Flow<Array<TradfriLightBulbFromResponse>> {
        Timber.d("getSingleTradfriLightBulbFromBacked()")
        return flow {
            emit (remoteDataSource.getTradfriLightBulbForPskAndBulbId(email, psk, bulbId))
        }
    }

    @VisibleForTesting
    suspend fun updateSingleLightBulbLocally(
            tradfriBulbFromResponse: Array<TradfriLightBulbFromResponse>,
            localDataSource: TradfriLightBulbDao = this.localDataSource
    ): Int {
        Timber.d("updateSingleLightBulbLocally()")
        val entityArray = tradfriBulbFromResponse.map { ApiResponseToEntityConverter.tradfriLightBulbFromResponseToEntity(it) }
        return if (entityArray.isNotEmpty()) localDataSource.update(entityArray[0]) else -1
    }

    fun getLightBulbById(psk: String, id: Int): LiveData<TradfriLightBulb> {
        return localDataSource.getLightBulbById(psk, id)
    }

    suspend fun updateSingleLightBulbOnBackend(
            tradfriLightBulb: TradfriLightBulb,
            remoteDataSource: SmartyHAService = this.remoteDataSource
    ) {
        Timber.d("updateSingleLightBulbOnBackend()")
        flow {
            val updateTradfriRequest = UpdateTradfriLightBulbRequest(tradfriLightBulb.gatewayPsk,
                                                                     userRepository.user.value!!.uid,
                                                                     tradfriLightBulb.bulbId,
                                                                     tradfriLightBulb.isOn,
                                                                     tradfriLightBulb.dimmerValue.toInt())
            emit(remoteDataSource.updateTradfriLightBulb(updateTradfriRequest))
        }
        .map { lastSeenReachablePair ->
            with (tradfriLightBulb) {
                if (lastSeenReachablePair.first != -1) {
                    updateSingleLightBulbLocally(arrayOf(TradfriLightBulbFromResponse(gatewayPsk,
                                                                                      bulbId,
                                                                                      userRepository.user.value!!.uid,
                                                                                      name,
                                                                                      lastSeenReachablePair.first,
                                                                                      lastSeenReachablePair.second,
                                                                                      if (isOn) 1 else 0,
                                                                                      dimmerValue.toInt(),
                                                                                      manufacturer,
                                                                                      modelNumber,
                                                                                      firmwareVersion)))
                }
            }
        }.catch { err ->
            Timber.e("updateSingleLightBulbOnBackend() Error: %s", err.message)
        }
        .flowOn(coroutineDispatcherProvider.getIO())
        .collect()
    }
}