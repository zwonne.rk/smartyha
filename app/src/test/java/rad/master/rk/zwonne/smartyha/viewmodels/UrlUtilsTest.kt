package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.core.util.PatternsCompat
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockkObject
import org.junit.jupiter.api.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import rad.master.rk.zwonne.smartyha.UrlTestDataFactory
import rad.master.rk.zwonne.smartyha.UrlTestDataFactory.PortTestData
import rad.master.rk.zwonne.smartyha.util.UrlUtils
import java.util.stream.Stream

class UrlUtilsTest {

    private companion object: UrlTestDataFactory()

    @BeforeAll
    fun setupBeforeAll() {
        mockkObject(UrlUtils)
        mockkObject(PatternsCompat.DOMAIN_NAME)
        every { PatternsCompat.DOMAIN_NAME.matcher(any()).matches() } returns true
    }

    @Nested
    inner class IsUrlValidTests {
        @Test
        @DisplayName("Given that a proper url with a port number is given then the result is true")
        fun isUrlValidFuncTest1() {
            UrlUtils.isUrlValid(getUrlString(httpStringLiteral, hostAddressLiteral, portLiteral))  shouldBe  true
        }

        @Test
        @DisplayName("Given that a proper url without a port number is given then the result is true")
        fun isUrlValidFuncTest2() {
            UrlUtils.isUrlValid(getUrlString(httpStringLiteral, hostAddressLiteral)) shouldBe true
        }

        @Test
        @DisplayName("Given that a proper url with a : but without a port number is given then the result is true")
        fun isUrlValidFuncTest3() {
            UrlUtils.isUrlValid(getUrlString(httpStringLiteral, "$hostAddressLiteral:")) shouldBe false
        }

        @Test
        @DisplayName("Given that a url with only a valid host is given then the result is false")
        fun isUrlValidFuncTest4() {
            UrlUtils.isUrlValid(hostAddressLiteral)  shouldBe false
        }

        @Test
        @DisplayName("Given that a url without a scheme is given then the result is false")
        fun isUrlValidFuncTest5() {
            UrlUtils.isUrlValid("$hostAddressLiteral:$portLiteral") shouldBe false
        }

        @Test
        @DisplayName("Given that a url with a invalid scheme is given then the result is false")
        fun isUrlValidFuncTest6() {
            UrlUtils.isUrlValid(getUrlString("asd", hostAddressLiteral))  shouldBe false
        }

        @Test
        @DisplayName("Given that a url with a invalid port is given then the result is false")
        fun isUrlValidFuncTest7() {
            UrlUtils.isUrlValid(getUrlString(httpStringLiteral, hostAddressLiteral, "asd"))  shouldBe false
        }
    }

    @Nested
    inner class IsSchemeValidTests {
        @Test
        @DisplayName("Given that scheme is http then the result is true")
        fun isSchemeValidFuncTest1() {
            UrlUtils.isSchemeValid(httpStringLiteral) shouldBe true
        }

        @Test
        @DisplayName("Given that scheme is https then the result is true")
        fun isSchemeValidFuncTest2() {
            UrlUtils.isSchemeValid(httpsStringLiteral) shouldBe true
        }

        @Test
        @DisplayName("Given that scheme is neither http, nor https then the result is false")
        fun isSchemeValidFuncTest3() {
            UrlUtils.isSchemeValid("random string") shouldBe false
        }
    }

    @Nested
    inner class IsPortValidTests {

        private fun validPortWithCharsProvider() = Stream.of(
                PortTestData("", true),
                PortTestData("1", true),
                PortTestData("12", true),
                PortTestData("123", true),
                PortTestData("1234", true),
                PortTestData("12345", true)
        )

        private fun invalidPortWithCharsProvider() = Stream.of(
                PortTestData("a", false),
                PortTestData("1a", false),
                PortTestData("ab", false),
                PortTestData("a3b", false),
                PortTestData("abc", false),
                PortTestData("abc6", false),
                PortTestData("abcd", false),
                PortTestData("abcd2", false),
                PortTestData("abcde", false),
                PortTestData("a1c2de", false),
                PortTestData("abcdef", false),
                PortTestData("1bc6ef", false),
                PortTestData("a123", false)
        )

        private fun invalidPortLengthProvider() = Stream.of(
                PortTestData("123214", false),
                PortTestData("1237214", false),
                PortTestData("138323214", false),
                PortTestData("1232514324", false),
                PortTestData("126327324314", false),
                PortTestData("1232349234214", false)
        )

        @ParameterizedTest
        @MethodSource("validPortWithCharsProvider")
        fun `Given that port length is lesser or equal to 5 and it contains only numbers Then the result is true`(data: PortTestData) {
            UrlUtils.isPortValid(data.input) shouldBe data.expected
        }

        @ParameterizedTest
        @MethodSource("invalidPortWithCharsProvider")
        fun `Given that port of any length contains a char Then the result is false`(data: PortTestData) {
            UrlUtils.isPortValid(data.input) shouldBe data.expected
        }

        @ParameterizedTest
        @MethodSource("invalidPortLengthProvider")
        fun `Given that port length is greater than 5 Then the result is false`(data: PortTestData) {
            UrlUtils.isPortValid(data.input) shouldBe data.expected
        }
    }
}