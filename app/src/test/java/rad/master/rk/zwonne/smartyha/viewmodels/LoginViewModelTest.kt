package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.lifecycle.MediatorLiveData
import com.google.firebase.auth.FirebaseUser
import com.jraska.livedata.test
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.UserTestDataFactory
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension

@ExtendWith(InstantTaskExecutorExtension::class)
class LoginViewModelTest {

    private companion object: UserTestDataFactory

    private val firebaseUserMock = createFirebaseUserMock()

    private val userRepositoryMock: UserRepository = mockk()

    private lateinit var firebaseUserTrigger: MediatorLiveData<FirebaseUser?>

    private lateinit var viewModel: LoginViewModel

    @BeforeEach
    fun setupBeforeEach() {
        clearMocks(userRepositoryMock)

        firebaseUserTrigger = MediatorLiveData()
        every { userRepositoryMock.firebaseUser } returns firebaseUserTrigger

        viewModel = LoginViewModel(userRepositoryMock)

        firebaseUserTrigger.value = null
    }

    @Nested
    inner class LoginEventTriggersTests {
        @Test
        fun `Given that firebase user is null AND firebaseUiLoginEvent is FirebaseLoginStarted Then firebaseUiLoginEvent live data value is FirebaseLoginStarted`() {
            assertThat(firebaseUserTrigger.value).isNull()
            assertThat(viewModel.firebaseUiLoginEvent.value).isNull()

            val firebaseUiLoginSuccessEventObserver = viewModel.firebaseUiLoginSuccessEvent
                                                               .test()
            // event has not been triggered yet
            firebaseUiLoginSuccessEventObserver.assertNoValue()

            viewModel.setFirebaseUiLoginEvent(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginStarted)

            // user has been triggered by a FirebaseLoginStarted event value
            val result = firebaseUiLoginSuccessEventObserver.awaitValue()
                                                            .value()

            assertThat(result).isExactlyInstanceOf(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginStarted::class.java)
        }

        @Test
        fun `Given that firebase user is null AND firebaseUiLoginEvent is FirebaseLoginFailure Then firebaseUiLoginEvent live data value is FirebaseLoginFailure`() {
            assertThat(firebaseUserTrigger.value).isNull()
            assertThat(viewModel.firebaseUiLoginEvent.value).isNull()

            val firebaseUiLoginSuccessEventObserver = viewModel.firebaseUiLoginSuccessEvent
                                                               .test()

            // event has not been triggered yet
            firebaseUiLoginSuccessEventObserver.assertNoValue()

            viewModel.setFirebaseUiLoginEvent(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginFailure(0))

            // user has been triggered by a FirebaseLoginStarted event value
            val result = firebaseUiLoginSuccessEventObserver.awaitValue()
                                                            .value()

            assertThat(result).isExactlyInstanceOf(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginFailure::class.java)
        }

        @Test
        fun `Given that firebase user is null AND firebaseUiLoginEvent is FirebaseLoginSuccess Then firebaseUiLoginEvent live data value is FirebaseLoginStarted`() {
            assertThat(firebaseUserTrigger.value).isNull()
            assertThat(viewModel.firebaseUiLoginEvent.value).isNull()

            val firebaseUiLoginSuccessEventObserver = viewModel.firebaseUiLoginSuccessEvent.test()
            // event has not been triggered yet
            firebaseUiLoginSuccessEventObserver.assertNoValue()

            viewModel.setFirebaseUiLoginEvent(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginSuccess)

            // user has been triggered by a FirebaseLoginStarted event value
            val result = firebaseUiLoginSuccessEventObserver.awaitValue()
                                                            .value()

            assertThat(result).isExactlyInstanceOf(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginStarted::class.java)
        }

        @Test
        fun `Given that firebase user is not null AND firebaseUiLoginEvent is FirebaseLoginStarted Then firebaseUiLoginEvent live data value is FirebaseLoginStarted`() {
            assertThat(firebaseUserTrigger.value).isNull()
            assertThat(viewModel.firebaseUiLoginEvent.value).isNull()

            val firebaseUiLoginSuccessEventObserver = viewModel.firebaseUiLoginSuccessEvent
                                                               .test()
            // event has not been triggered yet
            firebaseUiLoginSuccessEventObserver.assertNoValue()

            viewModel.setFirebaseUiLoginEvent(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginStarted)
            firebaseUserTrigger.value = firebaseUserMock

            val result = firebaseUiLoginSuccessEventObserver.awaitValue()
                                                            .value()

            assertThat(result).isExactlyInstanceOf(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginStarted::class.java)
        }

        @Test
        fun `Given that firebase user is not null AND firebaseUiLoginEvent is FirebaseLoginFailure Then firebaseUiLoginEvent live data value is FirebaseLoginFailure`() {
            assertThat(firebaseUserTrigger.value).isNull()
            assertThat(viewModel.firebaseUiLoginEvent.value).isNull()

            val firebaseUiLoginSuccessEventObserver = viewModel.firebaseUiLoginSuccessEvent.test()
            // event has not been triggered yet
            firebaseUiLoginSuccessEventObserver.assertNoValue()

            viewModel.setFirebaseUiLoginEvent(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginFailure(0))
            firebaseUserTrigger.value = firebaseUserMock

            val result = firebaseUiLoginSuccessEventObserver.awaitValue()
                                                            .value()

            assertThat(result).isExactlyInstanceOf(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginFailure::class.java)
        }

        @Test
        fun `Given that firebase user is not null AND firebaseUiLoginEvent is FirebaseLoginSuccess Then firebaseUiLoginEvent live data value is FirebaseLoginSuccess`() {
            assertThat(firebaseUserTrigger.value).isNull()
            assertThat(viewModel.firebaseUiLoginEvent.value).isNull()

            val firebaseUiLoginSuccessEventObserver = viewModel.firebaseUiLoginSuccessEvent
                                                               .test()
            // event has not been triggered yet
            firebaseUiLoginSuccessEventObserver.assertNoValue()

            viewModel.setFirebaseUiLoginEvent(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginSuccess)
            firebaseUserTrigger.value = firebaseUserMock

            firebaseUiLoginSuccessEventObserver.assertValue(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginSuccess)
        }
    }

    @Test
    fun `Given that onRetryLoginBtnClicked() method is called Then firebaseUiLoginEvent live data value will be FirebaseLoginStarted`() {
        assertThat(viewModel.firebaseUiLoginEvent.value).isNull()

        viewModel.onRetryLoginBtnClicked()

        assertThat(viewModel.firebaseUiLoginEvent.value).isExactlyInstanceOf(LoginViewModel.FirebaseUiLoginEvent.FirebaseLoginStarted::class.java)
    }
}
