package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.mockk.*
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.UserTestDataFactory
import rad.master.rk.zwonne.smartyha.data.repositories.TradfriGatewayRepository
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension
import rad.master.rk.zwonne.smartyha.util.TrampolineSchedulerExtension
import java.util.concurrent.TimeUnit

@Disabled("This is temporary until everything is patched up with Flows")
@ExtendWith(TrampolineSchedulerExtension::class, InstantTaskExecutorExtension::class)
class TradfriGatewayViewModelTest {

    private companion object: UserTestDataFactory

    private val testScheduler = TestScheduler()

    private val userRepositoryMock: UserRepository = mockk(relaxed = true)

    private val _userTrigger: MutableLiveData<User?> = MutableLiveData()

    private val userTrigger: LiveData<User?> = _userTrigger

    private lateinit var tradfriGatewayRepositorySpy: TradfriGatewayRepository

    private lateinit var viewModelSpy: TradfriGatewayViewModel

    @BeforeAll
    fun init() {
//        tradfriGatewayRepositorySpy = spyk(TradfriGatewayRepository(userRepositoryMock, mockk(), mockk(relaxed = true), mockk()))
        viewModelSpy = spyk(TradfriGatewayViewModel(userRepositoryMock, tradfriGatewayRepositorySpy))
    }

    @BeforeEach
    fun setupBeforeEach() {
        _userTrigger.value = createUserSpy()

        clearMocks(viewModelSpy, userRepositoryMock, tradfriGatewayRepositorySpy)

        every { userRepositoryMock.user } returns userTrigger
    }

    @Nested
    inner class SaveTradfriGatewayTests {

        @Test
        @DisplayName("Given that the gateway is successfully registered on the backend side and in the local DB Then handleSuccess() will be called")
        fun saveTradfriGatewayFuncTest1() {
            val expectedResult = 1L

            every { tradfriGatewayRepositorySpy.registerNewGatewayBackendSide(any(), any()) } returns Single.fromCallable { "psk" }
            every { tradfriGatewayRepositorySpy.persistNewGatewayLocally(any(), any()) } returns Single.fromCallable { expectedResult }

            viewModelSpy.saveTradfriGateway()

            testScheduler.advanceTimeBy(5, TimeUnit.MINUTES)

            verify {
                viewModelSpy.handleSuccess(expectedResult)
            }

            verify (inverse = true) {
                viewModelSpy.handleError(any())
            }
        }

        @Test
        @DisplayName("Given that the gateway is NOT successfully registered on the backend side Then handleError() will be called")
        fun saveTradfriGatewayFuncTest2() {
            val exceptionMock: Exception = mockk(relaxed = true)

            every { tradfriGatewayRepositorySpy.registerNewGatewayBackendSide(any(), any()) } returns Single.error(exceptionMock)

            viewModelSpy.saveTradfriGateway()

            testScheduler.advanceTimeBy(5, TimeUnit.MINUTES)

            verify {
                viewModelSpy.handleError(exceptionMock)
            }

            verify (inverse = true) {
                tradfriGatewayRepositorySpy.persistNewGatewayLocally(any(), any())
                viewModelSpy.handleSuccess(any())
            }
        }
    }
}