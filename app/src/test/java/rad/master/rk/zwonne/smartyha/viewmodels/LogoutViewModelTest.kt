package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.lifecycle.MediatorLiveData
import com.google.firebase.auth.FirebaseUser
import com.jraska.livedata.test
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.UserTestDataFactory
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension

@ExtendWith(InstantTaskExecutorExtension::class)
class LogoutViewModelTest {

    private companion object: UserTestDataFactory

    private val firebaseUserMock = createFirebaseUserMock()

    private val userSpy = createUserSpy()

    private val userRepository: UserRepository = mockk(relaxUnitFun = true)

    private lateinit var firebaseUserTrigger: MediatorLiveData<FirebaseUser?>

    private lateinit var userTrigger: MediatorLiveData<User?>

    private lateinit var viewModel: LogoutViewModel

    @BeforeEach
    fun setupBeforeEach() {
        clearMocks(userRepository)

        firebaseUserTrigger = MediatorLiveData()
        every { userRepository.firebaseUser } returns firebaseUserTrigger

        userTrigger = MediatorLiveData()
        every { userRepository.user } returns userTrigger

        viewModel = LogoutViewModel(userRepository)

        firebaseUserTrigger.value = null
        userTrigger.value = null
    }

    @Nested
    inner class LogoutEventTriggersTests {
        @Test
        fun `Given that both FirebaseUser and User are not null Then the isLoggedOut live data value is false`() {
            val isLoggedOutObserver = viewModel.isLoggedOut
                                               .test()

            isLoggedOutObserver.assertValue(true)

            firebaseUserTrigger.value = firebaseUserMock
            userTrigger.value = userSpy

            isLoggedOutObserver.awaitValue()
                               .assertValue(false)
        }

        @Test
        fun `Given that FirebaseUser is null and User is not null Then the isLoggedOut live data value is true`() {
            val isLoggedOutObserver = viewModel.isLoggedOut
                                               .test()

            isLoggedOutObserver.assertValue(true)

            userTrigger.value = userSpy

            isLoggedOutObserver.awaitValue()
                               .assertValue(true)
        }

        @Test
        fun `Given that FirebaseUser is not null and User is null Then the isLoggedOut live data value is true`() {
            val isLoggedOutObserver = viewModel.isLoggedOut
                                               .test()

            isLoggedOutObserver.assertValue(true)

            firebaseUserTrigger.value = firebaseUserMock

            isLoggedOutObserver.awaitValue()
                               .assertValue(true)
        }

        @Test
        fun `Given that both FirebaseUser and User are null Then the isLoggedOut live data value is true`() {
            val isLoggedOutObserver = viewModel.isLoggedOut
                                               .test()

            isLoggedOutObserver.assertValue(true)

            firebaseUserTrigger.value = firebaseUserMock
            userTrigger.value = userSpy

            isLoggedOutObserver.awaitValue()
                               .assertValue(false)

            userTrigger.value = null

            isLoggedOutObserver.awaitValue()
                               .assertValue(false)

            firebaseUserTrigger.value = null

            isLoggedOutObserver.awaitValue()
                               .assertValue(true)
        }
    }

    @Test
    fun `Given that signOut() method is called Then userRepository's signOut() method will be called`() {
        viewModel.signOut()

        verify {
            userRepository.signOut()
        }
    }
}
