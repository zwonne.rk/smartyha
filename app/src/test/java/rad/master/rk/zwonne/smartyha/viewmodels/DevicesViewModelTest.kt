package rad.master.rk.zwonne.smartyha.viewmodels

import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.data.repositories.TradfriLightbulbRepository
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension
import rad.master.rk.zwonne.smartyha.util.TestCoroutineDispatcherProvider

@ExtendWith(InstantTaskExecutorExtension::class)
class DevicesViewModelTest {

    @ExperimentalCoroutinesApi
    private val dispatcherProvider = TestCoroutineDispatcherProvider()

    private val lightBulbRepositoryMock: TradfriLightbulbRepository = mockk(relaxed = true, relaxUnitFun = true)

    private lateinit var viewModelSpy: DevicesViewModel

    @ExperimentalCoroutinesApi
    @BeforeEach
    fun setupBeforeEach() {
        viewModelSpy = spyk(DevicesViewModel(
                mockk(relaxed = true),
                mockk(relaxed = true),
                lightBulbRepositoryMock,
                mockk(relaxed = true),
                mockk(relaxed = true),
                mockk(relaxed = true)
        ))

        every { viewModelSpy.getViewModelScope() } returns TestCoroutineScope(dispatcherProvider.getIO())
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    @Test
    @DisplayName("Given that this method is called Then lightBulbRepository's trigger method will be called")
    fun testTriggerLightBulbs() = runBlockingTest {
        viewModelSpy.triggerDevices()

        coVerify {
            lightBulbRepositoryMock.triggerLightBulbs()
        }
    }
}