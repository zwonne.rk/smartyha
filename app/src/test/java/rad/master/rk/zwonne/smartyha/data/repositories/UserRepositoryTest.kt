package rad.master.rk.zwonne.smartyha.data.repositories

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GetTokenResult
import com.jraska.livedata.test
import io.mockk.*
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.UserTestDataFactory
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.remote.HostSelectionInterceptor
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.data.room.daos.UserDao
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension
import rad.master.rk.zwonne.smartyha.util.TrampolineSchedulerExtension
import java.util.concurrent.TimeUnit

@ExtendWith(TrampolineSchedulerExtension::class, InstantTaskExecutorExtension::class)
class UserRepositoryTest {

    private companion object: UserTestDataFactory

    private lateinit var expectedUserSpy: User

    private val firebaseUserMock: FirebaseUser = createFirebaseUserMock()

    private val idTokenResultMock: GetTokenResult = mockk(relaxed = true)

    private val idTokenTaskMock: Task<GetTokenResult> = mockk(relaxed = true)

    private val localDataSourceMock: UserDao = mockk(relaxed = true)

    private val remoteDataSourceMock: SmartyHAService = mockk(relaxed = true)

    private val hostSelectionInterceptorMock: HostSelectionInterceptor = mockk(relaxed = true)

    private val firebaseAuthMock: FirebaseAuth = mockk(relaxed = true)

    private val userIdToken = "SADSAdsadsADASDSADaSDasasdA"

    private lateinit var userRepositorySpy: UserRepository // the class under test has state

    @BeforeEach
    fun setupBeforeEach() {
        clearMocks(localDataSourceMock, remoteDataSourceMock, firebaseAuthMock,
                idTokenResultMock, idTokenTaskMock, hostSelectionInterceptorMock)

        every { idTokenResultMock.token } returns userIdToken
        every { idTokenTaskMock.result } returns idTokenResultMock
        every { firebaseUserMock.getIdToken(any()) } returns idTokenTaskMock

        expectedUserSpy = createUserSpy()

        // re-creation is required because of state
        userRepositorySpy = spyk(UserRepository(localDataSourceMock,
                                                remoteDataSourceMock,
                                                hostSelectionInterceptorMock,
                                                firebaseAuthMock))
    }

    @Nested
    inner class UserTriggersTests {
        @Test
        fun `Given that no firebase users are logged in Then current user will be null`() {
            val firebaseUserTrigger: MediatorLiveData<FirebaseUser?> = userRepositorySpy.firebaseUser as MediatorLiveData<FirebaseUser?>

            val userTestObserver = userRepositorySpy.user.test()
            // user has not been triggered yet
            userTestObserver.assertNoValue()

            firebaseUserTrigger.value = null

            // user has been triggered by a null FirebaseUser value
            val result = userTestObserver.awaitValue()
                                         .value()

            assertThat(result).isNull()
        }

        @Test
        @DisplayName("Given that a user is logged in Then a firebase Id Token will be requested and the current user will not be null")
        fun test2() {
            val userMutableLiveData = spyk(MutableLiveData<User?>())
            userMutableLiveData.value = expectedUserSpy

            every { localDataSourceMock.getUserByIdLiveData(any()) } returns userMutableLiveData

            val firebaseUserTrigger: MediatorLiveData<FirebaseUser?> = userRepositorySpy.firebaseUser as MediatorLiveData<FirebaseUser?>

            val userTestObserver = userRepositorySpy.user.test()
            // user has not been triggered yet
            userTestObserver.assertNoValue()

            firebaseUserTrigger.value = firebaseUserMock

            // user has been triggered by a null FirebaseUser value
            val result = userTestObserver.awaitValue(2, TimeUnit.SECONDS)
                                         .value()

            assertThat(result).isEqualTo(expectedUserSpy)
            verify {
                firebaseUserMock.getIdToken(any())
            }
        }
    }


    @Nested
    inner class UpdateUserTriggerTests {
        @Test
        @DisplayName("Given that both firebase user, and user are null Then only updateHostSelectionInterceptor() will be called with an empty UrlHolder")
        fun test1() {
            val firebaseUserTrigger: MediatorLiveData<FirebaseUser?> = userRepositorySpy.firebaseUser as MediatorLiveData<FirebaseUser?>

            val userTestObserver = userRepositorySpy.user.test()
            // user has not been triggered yet
            userTestObserver.assertNoValue()

            firebaseUserTrigger.value = null

            // user has been triggered by a null FirebaseUser value
            userTestObserver.awaitValue(1, TimeUnit.SECONDS)
                            .assertValue(null as User?)

            verify {
                hostSelectionInterceptorMock.scheme = any()
                hostSelectionInterceptorMock.host = any()
                hostSelectionInterceptorMock.port = any()
            }
        }

        @Test
        @DisplayName("Given that only user is null Then updateHostSelectionInterceptor(), and localDataSource.insertUser() will be called")
        fun test2() {
            val userMutableLiveData = spyk(MutableLiveData<User?>())
            userMutableLiveData.value = null
            every { localDataSourceMock.getUserByIdLiveData(any()) } returns userMutableLiveData

            val firebaseUserTrigger: MediatorLiveData<FirebaseUser?> = userRepositorySpy.firebaseUser as MediatorLiveData<FirebaseUser?>
            val userTestObserver = userRepositorySpy.user.test()

            firebaseUserTrigger.value = firebaseUserMock

            // user has been triggered by a null FirebaseUser value
            userTestObserver.awaitValue(1, TimeUnit.SECONDS)
                            .assertValue(null as User?)

            verify {
                localDataSourceMock.insertUser(any())
            }
        }

        @Test
        @DisplayName("Given that neither firebase user, nor user are null AND user is registered on backend (but flag not set in local db) Then hostSelectionInterceptor is updated, checkIfUserIsRegisteredOnBackend() will be called with updating local db, and registerUserOnBackendIfNeedBe() will be called without any effect")
        fun test3() {
            every { remoteDataSourceMock.isUserRegistered(any()) } returns Single.fromCallable { true }
            every { localDataSourceMock.updateRegisteredOnBackend(any(), any()) } returns 0

            val userMutableLiveData = spyk(MutableLiveData<User?>())
            userMutableLiveData.value = expectedUserSpy.apply { urlHolder = UrlHolder() }
            every { localDataSourceMock.getUserByIdLiveData(any()) } returns userMutableLiveData

            val firebaseUserTrigger: MediatorLiveData<FirebaseUser?> = userRepositorySpy.firebaseUser as MediatorLiveData<FirebaseUser?>
            val userTestObserver = userRepositorySpy.user.test()

            firebaseUserTrigger.value = firebaseUserMock

            // user has been triggered by a null FirebaseUser value
            userTestObserver.awaitValue(1, TimeUnit.SECONDS)
                            .assertValue(expectedUserSpy)

            verifyOrder {
                // these 3 lines are userRepositorySpy.updateHostSelectionInterceptor()
                hostSelectionInterceptorMock.scheme = any()
                hostSelectionInterceptorMock.host = any()
                hostSelectionInterceptorMock.port = any()

                // these 2 lines are userRepositorySpy.checkIfUserIsRegisteredOnBackend()
                remoteDataSourceMock.isUserRegistered(any())
                localDataSourceMock.updateRegisteredOnBackend(any(), any())
            }

            verify (inverse = true) {
                // this line is userRepositorySpy.registerUserOnBackendIfNeedBe()
                remoteDataSourceMock.registerUser(any())
            }
        }

        @Test
        @DisplayName("Given that neither firebase user, nor user are null AND user is registered on backend (with flag also set in local db) Then hostSelectionInterceptor is updated, checkIfUserIsRegisteredOnBackend() will be called with updating local db, and registerUserOnBackendIfNeedBe() will be called without any effect")
        fun test4() {
            every { localDataSourceMock.updateRegisteredOnBackend(any(), any()) } returns 0

            val userMutableLiveData = spyk(MutableLiveData<User?>())
            userMutableLiveData.value = expectedUserSpy.apply {
                                                                urlHolder = UrlHolder()
                                                                registeredOnBackend = true
                                                              }
            every { localDataSourceMock.getUserByIdLiveData(any()) } returns userMutableLiveData

            val firebaseUserTrigger: MediatorLiveData<FirebaseUser?> = userRepositorySpy.firebaseUser as MediatorLiveData<FirebaseUser?>
            val userTestObserver = userRepositorySpy.user.test()

            firebaseUserTrigger.value = firebaseUserMock

            // user has been triggered by a null FirebaseUser value
            userTestObserver.awaitValue(1, TimeUnit.SECONDS)
                    .assertValue(expectedUserSpy)

            verifyOrder {
                // these 3 lines are userRepositorySpy.updateHostSelectionInterceptor()
                hostSelectionInterceptorMock.scheme = any()
                hostSelectionInterceptorMock.host = any()
                hostSelectionInterceptorMock.port = any()
            }

            verify (inverse = true) {
                // these 2 lines are userRepositorySpy.checkIfUserIsRegisteredOnBackend()
                remoteDataSourceMock.isUserRegistered(any())
                localDataSourceMock.updateRegisteredOnBackend(any(), any())

                // this line is userRepositorySpy.registerUserOnBackendIfNeedBe()
                remoteDataSourceMock.registerUser(any())
            }
        }

        @Test
        @DisplayName("Given that neither firebase user, nor user are null AND user is not registered on backend Then hostSelectionInterceptor is updated, checkIfUserIsRegisteredOnBackend() will be called with updating local db, and registerUserOnBackendIfNeedBe() will be called with updating local db")
        fun test5() {
            every { remoteDataSourceMock.isUserRegistered(any()) } returns Single.fromCallable { false }
            every { remoteDataSourceMock.registerUser(any()) } returns Single.fromCallable { true }
            every { localDataSourceMock.updateRegisteredOnBackend(any(), any()) } returns 0

            val userMutableLiveData = spyk(MutableLiveData<User?>())
            userMutableLiveData.value = expectedUserSpy.apply { urlHolder = UrlHolder() }
            every { localDataSourceMock.getUserByIdLiveData(any()) } returns userMutableLiveData

            val firebaseUserTrigger: MediatorLiveData<FirebaseUser?> = userRepositorySpy.firebaseUser as MediatorLiveData<FirebaseUser?>
            val userTestObserver = userRepositorySpy.user.test()

            firebaseUserTrigger.value = firebaseUserMock

            // user has been triggered by a null FirebaseUser value
            userTestObserver.awaitValue(1, TimeUnit.SECONDS)
                            .assertValue(expectedUserSpy)

            verifyOrder {
                // these 3 lines are userRepositorySpy.updateHostSelectionInterceptor()
                hostSelectionInterceptorMock.scheme = any()
                hostSelectionInterceptorMock.host = any()
                hostSelectionInterceptorMock.port = any()

                // these 2 lines are userRepositorySpy.checkIfUserIsRegisteredOnBackend()
                remoteDataSourceMock.isUserRegistered(any())
                localDataSourceMock.updateRegisteredOnBackend(any(), any())

                // these 2 lines are userRepositorySpy.registerUserOnBackendIfNeedBe()
                remoteDataSourceMock.registerUser(any())
                localDataSourceMock.updateRegisteredOnBackend(any(), any())
            }
        }
    }

    @Nested
    inner class CheckIfUserIsRegisteredOnBackendTests {
        @Test
        fun `Given that the user has the registeredOnBackend flag set to true Then the result is that user`() {
            val expectedResult = expectedUserSpy.copy(registeredOnBackend = true)

            userRepositorySpy.checkIfUserIsRegisteredOnBackend(expectedResult,
                                                               localDataSourceMock,
                                                               remoteDataSourceMock)
                             .test()
                             .assertValue(expectedResult)
        }

        @Test
        @DisplayName("Given that the user doesn't have the registeredOnBackend flag set to true Then checkIfUserIsRegisteredOnBackendAndUpdateLocalDb() method is called AND the result is the result from that method")
        fun checkIfUserIsRegisteredOnBackendFunctionTest2() {
            every { userRepositorySpy.checkIfUserIsRegisteredOnBackendAndUpdateLocalDb(any(), any(), any()) } returns Single.just(expectedUserSpy)

            userRepositorySpy.checkIfUserIsRegisteredOnBackend(expectedUserSpy,
                                                               localDataSourceMock,
                                                               remoteDataSourceMock)
                             .test()
                             .assertValue(expectedUserSpy)

            verify {
                userRepositorySpy.checkIfUserIsRegisteredOnBackendAndUpdateLocalDb(any(), any(), any())
            }
        }
    }

    @Nested
    inner class CheckIfUserIsRegisteredOnBackendAndUpdateLocalDbTests {
        @Test
        @DisplayName("Given that the user is not registered on the backend Then user's registeredOnBackend flag is set to false AND updateUserInLocalDb() method is called AND the result is the user")
        fun checkIfUserIsRegisteredOnBackendAndUpdateLocalDbTest1() {
            every { remoteDataSourceMock.isUserRegistered(any()) } returns Single.fromCallable { false }
            every { localDataSourceMock.updateRegisteredOnBackend(any(), any()) } returns 0

            userRepositorySpy.checkIfUserIsRegisteredOnBackendAndUpdateLocalDb(expectedUserSpy,
                                                                               localDataSourceMock,
                                                                               remoteDataSourceMock)
                             .test()
                             .assertValue(expectedUserSpy)

            verify {
                localDataSourceMock.updateRegisteredOnBackend(any(), any())
            }
        }

        @Test
        @DisplayName("Given that the user is registered on the backend Then user's registeredOnBackend flag is set to true AND updateUserInLocalDb() method is called AND the result is the user")
        fun checkIfUserIsRegisteredOnBackendAndUpdateLocalDbTest2() {
            every { remoteDataSourceMock.isUserRegistered(any()) } returns Single.fromCallable { true }
            every { localDataSourceMock.updateRegisteredOnBackend(any(), any()) } returns 0

            userRepositorySpy.checkIfUserIsRegisteredOnBackendAndUpdateLocalDb(expectedUserSpy.copy(),
                                                                               localDataSourceMock,
                                                                               remoteDataSourceMock)
                             .test()
                             .assertValue {  result ->
                                 result.registeredOnBackend && result.uid == expectedUserSpy.uid
                             }

            verify {
                localDataSourceMock.updateRegisteredOnBackend(any(), any())
            }
        }

        @Test
        @DisplayName("Given that the registration API throws an exception Then the error is not handled")
        fun checkIfUserIsRegisteredOnBackendAndUpdateLocalDbTest3() {
            every { remoteDataSourceMock.isUserRegistered(any()) } returns Single.error(Exception("Surprise mothafucka!"))

            userRepositorySpy.checkIfUserIsRegisteredOnBackendAndUpdateLocalDb(expectedUserSpy.copy(),
                                                                               localDataSourceMock,
                                                                               remoteDataSourceMock)
                             .test()
                             .assertError { true }
        }
    }

    @Nested
    inner class RegisterUserOnBackendIfNeedBeTests {
        @Test
        @DisplayName("Given that the user has the registeredOnBackend flag set to true Then the result is that user")
        fun registerUserOnBackendIfNeedBeTest1() {
            val expectedResult = expectedUserSpy.copy(registeredOnBackend = true)

            userRepositorySpy.registerUserOnBackendIfNeedBe(expectedResult)
                             .test()
                             .assertValue(expectedResult)

            verify(inverse = true) {
                remoteDataSourceMock.registerUser(any())
            }
        }

        @Test
        @DisplayName("Given that the user has the registeredOnBackend flag set to false Then the registration API AND updateUserInLocalDb() methods are called AND result is the user object with the registeredOnBackend flag set to true")
        fun registerUserOnBackendIfNeedBeTest2() {
            every { remoteDataSourceMock.registerUser(any()) } returns Single.fromCallable { false }
            every { localDataSourceMock.updateRegisteredOnBackend(any(), any()) } returns 0

            userRepositorySpy.registerUserOnBackendIfNeedBe(expectedUserSpy.copy())
                             .test()
                             .assertValue(expectedUserSpy.copy(registeredOnBackend = true))

            verify {
                remoteDataSourceMock.registerUser(any())
                localDataSourceMock.updateRegisteredOnBackend(any(), any())
            }
        }
 
        @Test
        @DisplayName("Given that the API call throws an exception Then the error is not handled")
        fun registerUserOnBackendIfNeedBeTest3() {
            every { remoteDataSourceMock.registerUser(any()) } returns Single.error(Exception("Surprise mothafucka!"))

            userRepositorySpy.registerUserOnBackendIfNeedBe(expectedUserSpy.copy())
                             .test()
                             .assertError { true }
        }
    }
}
