package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.lifecycle.MediatorLiveData
import com.google.firebase.auth.FirebaseUser
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.UserTestDataFactory
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension
import rad.master.rk.zwonne.smartyha.util.TrampolineSchedulerExtension

@ExtendWith(TrampolineSchedulerExtension::class, InstantTaskExecutorExtension::class)
@Disabled
class DashboardViewModelTest {

    private companion object: UserTestDataFactory

    private val firebaseUserMock = createFirebaseUserMock()

    private val userRepositoryMock: UserRepository = mockk(relaxed = true)

    private val remoteDataSourceMock: SmartyHAService = mockk(relaxed = true)

    private lateinit var firebaseUserTrigger: MediatorLiveData<FirebaseUser?>

    private lateinit var viewModelSpy: DashboardViewModel

    @BeforeEach
    fun setupBeforeEach() {
        clearMocks(userRepositoryMock)

        firebaseUserTrigger = MediatorLiveData()
        every { userRepositoryMock.firebaseUser } returns firebaseUserTrigger

        viewModelSpy = spyk(DashboardViewModel(userRepositoryMock, remoteDataSourceMock))

        firebaseUserTrigger.value = null
    }
}