package rad.master.rk.zwonne.smartyha.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.TradfriTestDataFactory
import rad.master.rk.zwonne.smartyha.UserTestDataFactory
import rad.master.rk.zwonne.smartyha.data.repositories.TradfriLightbulbRepository
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriLightBulb
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension
import rad.master.rk.zwonne.smartyha.util.TestCoroutineDispatcherProvider
import rad.master.rk.zwonne.smartyha.util.TrampolineSchedulerExtension

@ExtendWith(TrampolineSchedulerExtension::class, InstantTaskExecutorExtension::class)
class TradfriLightBulbViewModelTest {

    private companion object : UserTestDataFactory, TradfriTestDataFactory

    private val userRepositoryMock: UserRepository = mockk(relaxed = true)

    private val _userTrigger: MutableLiveData<User?> = MutableLiveData()

    private val userTrigger: LiveData<User?> = _userTrigger

    private val _tradfriLightBulb: MutableLiveData<TradfriLightBulb> = MutableLiveData()

    private val tradfriLightBulb: LiveData<TradfriLightBulb> = _tradfriLightBulb

    @ExperimentalCoroutinesApi
    private val dispatcherProvider = TestCoroutineDispatcherProvider()

    private lateinit var tradfriLightBulbRepositorySpy: TradfriLightbulbRepository

    private lateinit var viewModelSpy: TradfriLightBulbViewModel

    @ExperimentalCoroutinesApi
    @BeforeAll
    fun init() {
        tradfriLightBulbRepositorySpy = spyk(TradfriLightbulbRepository(
                userRepositoryMock, mockk(relaxed = true), mockk(relaxed = true),
                mockk(relaxed = true), mockk(relaxed = true), spyk(dispatcherProvider)))

        viewModelSpy = spyk(TradfriLightBulbViewModel(userRepositoryMock, tradfriLightBulbRepositorySpy))
    }

    @ExperimentalCoroutinesApi
    @BeforeEach
    fun setupBeforeEach() {
        clearMocks(viewModelSpy, userRepositoryMock, tradfriLightBulbRepositorySpy)

        _userTrigger.value = null

        every { userRepositoryMock.user } returns userTrigger
        every { viewModelSpy.tradfriLightBulb } returns tradfriLightBulb
        every { viewModelSpy.getViewModelScope() } returns TestCoroutineScope(dispatcherProvider.getIO())
    }

    @Nested
    inner class OnTryConnectionClickedTests {
        @ExperimentalCoroutinesApi
        @InternalCoroutinesApi
        @Test
        @DisplayName("Given that user is null Then repository's method will not be called")
        fun onTryConnectionClickedTest1() = runBlockingTest {
            viewModelSpy.onTryConnectionClicked()

            coVerify(inverse = true) {
                tradfriLightBulbRepositorySpy.checkConnection(any(), any(), any())
            }
        }

        @ExperimentalCoroutinesApi
        @InternalCoroutinesApi
        @Test
        @DisplayName("Given that user is NOT null, but lightbulbs ARE null Then repository's method will not be called")
        fun onTryConnectionClickedTest2() = runBlockingTest {
            _userTrigger.value = createUserSpy()

            viewModelSpy.onTryConnectionClicked()

            coVerify(inverse = true) {
                tradfriLightBulbRepositorySpy.checkConnection(any(), any(), any())
            }
        }

        @ExperimentalCoroutinesApi
        @InternalCoroutinesApi
        @Test
        @DisplayName("Given that user is NOT null, AND lightbulbs is NOT null Then repository's method will be called")
        fun onTryConnectionClickedTest3() = runBlockingTest {
            coEvery { tradfriLightBulbRepositorySpy.checkConnection(any(), any(), any()) } just  Runs

            _tradfriLightBulb.value = createLightBulbSpy()
            _userTrigger.value = createUserSpy()

            viewModelSpy.onTryConnectionClicked()

            coVerify {
                tradfriLightBulbRepositorySpy.checkConnection(any(), any(), any())
            }
        }
    }
}