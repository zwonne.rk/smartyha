package rad.master.rk.zwonne.smartyha.viewmodels

import android.content.res.Resources
import com.jraska.livedata.test
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldBeBlank
import io.mockk.*
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.UrlTestDataFactory
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.remote.HostSelectionInterceptor
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension
import rad.master.rk.zwonne.smartyha.util.TrampolineSchedulerExtension
import java.util.concurrent.TimeUnit

@ExtendWith(TrampolineSchedulerExtension::class, InstantTaskExecutorExtension::class)
class BackendUrlViewModelTest {

    private companion object: UrlTestDataFactory()

    private val resourcesMock: Resources = mockk(relaxed = true)

    private lateinit var hostSelectionInterceptor: HostSelectionInterceptor

    private val remoteDataSourceMock: SmartyHAService = mockk(relaxed = true)

    private lateinit var viewModelSpy: BackendUrlViewModel

    @BeforeAll
    fun setupBeforeAll() {
        every { resourcesMock.getString(R.string.backend_url_fragment_https_label) } returns httpsStringLiteral
        every { resourcesMock.getString(R.string.backend_url_fragment_http_label) } returns httpStringLiteral
    }

    @BeforeEach
    fun setupBeforeEach() {
        clearMocks(remoteDataSourceMock)

        hostSelectionInterceptor = spyk(HostSelectionInterceptor())
        viewModelSpy = spyk(BackendUrlViewModel(resourcesMock, hostSelectionInterceptor, remoteDataSourceMock))
    }

    @Test
    fun `initial value of scheme live data is https`() {
        assertThat(viewModelSpy.urlHolder.scheme).isEqualTo(httpsStringLiteral)
    }

    @Nested
    inner class OnSchemeChangedTests {

        @Test
        @DisplayName("Given that viewId is for the https radio button Then the scheme value will be https")
        fun onSchemeChangedFuncTest1() {
            val viewId = R.id.httpsRadioButton
            // initial value
            viewModelSpy.urlHolder.scheme shouldBe httpsStringLiteral

            viewModelSpy.onSchemeChanged(viewId)

            // value after method call
            viewModelSpy.urlHolder.scheme shouldBe httpsStringLiteral
        }

        @Test
        @DisplayName("Given that viewId is anything other than the https radio button Then the scheme value will be http")
        fun onSchemeChangedFuncTest2() {
            val viewId = R.id.httpRadioButton
            // initial value
            viewModelSpy.urlHolder.scheme shouldBe httpsStringLiteral

            viewModelSpy.onSchemeChanged(viewId)

            // value after method call
            viewModelSpy.urlHolder.scheme shouldBe httpStringLiteral
        }
    }

    @Nested
    inner class OnShowPortUiChangedTests {
        @Test
        @DisplayName("Given that boolean function param is true Then showPortUi value will be true")
        fun onShowPortUiChangedFuncTest1() {
            // initial value
            viewModelSpy.showPortUi.value!!.shouldBeFalse()

            viewModelSpy.onShowPortUiChanged(true)

            // value after method call
            viewModelSpy.showPortUi.value!!.shouldBeTrue()
        }

        @Test
        @DisplayName("Given that boolean function param is false Then showPortUi value will be false")
        fun onShowPortUiChangedFuncTest2() {
            // initial value
            viewModelSpy.showPortUi.value!!.shouldBeFalse()

            viewModelSpy.onShowPortUiChanged(false)

            // value after method call
            viewModelSpy.showPortUi.value!!.shouldBeFalse()
        }
    }

    @Nested
    inner class SetupHostSelectionInterceptorTests {
        @Test
        @DisplayName("Given that scheme value is not changed Then the host interceptor's scheme value will be https")
        fun setupHostSelectionInterceptorMethodTest1() {
            // initial value
            viewModelSpy.urlHolder.scheme shouldBe httpsStringLiteral
            hostSelectionInterceptor.scheme.shouldBeBlank()

            viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)

            // value after method call
            hostSelectionInterceptor.scheme shouldBe httpsStringLiteral
        }

        @Test
        @DisplayName("Given that scheme value is http Then the host interceptor's scheme value will be http")
        fun setupHostSelectionInterceptorMethodTest2() {
            // initial value
            hostSelectionInterceptor.scheme.shouldBeBlank()
            viewModelSpy.urlHolder.scheme shouldBe httpsStringLiteral

            viewModelSpy.urlHolder.scheme = httpStringLiteral
            viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)

            // value after method call
            hostSelectionInterceptor.scheme shouldBe httpStringLiteral
        }

        @Test
        @DisplayName("Given that host value is an empty string Then the host interceptor's host value will be empty string")
        fun setupHostSelectionInterceptorMethodTest4() {
            // initial value
            hostSelectionInterceptor.host.shouldBeBlank()

            viewModelSpy.urlHolder.host = ""
            viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)

            // value after method call
            hostSelectionInterceptor.host.shouldBeBlank()
        }

        @Test
        @DisplayName("Given that host value is a non-empty string Then the host interceptor's host value will be that non-empty string")
        fun setupHostSelectionInterceptorMethodTest5() {
            // initial value
            hostSelectionInterceptor.host.shouldBeBlank()

            viewModelSpy.urlHolder.host = hostAddressLiteral
            viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)

            // value after method call
            hostSelectionInterceptor.host shouldBe hostAddressLiteral
        }

        @Test
        @DisplayName("Given that port value is an empty string Then the host interceptor's port value will be empty string")
        fun setupHostSelectionInterceptorMethodTest7() {
            // initial value
            hostSelectionInterceptor.port.shouldBeBlank()

            viewModelSpy.urlHolder.port = ""
            viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)

            // value after method call
            hostSelectionInterceptor.port.shouldBeBlank()
        }

        @Test
        @DisplayName("Given that port value is a non-empty string Then the host interceptor's port value will be that non-empty string")
        fun setupHostSelectionInterceptorMethodTest8() {
            // initial value
            hostSelectionInterceptor.port.shouldBeBlank()

            viewModelSpy.urlHolder.port = portLiteral
            viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)

            // value after method call
            hostSelectionInterceptor.port shouldBe portLiteral
        }

        @Test
        @DisplayName("Given that scheme value is not changed AND host and port values are empty strings Then the host interceptor's scheme value will be https, but host and port value will be empty strings")
        fun setupHostSelectionInterceptorMethodTest9() {
            // initial value
            hostSelectionInterceptor.scheme.shouldBeBlank()
            hostSelectionInterceptor.host.shouldBeBlank()
            hostSelectionInterceptor.port.shouldBeBlank()
            viewModelSpy.urlHolder.scheme shouldBe httpsStringLiteral
            viewModelSpy.urlHolder.host.shouldBeBlank()
            viewModelSpy.urlHolder.port.shouldBeBlank()

            viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)
            // value after method call
            hostSelectionInterceptor.scheme shouldBe httpsStringLiteral
            hostSelectionInterceptor.host.shouldBeBlank()
            hostSelectionInterceptor.port.shouldBeBlank()
        }

        @Test
        @DisplayName("Given that scheme value is http AND host is a non-empty string and port value is an empty string Then the host interceptor's scheme value will be http, host value will be a non-empty string and port value will be an empty string")
        fun setupHostSelectionInterceptorMethodTest10() {
            // initial value
            hostSelectionInterceptor.scheme.shouldBeBlank()
            hostSelectionInterceptor.host.shouldBeBlank()
            hostSelectionInterceptor.port.shouldBeBlank()
            viewModelSpy.urlHolder.scheme shouldBe httpsStringLiteral
            viewModelSpy.urlHolder.host.shouldBeBlank()
            viewModelSpy.urlHolder.port.shouldBeBlank()

            viewModelSpy.urlHolder.scheme = httpStringLiteral
            viewModelSpy.urlHolder.host = hostAddressLiteral
            viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)
            // value after method call
            hostSelectionInterceptor.scheme shouldBe httpStringLiteral
            hostSelectionInterceptor.host shouldBe hostAddressLiteral
            hostSelectionInterceptor.port.shouldBeBlank()
        }
    }

    @Nested
    inner class OnTryConnectionBtnClickedTests {
        @Test
        @DisplayName("Given that the API call does not throw an error Then the openLoginScreenOrErrorEvent value will be a OpenLoginScreenEvent")
        fun setupHostSelectionInterceptorMethodTest11() {
            every { remoteDataSourceMock.isServerAvailable() } returns Single.fromCallable { true }

            val loginEventObserver = viewModelSpy.openLoginScreenOrErrorEvent
                                                 .test()
            loginEventObserver.assertNoValue()

            viewModelSpy.onTryConnectionBtnClicked()
            loginEventObserver.awaitValue()

            if (loginEventObserver.valueHistory().size != 2) {
                loginEventObserver.awaitNextValue(3, TimeUnit.SECONDS)
            }

            loginEventObserver.assertValue(BackendUrlViewModel.BackendUrlEvents.OpenLoginScreenEvent(UrlHolder(scheme="https")))

            verify {
                viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)
                remoteDataSourceMock.isServerAvailable()
            }
        }

        @Test
        @DisplayName("Given that the API call throws an error Then the openLoginScreenOrErrorEvent value will be a ErrorEvent")
        fun setupHostSelectionInterceptorMethodTest12() {
            val exception = Exception()
            every { remoteDataSourceMock.isServerAvailable() } returns Single.error(exception)

            val loginEventObserver = viewModelSpy.openLoginScreenOrErrorEvent.test()
            loginEventObserver.assertNoValue()

            viewModelSpy.onTryConnectionBtnClicked()
            loginEventObserver.awaitValue()

            if (loginEventObserver.valueHistory().size != 2) {
                loginEventObserver.awaitNextValue(3, TimeUnit.SECONDS)
            }

            loginEventObserver.assertValue(BackendUrlViewModel.BackendUrlEvents.ErrorEvent(exception, false))

            verify {
                viewModelSpy.setupHostSelectionInterceptor(hostSelectionInterceptor)
                remoteDataSourceMock.isServerAvailable()
            }
        }
    }

    @Test
    @DisplayName("Given that onAlreadyHaveAccBtnClicked() function is called Then openLoginScreenOrErrorEvent value will be a OpenLoginScreenEvent")
    fun onAlreadyHaveAccBtnClickedFunctionTest() {
        // initial value
        assertThat(viewModelSpy.openLoginScreenOrErrorEvent.value).isNull()

        viewModelSpy.onAlreadyHaveAccBtnClicked()

        // value after method call
        assertThat(viewModelSpy.openLoginScreenOrErrorEvent.value).isExactlyInstanceOf(BackendUrlViewModel.BackendUrlEvents.OpenLoginScreenEvent::class.java)
    }
}
