package rad.master.rk.zwonne.smartyha.viewmodels

import android.content.SharedPreferences
import android.content.res.Resources
import androidx.core.util.PatternsCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.test
import io.kotest.matchers.shouldBe
import io.mockk.*
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.R
import rad.master.rk.zwonne.smartyha.UrlTestDataFactory
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.remote.HostSelectionInterceptor
import rad.master.rk.zwonne.smartyha.data.repositories.UserRepository
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension
import rad.master.rk.zwonne.smartyha.util.TrampolineSchedulerExtension
import rad.master.rk.zwonne.smartyha.util.UrlUtils
import java.util.concurrent.TimeUnit

@ExtendWith(TrampolineSchedulerExtension::class, InstantTaskExecutorExtension::class)
class SettingsViewModelTest {

    private companion object: UrlTestDataFactory()

    private val resourcesMock: Resources = mockk(relaxed = true)

    private lateinit var hostSelectionInterceptorSpy: HostSelectionInterceptor

    private val userRepositoryMock: UserRepository = mockk(relaxed = true)

    private val sharedPreferencesMock: SharedPreferences = mockk(relaxed = true)

    private lateinit var viewModelSpy: SettingsViewModel

    private val userLiveData: LiveData<User?> = MutableLiveData()

    @BeforeAll
    fun setupBeforeAll() {
        hostSelectionInterceptorSpy = spyk(HostSelectionInterceptor())

        every { resourcesMock.getString(R.string.backend_url_fragment_https_label) } returns httpsStringLiteral
        every { resourcesMock.getString(R.string.backend_url_fragment_http_label) } returns httpStringLiteral
        every { resourcesMock.getString(R.string.server_url_error_message) } returns "error message"
    }

    @BeforeEach
    fun setupBeforeEach() {
        clearMocks(userRepositoryMock, sharedPreferencesMock)

        every { userRepositoryMock.updateHostSelectionInterceptor(any(), any()) } just runs
        every { userRepositoryMock.updateUserUrlHolderLocally(any(), any()) } returns 1
        every { userRepositoryMock.user } returns userLiveData as MutableLiveData
        userLiveData.value = null

        // re-creation is required because of state
        viewModelSpy = spyk(SettingsViewModel(resourcesMock, userRepositoryMock,
                hostSelectionInterceptorSpy, sharedPreferencesMock))

        mockkObject(PatternsCompat.DOMAIN_NAME)
        every { PatternsCompat.DOMAIN_NAME.matcher(any()).matches() } returns true
    }

    @Nested
    inner class BackendServiceUrlFromLocalDbTests {
        @Test
        @DisplayName("Given that local db returns a null user then urlHolder will be an empty default urlHolder")
        fun backendServiceUrlFromLocalDbFuncTest1() {
            val urlFromLocalDbObserver = viewModelSpy.backendServiceUrlFromLocalDb
                                                     .test()
            // initial value
            (userLiveData as MutableLiveData).test()
                                             .assertValue(null as User?)

            // await this trigger
            urlFromLocalDbObserver.awaitValue(1, TimeUnit.SECONDS)

            viewModelSpy.urlHolder.get() shouldBe UrlHolder()
        }

        @Test
        @DisplayName("Given that local db returns a user with a valid UrlHolder then urlHolder will be a valid url string")
        fun backendServiceUrlFromLocalDbFuncTest2() {
            val urlFromLocalDbObserver = viewModelSpy.backendServiceUrlFromLocalDb
                                                     .test()
            // assert user null value at first
            (userLiveData as MutableLiveData).test()
                                             .assertValue(null as User?)

            // initial value
            viewModelSpy.urlHolder.get() shouldBe UrlHolder()

            // trigger adding a valid user
            val user = User(
                    "theUId",
                    "email@email.com",
                    true,
                    getUrlHolderSpy()
            )
            userLiveData.value = user

            urlFromLocalDbObserver.awaitValue(1, TimeUnit.SECONDS)

            UrlUtils.isUrlValid(viewModelSpy.urlHolder.toString())
        }
    }

    @Nested
    inner class BackendServiceUrlErrorMsgTests {
        @Test
        @DisplayName("Given that backendServiceUrlFromEditText contains an invalid url then backendServiceUrlErrorMsg will contain a value")
        fun backendServiceUrlErrorMsgFuncTest1() {
            val backendServiceUrlErrorMsgObserver = viewModelSpy._backendServiceUrlErrorMsg
                                                                .test()
            // input invalid url
            viewModelSpy.urlHolder.set(UrlHolder(scheme = "asc"))
            viewModelSpy.onUrlTextChanged()

            // error message liveData should contain a value at this point
            backendServiceUrlErrorMsgObserver.assertValue { errorMessage -> return@assertValue !errorMessage.isNullOrBlank() }
        }

        @Test
        @DisplayName("Given that backendServiceUrlFromEditText contains a valid url then backendServiceUrlErrorMsg will be an empty string")
        fun backendServiceUrlErrorMsgFuncTest2() {
            val backendServiceUrlErrorMsgObserver = viewModelSpy._backendServiceUrlErrorMsg
                                                                .test()
            // trigger an invalid url first
            viewModelSpy.urlHolder.set(UrlHolder(scheme = "a"))
            viewModelSpy.onUrlTextChanged()

            // error message liveData should contain a value at this point
            backendServiceUrlErrorMsgObserver.assertValue { errorMessage -> return@assertValue !errorMessage.isNullOrBlank() }

            // now input a valid url to editText
            viewModelSpy.urlHolder.set(getUrlHolderSpy())
            viewModelSpy.onUrlTextChanged()

            // error message should be an empty string now
            backendServiceUrlErrorMsgObserver.assertValue("")
        }

        @Test
        @DisplayName("Given that backendServiceUrlFromEditText contains an empty string then backendServiceUrlErrorMsg will be an empty string")
        fun backendServiceUrlErrorMsgFuncTest3() {
            val backendServiceUrlErrorMsgObserver = viewModelSpy._backendServiceUrlErrorMsg
                                                                .test()
            // trigger an empty url first
            viewModelSpy.onUrlTextChanged()

            // error message should be null now
            backendServiceUrlErrorMsgObserver.assertValue("")
        }
    }

    @Nested
    inner class IsBackendServiceUrlValidTests {
        @Test
        @DisplayName("Given that the user has inputted an invalid url, the value will be false")
        fun isBackendServiceUrlValidFuncTest1() {
            val isBackendServiceUrlValidbserver = viewModelSpy.isBackendServiceUrlValid
                                                              .test()
            // it should be false as start value
            isBackendServiceUrlValidbserver.assertValue(false)

            // input an invalid url to the editTex
            viewModelSpy.urlHolder.set(UrlHolder(scheme = "asda"))
            viewModelSpy.onUrlTextChanged()

            // it should be false now as well
            isBackendServiceUrlValidbserver.assertValue(false)
        }

        @Test
        @DisplayName("Given that the user has inputted a valid url, the backendUrl liveData value will be true")
        fun isBackendServiceUrlValidFuncTest2() {
            val isBackendServiceUrlValidObserver = viewModelSpy.isBackendServiceUrlValid
                                                               .test()
            // it should be false as start value
            isBackendServiceUrlValidObserver.assertValue(false)

            // input a valid url to the editTex
            viewModelSpy.urlHolder.set(getUrlHolderSpy())
            viewModelSpy.onUrlTextChanged()

            // it should be true now
            isBackendServiceUrlValidObserver.assertValue(true)
        }
    }

    @Test
    @DisplayName("Given that the user has inputted a valid url, and clicked the button, the value will become false")
    fun onSaveBtnClickedFuncTest1() {
        val btnEnabledObserver = viewModelSpy.saveBtnEnabled
                                             .test()
        // it should be false as start value
        btnEnabledObserver.assertValue(false)

        // input a valid url to the editTex
        viewModelSpy.urlHolder.set(getUrlHolderSpy())
        viewModelSpy.onUrlTextChanged()

        // it should be true now
        btnEnabledObserver.assertValue(true)

        // click on the button
        viewModelSpy.onSaveBtnClicked()

        // btn should be disabled now
        btnEnabledObserver.assertValue(false)
    }
}