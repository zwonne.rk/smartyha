package rad.master.rk.zwonne.smartyha.data.repositories

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.test
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.TestDataFactory.Companion.bulbId
import rad.master.rk.zwonne.smartyha.TestDataFactory.Companion.psk
import rad.master.rk.zwonne.smartyha.TradfriTestDataFactory
import rad.master.rk.zwonne.smartyha.UserTestDataFactory
import rad.master.rk.zwonne.smartyha.data.ApiResponseToEntityConverter
import rad.master.rk.zwonne.smartyha.data.livedata.SharedPrefKeys
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriLightBulbFromResponse
import rad.master.rk.zwonne.smartyha.data.room.daos.TradfriLightBulbDao
import rad.master.rk.zwonne.smartyha.data.room.entities.Device
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension
import rad.master.rk.zwonne.smartyha.util.TestCoroutineDispatcherProvider
import rad.master.rk.zwonne.smartyha.util.TrampolineSchedulerExtension

@Disabled("This is temporary until everything is patched up with Flows")
@ExtendWith(TrampolineSchedulerExtension::class, InstantTaskExecutorExtension::class)
class TradfriLightBulbRepositoryTest {

    private companion object: TradfriTestDataFactory, UserTestDataFactory

    private val userRepositoryMock: UserRepository = mockk(relaxed = true)

    private val gatewayRepositoryMock: TradfriGatewayRepository = mockk(relaxed = true)

    private val localDataSourceMock: TradfriLightBulbDao = mockk(relaxed = true)

    private val sharedPreferencesMock: SharedPreferences = mockk(relaxed = true)

    private val remoteDataSourceMock: SmartyHAService = mockk(relaxed = true)

    @ExperimentalCoroutinesApi
    private val coroutineDispatcherProviderSpy = spyk(TestCoroutineDispatcherProvider())

    val _userTrigger: MutableLiveData<User?> = spyk(MutableLiveData())

    val userTrigger: LiveData<User?> = _userTrigger

    val _gatewaysTrigger: MutableLiveData<Array<TradfriGateway>> = spyk(MutableLiveData())

    val gatewaysTrigger: LiveData<Array<TradfriGateway>> = _gatewaysTrigger

    private lateinit var lightBulbRepositorySpy: TradfriLightbulbRepository // the class under test has state

    @ExperimentalCoroutinesApi
    @BeforeEach
    fun setupBeforeEach() {
        clearMocks(localDataSourceMock, sharedPreferencesMock, remoteDataSourceMock, userRepositoryMock, gatewayRepositoryMock)

        _gatewaysTrigger.value = emptyArray()
        _userTrigger.value = null

        every { userRepositoryMock.user } returns userTrigger
//        every { gatewayRepositoryMock.tradfriGateways } returns gatewaysTrigger

        // re-creation is required because of state
        lightBulbRepositorySpy = spyk(TradfriLightbulbRepository(userRepositoryMock,
                                                                 gatewayRepositoryMock,
                                                                 localDataSourceMock,
                                                                 sharedPreferencesMock,
                                                                 remoteDataSourceMock,
                                                                 coroutineDispatcherProviderSpy))
    }

    @Nested
    inner class GetTradfriLightbulbsTests {
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that there are no Tradfri LightBulbs in the local DB Then the result is null")
        fun getTradfriLightbulbsTest1() = runBlockingTest {
//            lightBulbRepositorySpy.tradfriLightbulbs
//                                  .test()
//                                  .assertValue { it == null }
        }

        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that there are Tradfri LightBulbs in the local DB Then the result is an array of TradfriLightBulbs")
        fun getTradfriLightbulbsTest2() = runBlockingTest {
            val result = createLightBulbSpyList(1)
            every { localDataSourceMock.getLightBulbsByGateWayPsk(any()) } returns spyk(flow { emit(result) })

            val gateways = createListOfTradfriGatewayEntitySpies(listLength = 1).toTypedArray()
            _gatewaysTrigger.value = gateways

//            lightBulbRepositorySpy.tradfriLightbulbs
//                                  .test()
//                                  .assertValue { res -> res?.let { it[0] == result[0]} }
        }
    }

    @Nested
    inner class TradfriLightBulbsTriggerTests {
        @FlowPreview
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that user is null, and lightbulbs are null, backend sync will not be triggered")
        fun tradfriLightBulbsTriggerTest1() = runBlockingTest {
            TestCoroutineScope().launch {
                lightBulbRepositorySpy.triggerLightBulbs()
            }
            // user is null, and lightBulbs are null
            coVerify (inverse = true) {
                lightBulbRepositorySpy.getAllTradfriLightBulbsForUserFromBackend(any(), any())
            }
        }

        @FlowPreview
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that user is != null, BUT lightbulbs == null, backend sync will not be triggered")
        fun tradfriLightBulbsTriggerTest2() = runBlockingTest {
            _userTrigger.value = createUserSpy()
            TestCoroutineScope().launch {
                lightBulbRepositorySpy.triggerLightBulbs()
            }
            // lightBulbs are null
            coVerify (inverse = true) {
                lightBulbRepositorySpy.getAllTradfriLightBulbsForUserFromBackend(any(), any())
            }
        }

        @FlowPreview
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that user is == null, BUT lightbulbs != null, backend sync will not be triggered")
        fun tradfriLightBulbsTriggerTest3() = runBlockingTest {
            val lightBulb = createLightBulbSpyList(1)
            every { localDataSourceMock.getLightBulbsByGateWayPsk(any()) } returns spyk(flow { emit(lightBulb) })

            val gateways = createListOfTradfriGatewayEntitySpies(listLength = 1).toTypedArray()
            _gatewaysTrigger.value = gateways

            TestCoroutineScope().launch {
                lightBulbRepositorySpy.triggerLightBulbs()
            }
//            lightBulbRepositorySpy.tradfriLightbulbs
//                                  .test()
//                                  .assertValue { res -> res?.let { it[0] == lightBulb[0]} }
            // user is null
            verify (inverse = true) {
                lightBulbRepositorySpy.getAllTradfriLightBulbsForUserFromBackend(any(), any())
            }
        }

        @FlowPreview
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that user is != null, AND lightbulbs != null, BUT SYNC_TRADFRI_LIGHT_BULBS == false Then backend sync will not be triggered")
        fun tradfriLightBulbsTriggerTest4() = runBlockingTest {
            // trigger lightbulbs
            val lightBulb = createLightBulbSpyList(1)
            every { localDataSourceMock.getLightBulbsByGateWayPsk(any()) } returns spyk(flow { emit(lightBulb) })

            val gateways = createListOfTradfriGatewayEntitySpies(listLength = 1).toTypedArray()
            _gatewaysTrigger.value = gateways

            TestCoroutineScope().launch {
                lightBulbRepositorySpy.triggerLightBulbs()
            }
//            lightBulbRepositorySpy.tradfriLightbulbs
//                                  .test()
//                                  .assertValue { res -> res?.let { it[0] == lightBulb[0]} }
            // trigger user
            _userTrigger.value = createUserSpy()

            every { sharedPreferencesMock.getBoolean(SharedPrefKeys.SYNC_TRADFRI_LIGHT_BULBS, any()) } returns false

            verify (inverse = true) {
                lightBulbRepositorySpy.getAllTradfriLightBulbsForUserFromBackend(any(), any())
            }
        }

        @FlowPreview
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that user is != null, NAD lightbulbs != null, AND SYNC_TRADFRI_LIGHT_BULBS == true Then backend sync will be triggered")
        fun tradfriLightBulbsTriggerTest5() = runBlockingTest {
            val userSpy = createUserSpy()

            val sharedPrefEditorMock = mockk<SharedPreferences.Editor>(relaxed = true)
            every { sharedPreferencesMock.edit() } returns sharedPrefEditorMock
            every { sharedPreferencesMock.getBoolean(SharedPrefKeys.SYNC_TRADFRI_LIGHT_BULBS, any()) } returns true

            val firstLinkResult = createListOfTradfriLightBulbSpiesFromResponse(1)
            coEvery { remoteDataSourceMock.getTradfriLightBulbsForUser(any()) } returns firstLinkResult.toTypedArray()

            // trigger lightbulbs
            val lightBulb: List<Device.TradfriLightBulb> = listOf(ApiResponseToEntityConverter.tradfriLightBulbFromResponseToEntity(firstLinkResult[0]))
            every { localDataSourceMock.getLightBulbsByGateWayPsk(any()) } returns spyk(flow { emit(lightBulb) })
            every { localDataSourceMock.insert(any()) } returns mockk()

            mockkObject(ApiResponseToEntityConverter)
            every { ApiResponseToEntityConverter.tradfriLightBulbFromResponseToEntity(any()) } returns mockk(relaxed = true)

            val gateways = createListOfTradfriGatewayEntitySpies(listLength = 1).toTypedArray()
            _gatewaysTrigger.value = gateways

            TestCoroutineScope().launch {
                lightBulbRepositorySpy.triggerLightBulbs()
            }
//            lightBulbRepositorySpy.tradfriLightbulbs
//                                  .test()
//                                  .assertValue { res -> res?.let { it[0] == lightBulb[0]} }
            // trigger user
            _userTrigger.value = userSpy

            coVerify {
                // this should actually be lightBulbRepositorySpy.getAllTradfriLightBulbsForUserFromBackend(any(), any(), any()),
                // but it fails the test for some reason
                remoteDataSourceMock.getTradfriLightBulbsForUser(any())
                sharedPrefEditorMock.putBoolean(SharedPrefKeys.SYNC_TRADFRI_LIGHT_BULBS, false)
            }
        }
    }

    @Nested
    inner class GetAllTradfriLightBulbsForUserFromBackendTests {
        @ExperimentalCoroutinesApi
        @InternalCoroutinesApi
        @Test
        @DisplayName("Given that backend side successfully returns a result Then the result is an array of TradfriLightBulbFromResponses")
        fun getAllTradfriLightBulbsForUserFromBackendTest2() = runBlockingTest {
            val userSpy = createUserSpy()

            val expectedResult = createListOfTradfriLightBulbSpiesFromResponse(1)
            coEvery { remoteDataSourceMock.getTradfriLightBulbsForUser(any()) } returns expectedResult.toTypedArray()

            lightBulbRepositorySpy.getAllTradfriLightBulbsForUserFromBackend(userSpy.email,
                                                                             remoteDataSourceMock)
                                  .collect { result ->
                                      result[0] shouldBe expectedResult[0]
                                  }
        }

        @ExperimentalCoroutinesApi
        @InternalCoroutinesApi
        @Test
        @DisplayName("Given that an error occurs on the backend Then the result is an Exception")
        fun getAllTradfriLightBulbsForUserFromBackendTest3() = runBlockingTest {
            val userSpy = createUserSpy()

            val expectedExceptionSpy = spyk(Exception("Suprise suprise!!"))

            coEvery { remoteDataSourceMock.getTradfriLightBulbsForUser(any()) } throws expectedExceptionSpy

            lightBulbRepositorySpy.getAllTradfriLightBulbsForUserFromBackend(userSpy.email,
                                                                             remoteDataSourceMock)
                                  .catch { err -> err shouldBe expectedExceptionSpy }
        }
    }

    @Nested
    inner class GetSingleTradfriLightBulbFromBackendTests {
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that backend side successfully returns a result Then the result is an array of TradfriLightBulbFromResponses")
        fun getSingleTradfriLightBulbFromBackendTest2() = runBlockingTest {
            val userSpy = createUserSpy()

            val expectedResult = createListOfTradfriLightBulbSpiesFromResponse(1)
            coEvery { remoteDataSourceMock.getTradfriLightBulbForPskAndBulbId(any(), any(), any()) } returns expectedResult.toTypedArray()

            lightBulbRepositorySpy.getSingleTradfriLightBulbFromBackend(userSpy.email,
                                                                        psk,
                                                                        bulbId,
                                                                        remoteDataSourceMock)
                                  .collect { result ->
                                      result[0] shouldBe expectedResult[0]
                                  }
        }

        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that an error occurs on the backend Then the result is an Exception")
        fun getSingleTradfriLightBulbFromBackendTest3() = runBlockingTest {
            val userSpy = createUserSpy()

            val expectedExceptionSpy = spyk(Exception("Suprise suprise!!"))

            coEvery { remoteDataSourceMock.getTradfriLightBulbForPskAndBulbId(any(), any(), any()) } throws expectedExceptionSpy

            lightBulbRepositorySpy.getSingleTradfriLightBulbFromBackend(userSpy.email,
                                                                        psk,
                                                                        bulbId,
                                                                        remoteDataSourceMock)
                                  .catch { err -> err shouldBe expectedExceptionSpy }
        }
    }

    @Nested
    inner class UpdateSingleLightBulbLocallyTests {
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that the input Array for updating LightBulbs isn't empty, Then the result is a number of updated lightbulbs in local DB")
        fun updateSingleLightBulbLocallyTest1() = runBlockingTest {
            val lightBulbFromResponseSpy = createListOfTradfriLightBulbSpiesFromResponse(1).toTypedArray()
            coEvery { localDataSourceMock.update(any()) } returns 1

            mockkObject(ApiResponseToEntityConverter)
            every { ApiResponseToEntityConverter.tradfriLightBulbFromResponseToEntity(any()) } returns mockk()

            val result = lightBulbRepositorySpy.updateSingleLightBulbLocally(lightBulbFromResponseSpy, localDataSourceMock)

            verify { ApiResponseToEntityConverter.tradfriLightBulbFromResponseToEntity(lightBulbFromResponseSpy[0]) }
            result shouldBe 1
        }

        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that the input Array for updating LightBulbs is empty, Then the result is -1")
        fun updateSingleLightBulbLocallyTest2() = runBlockingTest {
            val lightBulbFromResponseSpy = emptyArray<TradfriLightBulbFromResponse>()

            val result = lightBulbRepositorySpy.updateSingleLightBulbLocally(lightBulbFromResponseSpy, localDataSourceMock)

            result shouldBe -1
        }
    }

    @Nested
    inner class CheckConnectionTests {
        @InternalCoroutinesApi
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("If there's no error with backend communication or local db Then 2 methods will be called by the repository")
        fun checkConnectionTest1() = runBlockingTest {
            val expectedResult = createListOfTradfriLightBulbSpiesFromResponse(1)
            coEvery { remoteDataSourceMock.getTradfriLightBulbForPskAndBulbId(any(), any(), any()) } returns expectedResult.toTypedArray()
            coEvery { localDataSourceMock.update(any()) } returns 1

            val userEmail = createUserSpy().email
            val (gatewayPsk, bulbId) = createLightBulbSpy()
            TestCoroutineScope().launch {
                lightBulbRepositorySpy.checkConnection(userEmail, gatewayPsk, bulbId, remoteDataSourceMock, localDataSourceMock)
            }

            coVerify {
                lightBulbRepositorySpy.updateSingleLightBulbLocally(any(), any())
            }

            verify {
                lightBulbRepositorySpy.getSingleTradfriLightBulbFromBackend(any(), any(), any(), any())
            }
        }

        @InternalCoroutinesApi
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given there's an error during backend communication Then an exception will be thrown")
        fun checkConnectionTest2() = runBlockingTest {
            val exceptionSpy = spyk(Exception("Yaas"))
            coEvery { remoteDataSourceMock.getTradfriLightBulbForPskAndBulbId(any(), any(), any()) } throws exceptionSpy
            coEvery { localDataSourceMock.update(any()) } returns 1

            val userEmail = createUserSpy().email
            val (gatewayPsk, bulbId) = createLightBulbSpy()
            TestCoroutineScope().launch {
                lightBulbRepositorySpy.checkConnection(userEmail, gatewayPsk, bulbId, remoteDataSourceMock, localDataSourceMock)
            }

            coVerify (inverse = true) {
                lightBulbRepositorySpy.updateSingleLightBulbLocally(any(), any())
            }

            verify {
                lightBulbRepositorySpy.getSingleTradfriLightBulbFromBackend(any(), any(), any(), any())
            }
        }
    }

    @Nested
    inner class UpdateSingleLightBulbOnBackendTests {
        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that no errors occur on Backend side Then the lightbulb will be updated locally")
        fun updateSingleLightBulbOnBackendTest1() = runBlockingTest {
            coEvery { remoteDataSourceMock.updateTradfriLightBulb(any()) } returns Pair(123, true)
            _userTrigger.value = createUserSpy()

            val lightBulbSpy = createLightBulbSpy()
            TestCoroutineScope().launch {
                lightBulbRepositorySpy.updateSingleLightBulbOnBackend(lightBulbSpy)
            }

            coVerify {
                lightBulbRepositorySpy.updateSingleLightBulbLocally(any())
            }
        }

        @ExperimentalCoroutinesApi
        @Test
        @DisplayName("Given that an error occurs on Backend side Then the lightbulb will NOT be updated locally")
        fun updateSingleLightBulbOnBackendTest2() = runBlockingTest {
            val exceptionSpy = spyk(Exception("ERROR"))
            coEvery { remoteDataSourceMock.updateTradfriLightBulb(any()) } throws exceptionSpy
            _userTrigger.value = createUserSpy()

            val lightBulbSpy = createLightBulbSpy()
            TestCoroutineScope().launch {
                lightBulbRepositorySpy.updateSingleLightBulbOnBackend(lightBulbSpy)
            }

            coVerify (inverse = true) {
                lightBulbRepositorySpy.updateSingleLightBulbLocally(any())
            }
        }
    }
}