package rad.master.rk.zwonne.smartyha.util

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain

@ExperimentalCoroutinesApi
class TestCoroutineDispatcherProvider: CoroutineDispatcherProvider() {

    private val dispatcher = TestCoroutineDispatcher()

    init {
        Dispatchers.setMain(dispatcher)
    }

    override fun getIO(): CoroutineDispatcher {
        return dispatcher
    }

    override fun getDefault(): CoroutineDispatcher {
        return dispatcher
    }
}