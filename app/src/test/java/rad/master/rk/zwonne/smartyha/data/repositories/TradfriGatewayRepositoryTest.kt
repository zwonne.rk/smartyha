package rad.master.rk.zwonne.smartyha.data.repositories

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.reactivex.Single
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import rad.master.rk.zwonne.smartyha.TestDataFactory.Companion.psk
import rad.master.rk.zwonne.smartyha.TradfriTestDataFactory
import rad.master.rk.zwonne.smartyha.UserTestDataFactory
import rad.master.rk.zwonne.smartyha.data.remote.SmartyHAService
import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriGatewayFromResponse
import rad.master.rk.zwonne.smartyha.data.room.daos.TradfriGatewayDao
import rad.master.rk.zwonne.smartyha.data.room.entities.User
import rad.master.rk.zwonne.smartyha.util.InstantTaskExecutorExtension
import rad.master.rk.zwonne.smartyha.util.TrampolineSchedulerExtension
import retrofit2.HttpException
import retrofit2.Response

@Disabled("This is temporary until everything is patched up with Flows")
@ExtendWith(TrampolineSchedulerExtension::class, InstantTaskExecutorExtension::class)
class TradfriGatewayRepositoryTest {

    private companion object: UserTestDataFactory, TradfriTestDataFactory

    private val userRepositoryMock: UserRepository = mockk(relaxed = true)

    private val gatewayDaoMock: TradfriGatewayDao = mockk(relaxed = true)

    private val sharedPreferencesMock: SharedPreferences = mockk(relaxed = true)

    private val remoteDataSourceMock: SmartyHAService = mockk(relaxed = true)

    private val uid: String = "sadasgdsgdsg"

    val _userTrigger: MutableLiveData<User?> = MutableLiveData()

    val userTrigger: LiveData<User?> = _userTrigger

    private lateinit var gatewayRepository: TradfriGatewayRepository // the class under test has state

    @BeforeEach
    fun setupBeforeEach() {
        clearMocks(gatewayDaoMock, sharedPreferencesMock, remoteDataSourceMock, userRepositoryMock)

        _userTrigger.value = null

        every { userRepositoryMock.user } returns userTrigger

        // re-creation is required because of state
//        gatewayRepository = spyk(TradfriGatewayRepository(userRepositoryMock,
//                                                          gatewayDaoMock,
//                                                          sharedPreferencesMock,
//                                                          remoteDataSourceMock))
    }

    @Nested
    inner class GetTradfriGatewaysForUserFromBackendTests {
        @Test
        @DisplayName("Given that there are no Tradfri Gateways registered on the backend Then the result is an empty array")
        fun getTradfriGatewaysForUserFromBackendTest1() {
            val userSpy = createUserSpy()

//            every { remoteDataSourceMock.getTradfriGatewaysForUser(any()) } returns Single.fromCallable { emptyArray<TradfriGatewayFromResponse>() }
//
//            gatewayRepository.getTradfriGatewaysForUserFromBackend(userSpy.email, remoteDataSourceMock)
//                             .test()
//                             .assertValue{ array -> array.isEmpty() }
        }

        @Test
        @DisplayName("Given that there are Tradfri Gateways registered on the backend Then the result is a list of those gateways")
        fun getTradfriGatewaysForUserFromBackendTest2() {
            val expectedResult = createArrayOfTradfriGatewaySpiesFromResponse(uid, 3)
//            every { remoteDataSourceMock.getTradfriGatewaysForUser(any()) } returns Single.fromCallable { expectedResult }
//
//            val userSpy = createUserSpy()
//
//            gatewayRepository.getTradfriGatewaysForUserFromBackend(userSpy.email, remoteDataSourceMock)
//                             .test()
//                             .assertResult(expectedResult)
        }

        @Test
        @DisplayName("Given that remoteDataSource throws an error Then the result is the thrown error")
        fun getTradfriGatewaysForUserFromBackendTest3() {
            val exceptionMock: Exception = mockk(relaxed = true)

            val userSpy = createUserSpy()
//            every { remoteDataSourceMock.getTradfriGatewaysForUser(any()) } returns Single.error(exceptionMock)
//
//            gatewayRepository.getTradfriGatewaysForUserFromBackend(userSpy.email, remoteDataSourceMock)
//                             .test()
//                             .assertError(Exception::class.java)
        }
    }

    @Nested
    inner class RegisterNewTradfriGatewayBackendSideTests {
        @Test
        @DisplayName("Given that there isn't a gateway registered on the backend that has the specific identity and security code AND no errors occur during the API call Then the result is the generated new foreign key from the backend")
        fun addNewTradfriGatewayTest1() {
            val expectedGatewayEnity = createListOfTradfriGatewayEntitySpies(listLength = 1)[0]

            // every time the psk is called, a new one is generated because of the getter
            val expectedPsk = psk

            every { remoteDataSourceMock.registerNewTradfriGateway(any()) } returns Single.fromCallable { expectedPsk }
            
            gatewayRepository.registerNewGatewayBackendSide(expectedGatewayEnity,
                                                            remoteDataSourceMock)
                             .test()
                             .assertValue(expectedPsk)
        }

        @Test
        @DisplayName("Given that there is already a gateway registered on the backend that has the specific identity and security code Then the result is an error returned from backend")
        fun addNewTradfriGatewayTest2() {
            val expectedGatewayEnity = createListOfTradfriGatewayEntitySpies(listLength = 1)[0]
            val responseMock: Response<String> = mockk(relaxed = true)
            every { responseMock.code() } returns 409
            every { responseMock.message() } returns "Already exists"
            val exceptionSpy = HttpException(responseMock)
            every { remoteDataSourceMock.registerNewTradfriGateway(any()) } returns Single.error(exceptionSpy)

            gatewayRepository.registerNewGatewayBackendSide(expectedGatewayEnity,
                                                            remoteDataSourceMock)
                             .test()
                             .assertError(HttpException::class.java)
                             .assertErrorMessage("HTTP 409 Already exists")
        }

        @Test
        @DisplayName("Given that an error occurs during the API call Then the result is an error returned from backend")
        fun addNewTradfriGatewayTest3() {
            val expectedGatewayEnity = createListOfTradfriGatewayEntitySpies(listLength = 1)[0]
            val exceptionSpy = Exception()
            every { remoteDataSourceMock.registerNewTradfriGateway(any()) } returns Single.error(exceptionSpy)

            gatewayRepository.registerNewGatewayBackendSide(expectedGatewayEnity,
                                                            remoteDataSourceMock)
                             .test()
                             .assertError(Exception::class.java)
        }

        @Test
        @DisplayName("Given that the idToken is an empty string Then the result is an empty string")
        fun addNewTradfriGatewayTest4() {
            val expectedGatewayEnity = createListOfTradfriGatewayEntitySpies(listLength = 1)[0]
            gatewayRepository.registerNewGatewayBackendSide(expectedGatewayEnity,
                                                            remoteDataSourceMock)
                             .test()
                             .assertValue("")
        }
    }

    @Nested
    inner class UpdateGatewayBackendSideTests {
        @Test
        @DisplayName("Given that there is a gateway registered on the backend that has the specific psk and userId AND no errors occur during the API call Then the result is the generated new foreign key from the backend")
        fun updateGatewayBackendSideTest1() {
            val expectedGatewayEnity = createListOfTradfriGatewayEntitySpies(listLength = 1)[0]

            // every time the psk is called, a new one is generated because of the getter
            val expectedPsk = psk

            every { remoteDataSourceMock.updateTradfriGateway(any()) } returns Single.just(expectedPsk)

            gatewayRepository.updateGatewayBackendSide(expectedGatewayEnity,
                                                       remoteDataSourceMock)
                             .test()
                             .assertValue(expectedPsk)
        }

        @Test
        @DisplayName("Given that an error occurs during the API call Then the result is an error returned from backend")
        fun updateGatewayBackendSideTest3() {
            val expectedGatewayEnity = createListOfTradfriGatewayEntitySpies(listLength = 1)[0]
            val exceptionSpy = Exception()
            every { remoteDataSourceMock.updateTradfriGateway(any()) } returns Single.error(exceptionSpy)

            gatewayRepository.updateGatewayBackendSide(expectedGatewayEnity,
                                                       remoteDataSourceMock)
                             .test()
                             .assertError(Exception::class.java)
        }

        @Test
        @DisplayName("Given that the idToken is an empty string Then the result is an empty string")
        fun updateGatewayBackendSideTest4() {
            val expectedGatewayEnyity = createListOfTradfriGatewayEntitySpies(listLength = 1)[0]
            gatewayRepository.updateGatewayBackendSide(expectedGatewayEnyity,
                                                       remoteDataSourceMock)
                             .test()
                             .assertValue("")
        }
    }
}