package rad.master.rk.zwonne.smartyha

import com.google.firebase.auth.FirebaseUser
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import rad.master.rk.zwonne.smartyha.data.room.entities.User

interface UserTestDataFactory : TestDataFactory {

    fun createUser(uid: String = TestDataFactory.uid,
                   email: String? = "test.user@yopmail.com"): User = User(uid, email!!)

    fun createUserSpy(
            uid: String = TestDataFactory.uid,
            email: String = "test.user@yopmail.com",
            registeredOnBackend: Boolean = false): User = spyk(User(uid, email, registeredOnBackend))

    fun createFirebaseUserMock(
            uid: String = TestDataFactory.uid,
            email: String? = "test.user@yopmail.com",
            displayName: String? = "TestUser"
    ): FirebaseUser {
        val firebaseUserMock: FirebaseUser = mockk(relaxed = true)
        every { firebaseUserMock.uid } returns uid
        every { firebaseUserMock.email } returns email
        every { firebaseUserMock.displayName } returns displayName

        return firebaseUserMock
    }
}
