package rad.master.rk.zwonne.smartyha

import kotlin.random.Random

interface TestDataFactory {
    companion object {
        val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        fun generateRandomAlphaNumericString(length: Long): String {
            return (1..length).map { Random.nextInt(0, charPool.size) }
                              .map(charPool::get)
                              .joinToString("")
        }

        val random: Random = Random(100)

        val psk: String = generateRandomAlphaNumericString(16)

        val uid: String = generateRandomAlphaNumericString(16)

        val bulbId: Int = random.nextInt(0, 100)
    }
}