package rad.master.rk.zwonne.smartyha

import rad.master.rk.zwonne.smartyha.data.UrlHolder

open class UrlTestDataFactory {

    val httpsStringLiteral = "https"

    val schemeColonBackslashLiteral = "://"

    val httpStringLiteral = "http"

    val hostAddressLiteral = "127.0.0.1"

    val portColonLiteral = ":"

    val portLiteral = "8888"

    fun getUrlHolderSpy(
            scheme: String = httpStringLiteral,
            schemeColonBackslash: String = schemeColonBackslashLiteral,
            host: String = hostAddressLiteral,
            portColon: String = portColonLiteral,
            port: String = portLiteral
    ) = UrlHolder(scheme, schemeColonBackslash, host, port, portColon)

    fun getUrlString(scheme: String, host: String, port: String = ""): String = StringBuilder(scheme).append(schemeColonBackslashLiteral)
                                                                                                     .append(host)
                                                                                                     .append(if (port.isNotBlank()) ":$port" else "")
                                                                                                     .toString()
    /**
     * nice data class for easier parameterized tests
     */
    data class PortTestData(
            val input: String,
            val expected: Boolean
    )
}