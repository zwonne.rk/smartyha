package rad.master.rk.zwonne.smartyha

import io.mockk.spyk
import rad.master.rk.zwonne.smartyha.TestDataFactory.Companion.generateRandomAlphaNumericString
import rad.master.rk.zwonne.smartyha.TestDataFactory.Companion.random
import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriGatewayFromResponse
import rad.master.rk.zwonne.smartyha.data.remote.responsemodels.TradfriLightBulbFromResponse
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriLightBulb
import rad.master.rk.zwonne.smartyha.data.room.entities.Device.TradfriGateway

interface TradfriTestDataFactory : TestDataFactory {

    fun createArrayOfTradfriGatewaySpiesFromResponse(
            uid: String = TestDataFactory.uid,
            arrayLength: Int
    ): Array<TradfriGatewayFromResponse> {
        return Array(arrayLength) {
            spyk(TradfriGatewayFromResponse(generateRandomAlphaNumericString(16),
                                            generateRandomAlphaNumericString(16),
                                            "192.168.1.$it",
                                            "test.user",
                                            uid,
                                            generateRandomAlphaNumericString(16)))
        }
    }

    fun createGateway(
            psk: String = TestDataFactory.psk,
            identity: String = "test.user",
            securityCode: String = generateRandomAlphaNumericString(16),
            userId: String = TestDataFactory.uid,
            host: String = "192.168.1.${random.nextInt()}",
            label: String = "",
            icon: String? = null
    ) = TradfriGateway(psk, identity,securityCode, userId, host, label, icon)

    fun createGatewaySpy(
            psk: String = TestDataFactory.psk,
            identity: String = generateRandomAlphaNumericString(16),
            securityCode: String = generateRandomAlphaNumericString(16),
            userId: String = TestDataFactory.uid,
            host: String = "192.168.1.${random.nextInt()}",
            label: String = "",
            icon: String? = null
    ) = spyk(createGateway(psk, identity, securityCode, userId, host, label, icon))

    fun createListOfTradfriGatewayEntitySpies(uid: String = TestDataFactory.uid,
                                              listLength: Int): List<TradfriGateway> {
        return List(listLength) { i ->
            val randomString = generateRandomAlphaNumericString(16)
            createGatewaySpy(randomString,
                             "test.user",
                             randomString,
                             uid,
                             "192.168.1.$i")
        }
    }

    fun createLightBulb(
            psk: String = TestDataFactory.psk,
            bulbId: Int = TestDataFactory.bulbId,
            uid: String = TestDataFactory.uid,
            manufacturer: String = "",
            modelNumber: String = "",
            firmwareVerison: String = "",
            name: String = "",
            reachable: Boolean = true,
            lastSeen: Int = random.nextInt(),
            isOn: Boolean = false,
            dimmerValue: Float = 127f,
            icon: String? = null
    ) = TradfriLightBulb(psk, bulbId, uid, name, lastSeen, reachable, isOn, dimmerValue, manufacturer, modelNumber, firmwareVerison)

    fun createLightBulbSpy(
            psk: String = TestDataFactory.psk,
            bulbId: Int = TestDataFactory.bulbId,
            uid: String = TestDataFactory.uid,
            manufacturer: String = "",
            modelNumber: String = "",
            firmwareVerison: String = "",
            name: String = "",
            reachable: Boolean = true,
            lastSeen: Int = random.nextInt(),
            isOn: Boolean = false,
            dimmerValue: Float = 127f,
            icon: String? = null
    ) = spyk(createLightBulb(psk, bulbId, uid, manufacturer, modelNumber, firmwareVerison, name, reachable, lastSeen, isOn, dimmerValue, icon))

    fun createLightBulbSpyList(arrayLength: Int): List<TradfriLightBulb> {
        return MutableList<TradfriLightBulb>(arrayLength) { i ->
            createLightBulbSpy(psk = generateRandomAlphaNumericString(16), bulbId = i)
        }
    }

    fun createListOfTradfriLightBulbSpiesFromResponse(length: Int): List<TradfriLightBulbFromResponse> {
        return MutableList<TradfriLightBulbFromResponse>(length) { i ->
            spyk(TradfriLightBulbFromResponse(generateRandomAlphaNumericString(16),
                                              i,
                                              "Name_$i",
                                              TestDataFactory.uid,
                                              random.nextInt(),
                                              true,
                                              1,
                                              127,
                                              "Manufacturer_$i",
                                              "Model_number_$i",
                                              "Firmware_version_$i"))
        }
    }
}