package rad.master.rk.zwonne.smartyha

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import io.kotest.matchers.shouldBe
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import rad.master.rk.zwonne.smartyha.data.UrlHolder
import rad.master.rk.zwonne.smartyha.data.room.SmartyHADatabase
import rad.master.rk.zwonne.smartyha.data.room.daos.UserDao

@RunWith(AndroidJUnit4ClassRunner::class)
class UserDaoTest {

    private companion object: UserTestDataFactory()

    private lateinit var userDao: UserDao

    private lateinit var db: SmartyHADatabase

    private val expectedResult = createUser()

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().context
        db = Room.inMemoryDatabaseBuilder(context, SmartyHADatabase::class.java)
                 .build()

        userDao = db.userDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertNewUsersAndReadByUidAndEmail() {
        val uid = expectedResult.uid
        val email = expectedResult.email

        // add the user to the db
        userDao.insertUser(expectedResult)

        val byEmail = userDao.getUserByEmail(email)
        assertThat(byEmail).isEqualTo(expectedResult)

        userDao.getUserByIdSingle(uid)
               .flattenAsObservable { it }
               .test()
               .assertResult(expectedResult)

        // result should be null since this user
        // wasn't inserted into local db
        userDao.getUserByIdSingle("11")
               .flattenAsObservable { it }
               .test()
               .assertResult()
    }

    @Test
    @Throws(Exception::class)
    fun UpdateUser() {
        val uid = expectedResult.uid
        val urlHolder = UrlHolder("https", "://","somewebsite.com")

        // add the user to the db
        userDao.insertUser(expectedResult)

        userDao.getUserByIdSingle(uid)
               .flattenAsObservable { it }
               .test()
               .assertResult(expectedResult)

        // change user's 'registeredOnBackend' field in the local db
        userDao.updateRegisteredOnBackend(expectedResult.uid, true) shouldBe 1
        userDao.updateUrlHolder(
                expectedResult.uid,
                urlHolder.scheme,
                urlHolder.schemeColonBackslash,
                urlHolder.scheme,
                urlHolder.portColon,
                urlHolder.portColon) shouldBe 1

        // verify that the changes were saved to local db
        userDao.getUserByIdSingle(uid)
               .flattenAsObservable { it }
               .test()
               .assertResult(expectedResult.copy(registeredOnBackend = true,
                                                 urlHolder = urlHolder))
    }

    @Test
    @Throws(Exception::class)
    fun deleteUser() {
        val uid = expectedResult.uid

        userDao.insertUser(expectedResult)

        userDao.getUserByIdSingle(uid)
               .flattenAsObservable { it }
               .test()
               .assertResult(expectedResult)

        userDao.deleteUser(expectedResult)

        userDao.getUserByIdSingle(uid)
               .flattenAsObservable { it }
               .test()
               .assertResult()
    }
}
