package rad.master.rk.zwonne.smartyha

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.jraska.livedata.test
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import rad.master.rk.zwonne.smartyha.data.room.SmartyHADatabase
import rad.master.rk.zwonne.smartyha.data.room.daos.TradfriGatewayDao
import rad.master.rk.zwonne.smartyha.data.room.daos.UserDao
import rad.master.rk.zwonne.smartyha.data.room.entities.Device

@RunWith(AndroidJUnit4ClassRunner::class)
class TradfriGatewayDaoTest {

    private companion object: UserTestDataFactory()

    private lateinit var tradfriGatewayDao: TradfriGatewayDao

    private lateinit var userDao: UserDao

    private lateinit var db: SmartyHADatabase

    private lateinit var expectedResult: Device.TradfriGateway

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().context
        db = Room.inMemoryDatabaseBuilder(context, SmartyHADatabase::class.java)
                 .build()

        tradfriGatewayDao = db.tradfriGatewayDao()
        userDao = db.userDao()
        expectedResult = createTradfriGateway()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertNewTradfriGatewaysAndReadByPskAndUserId() {
        // add a user to the db because the tradfri
        // gateway needs a foreign key
        userDao.insertUser(createUser())

        val psk = expectedResult.psk

        // add the gateway to the db
        tradfriGatewayDao.insert(expectedResult)

        tradfriGatewayDao.getGatewaysByUserIdLiveData(psk)
                         .test()
                         .assertValue { res: Array<Device.TradfriGateway> -> res.first() == expectedResult }

        // result should be empty since this user
        // wasn't inserted into local db
        tradfriGatewayDao.getGatewaysByUserIdLiveData("11")
                         .test()
                         .assertValue { res: Array<Device.TradfriGateway> -> res.isEmpty() }
    }

    @Test
    @Throws(Exception::class)
    fun updateTradfriGateway() {
        // add a user to the db because the tradfri
        // gateway needs a foreign key
        userDao.insertUser(createUser())

        val psk = expectedResult.psk

        // add the gateway to the db
        tradfriGatewayDao.insert(expectedResult)

        tradfriGatewayDao.getGatewaysByUserIdLiveData(psk)
                         .test()
                         .assertValue { res: Array<Device.TradfriGateway> -> res.first() == expectedResult }

        // change gateway's 'identity' field in the local db
        tradfriGatewayDao.updateTradfriGateway(expectedResult.psk, "211.212.222.2", expectedResult.label)
                         .test()
                         .assertValue(1)

        // verify that the changes were saved to local db
        tradfriGatewayDao.getGatewaysByUserIdLiveData(psk)
                         .test()
                         .assertValue { res: Array<Device.TradfriGateway> -> res.first() == expectedResult.copy(host = "211.212.222.2") }
    }

    @Test
    @Throws(Exception::class)
    fun deleteTradfriGateway() {
        // add the user to the db because the tradfri
        // gateway needs a foreign key
        userDao.insertUser(createUser())

        val psk = expectedResult.psk

        tradfriGatewayDao.insert(expectedResult)

        tradfriGatewayDao.getGatewaysByUserIdLiveData(psk)
                         .test()
                         .assertValue { res: Array<Device.TradfriGateway> -> res.first() == expectedResult }

        tradfriGatewayDao.delete(expectedResult)

        tradfriGatewayDao.getGatewaysByUserIdLiveData(psk)
                         .test()
                         .assertValue { res: Array<Device.TradfriGateway> -> res.isEmpty() }
    }

    @Test
    @Throws(Exception::class)
    fun deleteTradfriGatewayByCascadeFromUser() {
        val user = createUser()
        // add the user to the db because the tradfri
        // gateway needs a foreign key
        userDao.insertUser(user)

        val psk = expectedResult.psk

        tradfriGatewayDao.insert(expectedResult)

        tradfriGatewayDao.getGatewaysByUserIdLiveData(psk)
                         .test()
                         .assertValue { res: Array<Device.TradfriGateway> -> res.first() == expectedResult }

        // tradfri gateway should also be deleted by
        // cascade effect
        userDao.deleteUser(user)

        tradfriGatewayDao.getGatewaysByUserIdLiveData(psk)
                         .test()
                         .assertValue { res: Array<Device.TradfriGateway> -> res.isEmpty() }
    }
}