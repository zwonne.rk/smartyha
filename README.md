# SmartyHA Android client

A DIY Home Automation system, created as a playground project.

## Mandatory setup

- In order for this system to work, it needs [Smarty HA backend](https://gitlab.com/zwonne.rk/smarty-ha-backend) installed on a pc/laptop/raspberry pi.
- The Android client app will ask for a URL to the deployed server. If you plan on using https with a self signed certificate, you need to clone this repo, and put the cert file (public key) in the /res/raw folder. After that, build the apk, and install it from your machine.

## Supported devices:

- IKEA TRÅDFRI lightbulbs (additionally needs IKEA Gateway, and a steering device in order to work),
- Amazon's Echo Dot 2,
- Any sensor module compatible with a Raspberry Pi, on the assumption that the backend side is deployed on the Raspberry,
- Beacons,